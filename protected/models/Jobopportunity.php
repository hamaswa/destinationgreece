<?php

/**
 * This is the model class for table "jobopportunity".
 *
 * The followings are the available columns in table 'jobopportunity':
 * @property integer $ID
 * @property string $title
 * @property string $description
 * @property string $date
 * @property string $applyBy
 * @property integer $language
 */
class Jobopportunity extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Jobopportunity the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jobopportunity';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('language', 'numerical', 'integerOnly'=>true),
			array('title, description', 'length', 'max'=>255),
			array('date, applyBy', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, title, description, date, applyBy, language', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'title' => 'Title',
			'description' => 'Description',
			'date' => 'Date',
			'applyBy' => 'Apply By',
			'language' => 'Language',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('applyBy',$this->applyBy,true);
		$criteria->compare('language',$this->language);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}