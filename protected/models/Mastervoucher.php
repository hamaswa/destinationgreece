<?php

/**
 * This is the model class for table "mastervoucher".
 *
 * The followings are the available columns in table 'mastervoucher':
 * @property integer $ID
 * @property string $voucherID
 * @property string $date
 * @property integer $quoteid
 * @property integer $editedby
 */
class Mastervoucher extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Mastervoucher the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mastervoucher';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('quoteid, editedby', 'numerical', 'integerOnly'=>true),
			array('voucherID', 'length', 'max'=>255),
			array('date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, voucherID, date, quoteid, editedby', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'voucherID' => 'Voucher',
			'date' => 'Date',
			'quoteid' => 'Quoteid',
			'editedby' => 'Editedby',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('voucherID',$this->voucherID,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('quoteid',$this->quoteid);
		$criteria->compare('editedby',$this->editedby);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}