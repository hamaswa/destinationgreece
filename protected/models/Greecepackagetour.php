<?php

/**
 * This is the model class for table "greecepackagetour".
 *
 * The followings are the available columns in table 'greecepackagetour':
 * @property integer $ID
 * @property integer $gpID
 * @property integer $tour
 * @property integer $numOverNights
 */
class Greecepackagetour extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Greecepackagetour the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'greecepackagetour';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('gpID, tour, numOverNights', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, gpID, tour, numOverNights', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'gpID' => 'Gp',
			'tour' => 'Tour',
			'numOverNights' => 'Num Over Nights',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('gpID',$this->gpID);
		$criteria->compare('tour',$this->tour);
		$criteria->compare('numOverNights',$this->numOverNights);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}