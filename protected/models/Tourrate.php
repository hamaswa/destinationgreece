<?php

/**
 * This is the model class for table "tourrate".
 *
 * The followings are the available columns in table 'tourrate':
 * @property integer $ID
 * @property integer $tourID
 * @property string $fromDate
 * @property string $toDate
 * @property string $standard
 * @property string $sgl3
 * @property string $sgl4
 * @property string $dbl3
 * @property string $dbl4
 */
class Tourrate extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Tourrate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tourrate';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sgl3, sgl4, dbl3, dbl4', 'required'),
			array('tourID', 'numerical', 'integerOnly'=>true),
			array('standard, sgl3, sgl4, dbl3, dbl4', 'length', 'max'=>10),
			array('fromDate, toDate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, tourID, fromDate, toDate, standard, sgl3, sgl4, dbl3, dbl4', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'tourID' => 'Tour',
			'fromDate' => 'From Date',
			'toDate' => 'To Date',
			'standard' => 'Standard',
			'sgl3' => 'Sgl3',
			'sgl4' => 'Sgl4',
			'dbl3' => 'Dbl3',
			'dbl4' => 'Dbl4',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('tourID',$this->tourID);
		$criteria->compare('fromDate',$this->fromDate,true);
		$criteria->compare('toDate',$this->toDate,true);
		$criteria->compare('standard',$this->standard,true);
		$criteria->compare('sgl3',$this->sgl3,true);
		$criteria->compare('sgl4',$this->sgl4,true);
		$criteria->compare('dbl3',$this->dbl3,true);
		$criteria->compare('dbl4',$this->dbl4,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}