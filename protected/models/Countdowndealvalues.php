<?php

/**
 * This is the model class for table "countdowndealvalues".
 *
 * The followings are the available columns in table 'countdowndealvalues':
 * @property integer $ID
 * @property integer $countdownDealID
 * @property integer $saleNum
 * @property integer $discountValue
 */
class Countdowndealvalues extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Countdowndealvalues the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'countdowndealvalues';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('countdownDealID, saleNum, discountValue', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, countdownDealID, saleNum, discountValue', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'countdownDealID' => 'Countdown Deal',
			'saleNum' => 'Sale Num',
			'discountValue' => 'Discount Value',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('countdownDealID',$this->countdownDealID);
		$criteria->compare('saleNum',$this->saleNum);
		$criteria->compare('discountValue',$this->discountValue);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}