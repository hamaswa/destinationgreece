<?php

/**
 * This is the model class for table "villa".
 *
 * The followings are the available columns in table 'villa':
 * @property integer $ID
 * @property integer $provider
 * @property string $name
 * @property integer $city
 * @property string $location
 * @property string $reservationEmail
 * @property string $reservationPhone
 * @property integer $roomNum
 * @property integer $bathrooms
 * @property integer $ensuite
 * @property string $sleeps
 * @property integer $status
 * @property string $alias
 * @property string $description
 * @property string $terms
 * @property string $contact
 * @property integer $currency
 * @property string $facility
 * @property string $address1
 * @property string $address2
 * @property string $zip
 * @property string $fax
 * @property string $contactName
 * @property string $cell
 * @property string $email1
 * @property string $email2
 *
 * The followings are the available model relations:
 * @property Currency $currency0
 * @property Provider $provider0
 */
class Villa extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Villa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'villa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('reservationEmail, reservationPhone, status, facility, address1, address2, zip, fax, contactName, cell, email1, email2', 'required'),
			array('provider, city, roomNum, bathrooms, ensuite, status, currency', 'numerical', 'integerOnly'=>true),
			array('name, location, reservationEmail, reservationPhone, sleeps, alias, description, terms, contact, address1, address2, contactName, cell, email1, email2', 'length', 'max'=>255),
			array('zip', 'length', 'max'=>20),
			array('fax', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, provider, name, city, location, reservationEmail, reservationPhone, roomNum, bathrooms, ensuite, sleeps, status, alias, description, terms, contact, currency, facility, address1, address2, zip, fax, contactName, cell, email1, email2', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'currency0' => array(self::BELONGS_TO, 'Currency', 'currency'),
			'provider0' => array(self::BELONGS_TO, 'Provider', 'provider'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'provider' => 'Provider',
			'name' => 'Name',
			'city' => 'City',
			'location' => 'Location',
			'reservationEmail' => 'Reservation Email',
			'reservationPhone' => 'Reservation Phone',
			'roomNum' => 'Room Num',
			'bathrooms' => 'Bathrooms',
			'ensuite' => 'Ensuite',
			'sleeps' => 'Sleeps',
			'status' => 'Status',
			'alias' => 'Alias',
			'description' => 'Description',
			'terms' => 'Terms',
			'contact' => 'Contact',
			'currency' => 'Currency',
			'facility' => 'Facility',
			'address1' => 'Address1',
			'address2' => 'Address2',
			'zip' => 'Zip',
			'fax' => 'Fax',
			'contactName' => 'Contact Name',
			'cell' => 'Cell',
			'email1' => 'Email1',
			'email2' => 'Email2',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('provider',$this->provider);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('city',$this->city);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('reservationEmail',$this->reservationEmail,true);
		$criteria->compare('reservationPhone',$this->reservationPhone,true);
		$criteria->compare('roomNum',$this->roomNum);
		$criteria->compare('bathrooms',$this->bathrooms);
		$criteria->compare('ensuite',$this->ensuite);
		$criteria->compare('sleeps',$this->sleeps,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('alias',$this->alias,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('terms',$this->terms,true);
		$criteria->compare('contact',$this->contact,true);
		$criteria->compare('currency',$this->currency);
		$criteria->compare('facility',$this->facility,true);
		$criteria->compare('address1',$this->address1,true);
		$criteria->compare('address2',$this->address2,true);
		$criteria->compare('zip',$this->zip,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('contactName',$this->contactName,true);
		$criteria->compare('cell',$this->cell,true);
		$criteria->compare('email1',$this->email1,true);
		$criteria->compare('email2',$this->email2,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}