<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $ID
 * @property string $firstName
 * @property string $lastName
 * @property string $secretQuestion
 * @property string $secretAnswer
 * @property string $email1
 * @property string $email2
 * @property string $phone1
 * @property string $phone2
 * @property string $cell1
 * @property string $cell2
 * @property string $fax
 * @property string $address1
 * @property string $address2
 * @property string $city
 * @property string $zip
 * @property string $state
 * @property string $country
 * @property string $notes
 * @property string $areacode1
 * @property string $areacode2
 * @property string $username
 * @property string $password
 * @property integer $changePass
 * @property string $dateCreated
 *
 * The followings are the available model relations:
 * @property Agent $agent
 * @property Provider $provider
 */
class User extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('address2, changePass', 'required'),
			array('changePass', 'numerical', 'integerOnly'=>true),
			array('firstName, lastName, secretQuestion, secretAnswer, email1, email2, phone1, phone2, cell1, cell2, fax, address1, address2, city, zip, state, country, notes, areacode1, areacode2, username, password', 'length', 'max'=>255),
			array('dateCreated', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, firstName, lastName, secretQuestion, secretAnswer, email1, email2, phone1, phone2, cell1, cell2, fax, address1, address2, city, zip, state, country, notes, areacode1, areacode2, username, password, changePass, dateCreated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'agent' => array(self::HAS_ONE, 'Agent', 'agentID'),
			'provider' => array(self::HAS_ONE, 'Provider', 'providerID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'firstName' => 'First Name',
			'lastName' => 'Last Name',
			'secretQuestion' => 'Secret Question',
			'secretAnswer' => 'Secret Answer',
			'email1' => 'Email1',
			'email2' => 'Email2',
			'phone1' => 'Phone1',
			'phone2' => 'Phone2',
			'cell1' => 'Cell1',
			'cell2' => 'Cell2',
			'fax' => 'Fax',
			'address1' => 'Address1',
			'address2' => 'Address2',
			'city' => 'City',
			'zip' => 'Zip',
			'state' => 'State',
			'country' => 'Country',
			'notes' => 'Notes',
			'areacode1' => 'Areacode1',
			'areacode2' => 'Areacode2',
			'username' => 'Username',
			'password' => 'Password',
			'changePass' => 'Change Pass',
			'dateCreated' => 'Date Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('firstName',$this->firstName,true);
		$criteria->compare('lastName',$this->lastName,true);
		$criteria->compare('secretQuestion',$this->secretQuestion,true);
		$criteria->compare('secretAnswer',$this->secretAnswer,true);
		$criteria->compare('email1',$this->email1,true);
		$criteria->compare('email2',$this->email2,true);
		$criteria->compare('phone1',$this->phone1,true);
		$criteria->compare('phone2',$this->phone2,true);
		$criteria->compare('cell1',$this->cell1,true);
		$criteria->compare('cell2',$this->cell2,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('address1',$this->address1,true);
		$criteria->compare('address2',$this->address2,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('zip',$this->zip,true);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('notes',$this->notes,true);
		$criteria->compare('areacode1',$this->areacode1,true);
		$criteria->compare('areacode2',$this->areacode2,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('changePass',$this->changePass);
		$criteria->compare('dateCreated',$this->dateCreated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}