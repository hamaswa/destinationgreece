<?php

/**
 * This is the model class for table "nongreecepackage".
 *
 * The followings are the available columns in table 'nongreecepackage':
 * @property integer $ID
 * @property string $name
 * @property string $departson
 * @property string $visiting
 * @property string $packageBasis
 * @property integer $numNights
 * @property integer $numDays
 * @property integer $currency
 * @property integer $status
 * @property string $summary
 * @property string $dayToDayItenirary
 * @property string $servicesIncluded
 * @property string $map
 * @property string $datesAndRates
 * @property string $terms
 * @property string $entraceFee
 * @property string $lunch
 */
class Nongreecepackage extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Nongreecepackage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'nongreecepackage';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status', 'required'),
			array('numNights, numDays, currency, status', 'numerical', 'integerOnly'=>true),
			array('name, departson, visiting, packageBasis, entraceFee, lunch', 'length', 'max'=>255),
			array('summary, dayToDayItenirary, servicesIncluded, map, datesAndRates, terms', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, name, departson, visiting, packageBasis, numNights, numDays, currency, status, summary, dayToDayItenirary, servicesIncluded, map, datesAndRates, terms, entraceFee, lunch', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'name' => 'Name',
			'departson' => 'Departson',
			'visiting' => 'Visiting',
			'packageBasis' => 'Package Basis',
			'numNights' => 'Num Nights',
			'numDays' => 'Num Days',
			'currency' => 'Currency',
			'status' => 'Status',
			'summary' => 'Summary',
			'dayToDayItenirary' => 'Day To Day Itenirary',
			'servicesIncluded' => 'Services Included',
			'map' => 'Map',
			'datesAndRates' => 'Dates And Rates',
			'terms' => 'Terms',
			'entraceFee' => 'Entrace Fee',
			'lunch' => 'Lunch',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('departson',$this->departson,true);
		$criteria->compare('visiting',$this->visiting,true);
		$criteria->compare('packageBasis',$this->packageBasis,true);
		$criteria->compare('numNights',$this->numNights);
		$criteria->compare('numDays',$this->numDays);
		$criteria->compare('currency',$this->currency);
		$criteria->compare('status',$this->status);
		$criteria->compare('summary',$this->summary,true);
		$criteria->compare('dayToDayItenirary',$this->dayToDayItenirary,true);
		$criteria->compare('servicesIncluded',$this->servicesIncluded,true);
		$criteria->compare('map',$this->map,true);
		$criteria->compare('datesAndRates',$this->datesAndRates,true);
		$criteria->compare('terms',$this->terms,true);
		$criteria->compare('entraceFee',$this->entraceFee,true);
		$criteria->compare('lunch',$this->lunch,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}