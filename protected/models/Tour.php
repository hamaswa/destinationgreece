<?php

/**
 * This is the model class for table "tour".
 *
 * The followings are the available columns in table 'tour':
 * @property integer $ID
 * @property string $name
 * @property integer $provider
 * @property integer $numNights
 * @property string $departureDates
 * @property integer $currency
 * @property integer $status
 * @property string $itinerary
 * @property string $description
 * @property string $terms
 * @property string $contact
 * @property string $address1
 * @property string $address2
 * @property string $zip
 * @property string $fax
 * @property string $contactName
 * @property string $cell
 * @property string $email1
 * @property string $email2
 */
class Tour extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Tour the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tour';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('numNights, status, address1, address2, zip, fax, contactName, cell, email1, email2', 'required'),
			array('provider, numNights, currency, status', 'numerical', 'integerOnly'=>true),
			array('name, itinerary, description, terms, contact, contactName, email1, email2', 'length', 'max'=>255),
			array('address1, address2', 'length', 'max'=>512),
			array('zip, fax, cell', 'length', 'max'=>20),
			array('departureDates', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, name, provider, numNights, departureDates, currency, status, itinerary, description, terms, contact, address1, address2, zip, fax, contactName, cell, email1, email2', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'name' => 'Name',
			'provider' => 'Provider',
			'numNights' => 'Num Nights',
			'departureDates' => 'Departure Dates',
			'currency' => 'Currency',
			'status' => 'Status',
			'itinerary' => 'Itinerary',
			'description' => 'Description',
			'terms' => 'Terms',
			'contact' => 'Contact',
			'address1' => 'Address1',
			'address2' => 'Address2',
			'zip' => 'Zip',
			'fax' => 'Fax',
			'contactName' => 'Contact Name',
			'cell' => 'Cell',
			'email1' => 'Email1',
			'email2' => 'Email2',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('provider',$this->provider);
		$criteria->compare('numNights',$this->numNights);
		$criteria->compare('departureDates',$this->departureDates,true);
		$criteria->compare('currency',$this->currency);
		$criteria->compare('status',$this->status);
		$criteria->compare('itinerary',$this->itinerary,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('terms',$this->terms,true);
		$criteria->compare('contact',$this->contact,true);
		$criteria->compare('address1',$this->address1,true);
		$criteria->compare('address2',$this->address2,true);
		$criteria->compare('zip',$this->zip,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('contactName',$this->contactName,true);
		$criteria->compare('cell',$this->cell,true);
		$criteria->compare('email1',$this->email1,true);
		$criteria->compare('email2',$this->email2,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}