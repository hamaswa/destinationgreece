<?php

/**
 * This is the model class for table "greecepackagetransfer".
 *
 * The followings are the available columns in table 'greecepackagetransfer':
 * @property integer $ID
 * @property integer $gpID
 * @property integer $transfer
 * @property integer $numOverNights
 */
class Greecepackagetransfer extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Greecepackagetransfer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'greecepackagetransfer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('gpID, transfer, numOverNights', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, gpID, transfer, numOverNights', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'gpID' => 'Gp',
			'transfer' => 'Transfer',
			'numOverNights' => 'Num Over Nights',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('gpID',$this->gpID);
		$criteria->compare('transfer',$this->transfer);
		$criteria->compare('numOverNights',$this->numOverNights);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}