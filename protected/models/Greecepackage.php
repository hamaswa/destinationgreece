<?php

/**
 * This is the model class for table "greecepackage".
 *
 * The followings are the available columns in table 'greecepackage':
 * @property integer $ID
 * @property string $name
 * @property string $departsonDate
 * @property string $visiting
 * @property string $status
 * @property integer $packageClass
 * @property integer $numnights
 * @property integer $numdays
 * @property string $summary
 * @property string $dayToDayItinerary
 * @property string $servicesIncluded
 * @property string $map
 * @property string $terms
 */
class Greecepackage extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Greecepackage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'greecepackage';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('packageClass, numnights, numdays', 'numerical', 'integerOnly'=>true),
			array('name, visiting, status', 'length', 'max'=>255),
			array('departsonDate, summary, dayToDayItinerary, servicesIncluded, map, terms', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, name, departsonDate, visiting, status, packageClass, numnights, numdays, summary, dayToDayItinerary, servicesIncluded, map, terms', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'name' => 'Name',
			'departsonDate' => 'Departson Date',
			'visiting' => 'Visiting',
			'status' => 'Status',
			'packageClass' => 'Package Class',
			'numnights' => 'Numnights',
			'numdays' => 'Numdays',
			'summary' => 'Summary',
			'dayToDayItinerary' => 'Day To Day Itinerary',
			'servicesIncluded' => 'Services Included',
			'map' => 'Map',
			'terms' => 'Terms',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('departsonDate',$this->departsonDate,true);
		$criteria->compare('visiting',$this->visiting,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('packageClass',$this->packageClass);
		$criteria->compare('numnights',$this->numnights);
		$criteria->compare('numdays',$this->numdays);
		$criteria->compare('summary',$this->summary,true);
		$criteria->compare('dayToDayItinerary',$this->dayToDayItinerary,true);
		$criteria->compare('servicesIncluded',$this->servicesIncluded,true);
		$criteria->compare('map',$this->map,true);
		$criteria->compare('terms',$this->terms,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}