<?php

/**
 * This is the model class for table "cruise".
 *
 * The followings are the available columns in table 'cruise':
 * @property integer $ID
 * @property integer $provider
 * @property string $cruiseName
 * @property string $shipName
 * @property string $departureDates
 * @property integer $numNights
 * @property string $reservationEmail
 * @property string $reserationPhone
 * @property string $cruiseLine
 * @property string $portTax
 * @property string $portsOfCall
 * @property integer $currency
 * @property integer $status
 * @property string $itinerary
 * @property string $shipInfo
 * @property string $shorex
 * @property string $terms
 * @property string $contact
 * @property string $address1
 * @property string $address2
 * @property string $zip
 * @property string $fax
 * @property string $contactName
 * @property string $cell
 * @property string $email1
 * @property string $email2
 */
class Cruise extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Cruise the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cruise';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status, address1, address2, zip, fax, contactName, cell, email1, email2', 'required'),
			array('provider, numNights, currency, status', 'numerical', 'integerOnly'=>true),
			array('cruiseName, shipName, departureDates, reservationEmail, reserationPhone, cruiseLine, portTax, portsOfCall, itinerary, shipInfo, shorex, terms, contact, fax, contactName, email1, email2', 'length', 'max'=>255),
			array('address1, address2', 'length', 'max'=>512),
			array('zip', 'length', 'max'=>20),
			array('cell', 'length', 'max'=>30),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, provider, cruiseName, shipName, departureDates, numNights, reservationEmail, reserationPhone, cruiseLine, portTax, portsOfCall, currency, status, itinerary, shipInfo, shorex, terms, contact, address1, address2, zip, fax, contactName, cell, email1, email2', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'provider' => 'Provider',
			'cruiseName' => 'Cruise Name',
			'shipName' => 'Ship Name',
			'departureDates' => 'Departure Dates',
			'numNights' => 'Num Nights',
			'reservationEmail' => 'Reservation Email',
			'reserationPhone' => 'Reseration Phone',
			'cruiseLine' => 'Cruise Line',
			'portTax' => 'Port Tax',
			'portsOfCall' => 'Ports Of Call',
			'currency' => 'Currency',
			'status' => 'Status',
			'itinerary' => 'Itinerary',
			'shipInfo' => 'Ship Info',
			'shorex' => 'Shorex',
			'terms' => 'Terms',
			'contact' => 'Contact',
			'address1' => 'Address1',
			'address2' => 'Address2',
			'zip' => 'Zip',
			'fax' => 'Fax',
			'contactName' => 'Contact Name',
			'cell' => 'Cell',
			'email1' => 'Email1',
			'email2' => 'Email2',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('provider',$this->provider);
		$criteria->compare('cruiseName',$this->cruiseName,true);
		$criteria->compare('shipName',$this->shipName,true);
		$criteria->compare('departureDates',$this->departureDates,true);
		$criteria->compare('numNights',$this->numNights);
		$criteria->compare('reservationEmail',$this->reservationEmail,true);
		$criteria->compare('reserationPhone',$this->reserationPhone,true);
		$criteria->compare('cruiseLine',$this->cruiseLine,true);
		$criteria->compare('portTax',$this->portTax,true);
		$criteria->compare('portsOfCall',$this->portsOfCall,true);
		$criteria->compare('currency',$this->currency);
		$criteria->compare('status',$this->status);
		$criteria->compare('itinerary',$this->itinerary,true);
		$criteria->compare('shipInfo',$this->shipInfo,true);
		$criteria->compare('shorex',$this->shorex,true);
		$criteria->compare('terms',$this->terms,true);
		$criteria->compare('contact',$this->contact,true);
		$criteria->compare('address1',$this->address1,true);
		$criteria->compare('address2',$this->address2,true);
		$criteria->compare('zip',$this->zip,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('contactName',$this->contactName,true);
		$criteria->compare('cell',$this->cell,true);
		$criteria->compare('email1',$this->email1,true);
		$criteria->compare('email2',$this->email2,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}