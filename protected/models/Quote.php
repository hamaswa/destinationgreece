<?php

/**
 * This is the model class for table "quote".
 *
 * The followings are the available columns in table 'quote':
 * @property integer $ID
 * @property integer $clientid
 * @property integer $num
 * @property string $pax
 * @property integer $currency
 * @property string $validityDate
 * @property integer $travelAgent
 * @property string $agentCommissionPc
 * @property string $dgmarkupPc
 * @property string $ccHandlingPc
 * @property string $cost
 * @property string $dgMarkup
 * @property string $dgNetPrice
 * @property string $agentCommission
 * @property string $grossRate
 * @property string $ccHandling
 * @property string $grandTotal
 * @property integer $editedBy
 */
class Quote extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Quote the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'quote';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('clientid, num, currency, travelAgent, editedBy', 'numerical', 'integerOnly'=>true),
			array('pax, agentCommissionPc, dgmarkupPc, ccHandlingPc, cost, dgMarkup, dgNetPrice, agentCommission, grossRate, ccHandling, grandTotal', 'length', 'max'=>255),
			array('validityDate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, clientid, num, pax, currency, validityDate, travelAgent, agentCommissionPc, dgmarkupPc, ccHandlingPc, cost, dgMarkup, dgNetPrice, agentCommission, grossRate, ccHandling, grandTotal, editedBy', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'clientid' => 'Clientid',
			'num' => 'Num',
			'pax' => 'Pax',
			'currency' => 'Currency',
			'validityDate' => 'Validity Date',
			'travelAgent' => 'Travel Agent',
			'agentCommissionPc' => 'Agent Commission Pc',
			'dgmarkupPc' => 'Dgmarkup Pc',
			'ccHandlingPc' => 'Cc Handling Pc',
			'cost' => 'Cost',
			'dgMarkup' => 'Dg Markup',
			'dgNetPrice' => 'Dg Net Price',
			'agentCommission' => 'Agent Commission',
			'grossRate' => 'Gross Rate',
			'ccHandling' => 'Cc Handling',
			'grandTotal' => 'Grand Total',
			'editedBy' => 'Edited By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('clientid',$this->clientid);
		$criteria->compare('num',$this->num);
		$criteria->compare('pax',$this->pax,true);
		$criteria->compare('currency',$this->currency);
		$criteria->compare('validityDate',$this->validityDate,true);
		$criteria->compare('travelAgent',$this->travelAgent);
		$criteria->compare('agentCommissionPc',$this->agentCommissionPc,true);
		$criteria->compare('dgmarkupPc',$this->dgmarkupPc,true);
		$criteria->compare('ccHandlingPc',$this->ccHandlingPc,true);
		$criteria->compare('cost',$this->cost,true);
		$criteria->compare('dgMarkup',$this->dgMarkup,true);
		$criteria->compare('dgNetPrice',$this->dgNetPrice,true);
		$criteria->compare('agentCommission',$this->agentCommission,true);
		$criteria->compare('grossRate',$this->grossRate,true);
		$criteria->compare('ccHandling',$this->ccHandling,true);
		$criteria->compare('grandTotal',$this->grandTotal,true);
		$criteria->compare('editedBy',$this->editedBy);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}