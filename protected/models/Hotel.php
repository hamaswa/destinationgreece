<?php

/**
 * This is the model class for table "hotel".
 *
 * The followings are the available columns in table 'hotel':
 * @property integer $ID
 * @property integer $provider
 * @property string $name
 * @property string $reservationEmail
 * @property string $reservationPhone
 * @property integer $city
 * @property integer $hotelCategory
 * @property string $tripAdvisorRating
 * @property integer $status
 * @property integer $currency
 * @property string $description
 * @property string $terms
 * @property string $address1
 * @property string $address2
 * @property string $zip
 * @property string $fax
 * @property string $contactName
 * @property string $cell
 * @property string $email1
 * @property string $email2
 * @property string $facility
 * @property string $amenity
 *
 * The followings are the available model relations:
 * @property Hotelcategory $hotelCategory0
 * @property Provider $provider0
 * @property Currency $currency0
 */
class Hotel extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Hotel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'hotel';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status, address2, zip, fax, contactName, cell, email1, email2, facility, amenity', 'required'),
			array('provider, city, hotelCategory, status, currency', 'numerical', 'integerOnly'=>true),
			array('name, reservationEmail, reservationPhone, tripAdvisorRating, description, terms, address1, zip, contactName, cell, email1, email2', 'length', 'max'=>255),
			array('address2', 'length', 'max'=>256),
			array('fax', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, provider, name, reservationEmail, reservationPhone, city, hotelCategory, tripAdvisorRating, status, currency, description, terms, address1, address2, zip, fax, contactName, cell, email1, email2, facility, amenity', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'hotelCategory0' => array(self::BELONGS_TO, 'Hotelcategory', 'hotelCategory'),
			'provider0' => array(self::BELONGS_TO, 'Provider', 'provider'),
			'currency0' => array(self::BELONGS_TO, 'Currency', 'currency'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'provider' => 'Provider',
			'name' => 'Name',
			'reservationEmail' => 'Reservation Email',
			'reservationPhone' => 'Reservation Phone',
			'city' => 'City',
			'hotelCategory' => 'Hotel Category',
			'tripAdvisorRating' => 'Trip Advisor Rating',
			'status' => 'Status',
			'currency' => 'Currency',
			'description' => 'Description',
			'terms' => 'Terms',
			'address1' => 'Address1',
			'address2' => 'Address2',
			'zip' => 'Zip',
			'fax' => 'Fax',
			'contactName' => 'Contact Name',
			'cell' => 'Cell',
			'email1' => 'Email1',
			'email2' => 'Email2',
			'facility' => 'Facility',
			'amenity' => 'Amenity',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('provider',$this->provider);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('reservationEmail',$this->reservationEmail,true);
		$criteria->compare('reservationPhone',$this->reservationPhone,true);
		$criteria->compare('city',$this->city);
		$criteria->compare('hotelCategory',$this->hotelCategory);
		$criteria->compare('tripAdvisorRating',$this->tripAdvisorRating,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('currency',$this->currency);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('terms',$this->terms,true);
		$criteria->compare('address1',$this->address1,true);
		$criteria->compare('address2',$this->address2,true);
		$criteria->compare('zip',$this->zip,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('contactName',$this->contactName,true);
		$criteria->compare('cell',$this->cell,true);
		$criteria->compare('email1',$this->email1,true);
		$criteria->compare('email2',$this->email2,true);
		$criteria->compare('facility',$this->facility,true);
		$criteria->compare('amenity',$this->amenity,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}