<?php

/**
 * This is the model class for table "shorex".
 *
 * The followings are the available columns in table 'shorex':
 * @property integer $ID
 * @property string $name
 * @property integer $provider
 * @property string $tourDuration
 * @property string $port
 * @property integer $countryID
 * @property string $itinerary
 * @property string $description
 * @property string $services
 * @property string $summary
 * @property string $terms
 * @property string $contact
 * @property integer $status
 * @property integer $numNights
 * @property integer $currency
 * @property string $address1
 * @property string $address2
 * @property string $zip
 * @property string $fax
 * @property string $contactName
 * @property string $cell
 * @property string $email1
 * @property string $email2
 * @property string $entranceFee
 * @property string $lunchFee
 */
class Shorex extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Shorex the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'shorex';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('services, summary, status, address1, address2, zip, fax, contactName, cell, email1, email2, entranceFee, lunchFee', 'required'),
			array('provider, countryID, status, numNights, currency', 'numerical', 'integerOnly'=>true),
			array('name, tourDuration, port, itinerary, description, terms, contact, contactName, email1, email2', 'length', 'max'=>255),
			array('address1, address2', 'length', 'max'=>512),
			array('zip', 'length', 'max'=>10),
			array('fax', 'length', 'max'=>50),
			array('cell', 'length', 'max'=>20),
			array('entranceFee, lunchFee', 'length', 'max'=>5),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, name, provider, tourDuration, port, countryID, itinerary, description, services, summary, terms, contact, status, numNights, currency, address1, address2, zip, fax, contactName, cell, email1, email2, entranceFee, lunchFee', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'name' => 'Name',
			'provider' => 'Provider',
			'tourDuration' => 'Tour Duration',
			'port' => 'Port',
			'countryID' => 'Country',
			'itinerary' => 'Itinerary',
			'description' => 'Description',
			'services' => 'Services',
			'summary' => 'Summary',
			'terms' => 'Terms',
			'contact' => 'Contact',
			'status' => 'Status',
			'numNights' => 'Num Nights',
			'currency' => 'Currency',
			'address1' => 'Address1',
			'address2' => 'Address2',
			'zip' => 'Zip',
			'fax' => 'Fax',
			'contactName' => 'Contact Name',
			'cell' => 'Cell',
			'email1' => 'Email1',
			'email2' => 'Email2',
			'entranceFee' => 'Entrance Fee',
			'lunchFee' => 'Lunch Fee',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('provider',$this->provider);
		$criteria->compare('tourDuration',$this->tourDuration,true);
		$criteria->compare('port',$this->port,true);
		$criteria->compare('countryID',$this->countryID);
		$criteria->compare('itinerary',$this->itinerary,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('services',$this->services,true);
		$criteria->compare('summary',$this->summary,true);
		$criteria->compare('terms',$this->terms,true);
		$criteria->compare('contact',$this->contact,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('numNights',$this->numNights);
		$criteria->compare('currency',$this->currency);
		$criteria->compare('address1',$this->address1,true);
		$criteria->compare('address2',$this->address2,true);
		$criteria->compare('zip',$this->zip,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('contactName',$this->contactName,true);
		$criteria->compare('cell',$this->cell,true);
		$criteria->compare('email1',$this->email1,true);
		$criteria->compare('email2',$this->email2,true);
		$criteria->compare('entranceFee',$this->entranceFee,true);
		$criteria->compare('lunchFee',$this->lunchFee,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}