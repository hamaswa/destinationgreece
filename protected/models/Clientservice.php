<?php

/**
 * This is the model class for table "clientservice".
 *
 * The followings are the available columns in table 'clientservice':
 * @property integer $ID
 * @property integer $quoteID
 * @property string $date
 * @property integer $service
 * @property integer $city
 * @property integer $numOfNights
 * @property integer $hotel
 * @property integer $roomOccupency
 * @property integer $roomtype
 * @property integer $accomodationBasis
 * @property integer $rate
 * @property integer $transferFromTo
 * @property integer $transferPassengersNum
 * @property string $transferRate
 */
class Clientservice extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Clientservice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'clientservice';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('quoteID, service, city, numOfNights, hotel, roomOccupency, roomtype, accomodationBasis, rate, transferFromTo, transferPassengersNum', 'numerical', 'integerOnly'=>true),
			array('transferRate', 'length', 'max'=>255),
			array('date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, quoteID, date, service, city, numOfNights, hotel, roomOccupency, roomtype, accomodationBasis, rate, transferFromTo, transferPassengersNum, transferRate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'quoteID' => 'Quote',
			'date' => 'Date',
			'service' => 'Service',
			'city' => 'City',
			'numOfNights' => 'Num Of Nights',
			'hotel' => 'Hotel',
			'roomOccupency' => 'Room Occupency',
			'roomtype' => 'Roomtype',
			'accomodationBasis' => 'Accomodation Basis',
			'rate' => 'Rate',
			'transferFromTo' => 'Transfer From To',
			'transferPassengersNum' => 'Transfer Passengers Num',
			'transferRate' => 'Transfer Rate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('quoteID',$this->quoteID);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('service',$this->service);
		$criteria->compare('city',$this->city);
		$criteria->compare('numOfNights',$this->numOfNights);
		$criteria->compare('hotel',$this->hotel);
		$criteria->compare('roomOccupency',$this->roomOccupency);
		$criteria->compare('roomtype',$this->roomtype);
		$criteria->compare('accomodationBasis',$this->accomodationBasis);
		$criteria->compare('rate',$this->rate);
		$criteria->compare('transferFromTo',$this->transferFromTo);
		$criteria->compare('transferPassengersNum',$this->transferPassengersNum);
		$criteria->compare('transferRate',$this->transferRate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}