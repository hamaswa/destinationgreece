<?php

/**
 * This is the model class for table "coupon".
 *
 * The followings are the available columns in table 'coupon':
 * @property integer $ID
 * @property integer $packageID
 * @property string $fromDate
 * @property string $toDate
 * @property integer $value
 * @property string $valueUnit
 * @property integer $currency
 * @property integer $person
 * @property integer $status
 */
class Coupon extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Coupon the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'coupon';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status', 'required'),
			array('packageID, value, currency, person, status', 'numerical', 'integerOnly'=>true),
			array('valueUnit', 'length', 'max'=>255),
			array('fromDate, toDate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, packageID, fromDate, toDate, value, valueUnit, currency, person, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'packageID' => 'Package',
			'fromDate' => 'From Date',
			'toDate' => 'To Date',
			'value' => 'Value',
			'valueUnit' => 'Value Unit',
			'currency' => 'Currency',
			'person' => 'Person',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('packageID',$this->packageID);
		$criteria->compare('fromDate',$this->fromDate,true);
		$criteria->compare('toDate',$this->toDate,true);
		$criteria->compare('value',$this->value);
		$criteria->compare('valueUnit',$this->valueUnit,true);
		$criteria->compare('currency',$this->currency);
		$criteria->compare('person',$this->person);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}