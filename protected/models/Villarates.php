<?php

/**
 * This is the model class for table "villarates".
 *
 * The followings are the available columns in table 'villarates':
 * @property integer $ID
 * @property integer $villaid
 * @property string $from
 * @property string $to
 * @property integer $roomTypeID
 * @property string $netRate
 * @property string $dgCommission
 * @property string $ccHandlingpc
 * @property integer $status
 */
class Villarates extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Villarates the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'villarates';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('roomTypeID, status', 'required'),
			array('villaid, roomTypeID, status', 'numerical', 'integerOnly'=>true),
			array('netRate', 'length', 'max'=>10),
			array('dgCommission, ccHandlingpc', 'length', 'max'=>255),
			array('from, to', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, villaid, from, to, roomTypeID, netRate, dgCommission, ccHandlingpc, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'villaid' => 'Villaid',
			'from' => 'From',
			'to' => 'To',
			'roomTypeID' => 'Room Type',
			'netRate' => 'Net Rate',
			'dgCommission' => 'Dg Commission',
			'ccHandlingpc' => 'Cc Handlingpc',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('villaid',$this->villaid);
		$criteria->compare('from',$this->from,true);
		$criteria->compare('to',$this->to,true);
		$criteria->compare('roomTypeID',$this->roomTypeID);
		$criteria->compare('netRate',$this->netRate,true);
		$criteria->compare('dgCommission',$this->dgCommission,true);
		$criteria->compare('ccHandlingpc',$this->ccHandlingpc,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}