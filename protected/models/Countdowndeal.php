<?php

/**
 * This is the model class for table "countdowndeal".
 *
 * The followings are the available columns in table 'countdowndeal':
 * @property integer $ID
 * @property integer $packageID
 * @property integer $currentSale
 * @property string $fromDate
 * @property string $toDate
 * @property string $valueUnit
 * @property integer $currency
 * @property integer $status
 */
class Countdowndeal extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Countdowndeal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'countdowndeal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status', 'required'),
			array('packageID, currentSale, currency, status', 'numerical', 'integerOnly'=>true),
			array('valueUnit', 'length', 'max'=>255),
			array('fromDate, toDate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, packageID, currentSale, fromDate, toDate, valueUnit, currency, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'packageID' => 'Package',
			'currentSale' => 'Current Sale',
			'fromDate' => 'From Date',
			'toDate' => 'To Date',
			'valueUnit' => 'Value Unit',
			'currency' => 'Currency',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('packageID',$this->packageID);
		$criteria->compare('currentSale',$this->currentSale);
		$criteria->compare('fromDate',$this->fromDate,true);
		$criteria->compare('toDate',$this->toDate,true);
		$criteria->compare('valueUnit',$this->valueUnit,true);
		$criteria->compare('currency',$this->currency);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}