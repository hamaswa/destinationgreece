<?php

/**
 * This is the model class for table "combopackage".
 *
 * The followings are the available columns in table 'combopackage':
 * @property integer $ID
 * @property string $name
 * @property string $departsOn
 * @property integer $status
 * @property integer $packageType
 * @property string $extras
 * @property string $summary
 * @property string $map
 * @property string $terms
 */
class Combopackage extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Combopackage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'combopackage';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status', 'required'),
			array('status, packageType', 'numerical', 'integerOnly'=>true),
			array('name, extras, summary', 'length', 'max'=>255),
			array('departsOn, map, terms', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, name, departsOn, status, packageType, extras, summary, map, terms', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'name' => 'Name',
			'departsOn' => 'Departs On',
			'status' => 'Status',
			'packageType' => 'Package Type',
			'extras' => 'Extras',
			'summary' => 'Summary',
			'map' => 'Map',
			'terms' => 'Terms',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('departsOn',$this->departsOn,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('packageType',$this->packageType);
		$criteria->compare('extras',$this->extras,true);
		$criteria->compare('summary',$this->summary,true);
		$criteria->compare('map',$this->map,true);
		$criteria->compare('terms',$this->terms,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}