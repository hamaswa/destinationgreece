<?php

/**
 * This is the model class for table "agent".
 *
 * The followings are the available columns in table 'agent':
 * @property integer $agentID
 * @property string $agentName
 * @property string $title
 * @property string $agencyName
 * @property string $website
 * @property string $hostingAgency
 * @property string $hostingMemberNo
 * @property string $astaNum
 * @property string $cliaNum
 * @property string $iatanNum
 * @property string $arcNum
 * @property string $hotelsCommission
 * @property string $transfersCommision
 * @property string $ferriesCommision
 * @property string $hydroCommission
 * @property string $toursCommission
 * @property string $ceremonyCommission
 * @property string $shoresCommission
 * @property integer $excludeBranding
 * @property string $extracommission
 * @property string $tsb_datetimestamp
 * @property string $status
 * @property string $cruiseCommission
 * @property string $airCommission
 * @property string $packageCommission
 * @property string $sendPassword
 *
 * The followings are the available model relations:
 * @property User $agent
 */
class Agent extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Agent the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agent';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('agentID, hostingAgency, hostingMemberNo, excludeBranding', 'required'),
			array('agentID, excludeBranding', 'numerical', 'integerOnly'=>true),
			array('agentName, title, agencyName, website, hostingMemberNo, astaNum, cliaNum, iatanNum, arcNum, hotelsCommission, transfersCommision, ferriesCommision, hydroCommission, toursCommission, ceremonyCommission, shoresCommission, extracommission, status, cruiseCommission, airCommission, packageCommission, sendPassword', 'length', 'max'=>255),
			array('hostingAgency', 'length', 'max'=>512),
			array('tsb_datetimestamp', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('agentID, agentName, title, agencyName, website, hostingAgency, hostingMemberNo, astaNum, cliaNum, iatanNum, arcNum, hotelsCommission, transfersCommision, ferriesCommision, hydroCommission, toursCommission, ceremonyCommission, shoresCommission, excludeBranding, extracommission, tsb_datetimestamp, status, cruiseCommission, airCommission, packageCommission, sendPassword', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'agent' => array(self::BELONGS_TO, 'User', 'agentID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'agentID' => 'Agent',
			'agentName' => 'Agent Name',
			'title' => 'Title',
			'agencyName' => 'Agency Name',
			'website' => 'Website',
			'hostingAgency' => 'Hosting Agency',
			'hostingMemberNo' => 'Hosting Member No',
			'astaNum' => 'Asta Num',
			'cliaNum' => 'Clia Num',
			'iatanNum' => 'Iatan Num',
			'arcNum' => 'Arc Num',
			'hotelsCommission' => 'Hotels Commission',
			'transfersCommision' => 'Transfers Commision',
			'ferriesCommision' => 'Ferries Commision',
			'hydroCommission' => 'Hydro Commission',
			'toursCommission' => 'Tours Commission',
			'ceremonyCommission' => 'Ceremony Commission',
			'shoresCommission' => 'Shores Commission',
			'excludeBranding' => 'Exclude Branding',
			'extracommission' => 'Extracommission',
			'tsb_datetimestamp' => 'Tsb Datetimestamp',
			'status' => 'Status',
			'cruiseCommission' => 'Cruise Commission',
			'airCommission' => 'Air Commission',
			'packageCommission' => 'Package Commission',
			'sendPassword' => 'Send Password',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('agentID',$this->agentID);
		$criteria->compare('agentName',$this->agentName,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('agencyName',$this->agencyName,true);
		$criteria->compare('website',$this->website,true);
		$criteria->compare('hostingAgency',$this->hostingAgency,true);
		$criteria->compare('hostingMemberNo',$this->hostingMemberNo,true);
		$criteria->compare('astaNum',$this->astaNum,true);
		$criteria->compare('cliaNum',$this->cliaNum,true);
		$criteria->compare('iatanNum',$this->iatanNum,true);
		$criteria->compare('arcNum',$this->arcNum,true);
		$criteria->compare('hotelsCommission',$this->hotelsCommission,true);
		$criteria->compare('transfersCommision',$this->transfersCommision,true);
		$criteria->compare('ferriesCommision',$this->ferriesCommision,true);
		$criteria->compare('hydroCommission',$this->hydroCommission,true);
		$criteria->compare('toursCommission',$this->toursCommission,true);
		$criteria->compare('ceremonyCommission',$this->ceremonyCommission,true);
		$criteria->compare('shoresCommission',$this->shoresCommission,true);
		$criteria->compare('excludeBranding',$this->excludeBranding);
		$criteria->compare('extracommission',$this->extracommission,true);
		$criteria->compare('tsb_datetimestamp',$this->tsb_datetimestamp,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('cruiseCommission',$this->cruiseCommission,true);
		$criteria->compare('airCommission',$this->airCommission,true);
		$criteria->compare('packageCommission',$this->packageCommission,true);
		$criteria->compare('sendPassword',$this->sendPassword,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}