<?php

/**
 * This is the model class for table "grouprequest".
 *
 * The followings are the available columns in table 'grouprequest':
 * @property integer $ID
 * @property string $firstName
 * @property string $middleName
 * @property string $lastName
 * @property string $email
 * @property string $phone
 * @property string $cellPhone
 * @property string $address
 * @property string $zip
 * @property integer $city
 * @property integer $state
 * @property integer $country
 * @property string $groupName
 * @property string $placestoVisit
 * @property string $departingUSon
 * @property string $returningtoUSon
 * @property integer $numOfAdultsTravelling
 * @property integer $numberOfChildren
 * @property integer $numOfSingleRooms
 * @property integer $numOfDoubleRooms
 * @property integer $numOfTripleRooms
 * @property integer $numOfQuadrupleRooms
 * @property integer $hotelCategory
 * @property integer $budget
 * @property string $additionalInfo
 */
class Grouprequest extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Grouprequest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'grouprequest';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('city, state, country, numOfAdultsTravelling, numberOfChildren, numOfSingleRooms, numOfDoubleRooms, numOfTripleRooms, numOfQuadrupleRooms, hotelCategory, budget', 'numerical', 'integerOnly'=>true),
			array('firstName, middleName, lastName, email, phone, cellPhone, address, zip, groupName, placestoVisit, additionalInfo', 'length', 'max'=>255),
			array('departingUSon, returningtoUSon', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, firstName, middleName, lastName, email, phone, cellPhone, address, zip, city, state, country, groupName, placestoVisit, departingUSon, returningtoUSon, numOfAdultsTravelling, numberOfChildren, numOfSingleRooms, numOfDoubleRooms, numOfTripleRooms, numOfQuadrupleRooms, hotelCategory, budget, additionalInfo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'firstName' => 'First Name',
			'middleName' => 'Middle Name',
			'lastName' => 'Last Name',
			'email' => 'Email',
			'phone' => 'Phone',
			'cellPhone' => 'Cell Phone',
			'address' => 'Address',
			'zip' => 'Zip',
			'city' => 'City',
			'state' => 'State',
			'country' => 'Country',
			'groupName' => 'Group Name',
			'placestoVisit' => 'Placesto Visit',
			'departingUSon' => 'Departing Uson',
			'returningtoUSon' => 'Returningto Uson',
			'numOfAdultsTravelling' => 'Num Of Adults Travelling',
			'numberOfChildren' => 'Number Of Children',
			'numOfSingleRooms' => 'Num Of Single Rooms',
			'numOfDoubleRooms' => 'Num Of Double Rooms',
			'numOfTripleRooms' => 'Num Of Triple Rooms',
			'numOfQuadrupleRooms' => 'Num Of Quadruple Rooms',
			'hotelCategory' => 'Hotel Category',
			'budget' => 'Budget',
			'additionalInfo' => 'Additional Info',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('firstName',$this->firstName,true);
		$criteria->compare('middleName',$this->middleName,true);
		$criteria->compare('lastName',$this->lastName,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('cellPhone',$this->cellPhone,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('zip',$this->zip,true);
		$criteria->compare('city',$this->city);
		$criteria->compare('state',$this->state);
		$criteria->compare('country',$this->country);
		$criteria->compare('groupName',$this->groupName,true);
		$criteria->compare('placestoVisit',$this->placestoVisit,true);
		$criteria->compare('departingUSon',$this->departingUSon,true);
		$criteria->compare('returningtoUSon',$this->returningtoUSon,true);
		$criteria->compare('numOfAdultsTravelling',$this->numOfAdultsTravelling);
		$criteria->compare('numberOfChildren',$this->numberOfChildren);
		$criteria->compare('numOfSingleRooms',$this->numOfSingleRooms);
		$criteria->compare('numOfDoubleRooms',$this->numOfDoubleRooms);
		$criteria->compare('numOfTripleRooms',$this->numOfTripleRooms);
		$criteria->compare('numOfQuadrupleRooms',$this->numOfQuadrupleRooms);
		$criteria->compare('hotelCategory',$this->hotelCategory);
		$criteria->compare('budget',$this->budget);
		$criteria->compare('additionalInfo',$this->additionalInfo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}