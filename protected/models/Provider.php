<?php

/**
 * This is the model class for table "provider".
 *
 * The followings are the available columns in table 'provider':
 * @property integer $providerID
 * @property string $providerName
 * @property string $title
 * @property string $agency
 * @property string $website
 * @property string $astaNum
 * @property string $iatanNum
 *
 * The followings are the available model relations:
 * @property User $provider
 */
class Provider extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Provider the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'provider';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('providerID, providerName', 'required'),
			array('providerID', 'numerical', 'integerOnly'=>true),
			array('providerName, title, agency, website, astaNum, iatanNum', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('providerID, providerName, title, agency, website, astaNum, iatanNum', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'provider' => array(self::BELONGS_TO, 'User', 'providerID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'providerID' => 'Provider',
			'providerName' => 'Provider Name',
			'title' => 'Title',
			'agency' => 'Agency',
			'website' => 'Website',
			'astaNum' => 'Asta Num',
			'iatanNum' => 'Iatan Num',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('providerID',$this->providerID);
		$criteria->compare('providerName',$this->providerName,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('agency',$this->agency,true);
		$criteria->compare('website',$this->website,true);
		$criteria->compare('astaNum',$this->astaNum,true);
		$criteria->compare('iatanNum',$this->iatanNum,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}