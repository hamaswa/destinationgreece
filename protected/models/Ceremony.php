<?php

/**
 * This is the model class for table "ceremony".
 *
 * The followings are the available columns in table 'ceremony':
 * @property integer $ID
 * @property string $name
 * @property integer $city
 * @property integer $providerID
 * @property string $providerEmail
 * @property string $providerPhone
 * @property integer $status
 * @property string $summary
 * @property string $description
 * @property string $rates
 * @property string $terms
 */
class Ceremony extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Ceremony the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ceremony';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status', 'required'),
			array('city, providerID, status', 'numerical', 'integerOnly'=>true),
			array('name, providerEmail, providerPhone', 'length', 'max'=>255),
			array('summary, description, rates, terms', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, name, city, providerID, providerEmail, providerPhone, status, summary, description, rates, terms', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'name' => 'Name',
			'city' => 'City',
			'providerID' => 'Provider',
			'providerEmail' => 'Provider Email',
			'providerPhone' => 'Provider Phone',
			'status' => 'Status',
			'summary' => 'Summary',
			'description' => 'Description',
			'rates' => 'Rates',
			'terms' => 'Terms',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('city',$this->city);
		$criteria->compare('providerID',$this->providerID);
		$criteria->compare('providerEmail',$this->providerEmail,true);
		$criteria->compare('providerPhone',$this->providerPhone,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('summary',$this->summary,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('rates',$this->rates,true);
		$criteria->compare('terms',$this->terms,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}