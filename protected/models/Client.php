<?php

/**
 * This is the model class for table "client".
 *
 * The followings are the available columns in table 'client':
 * @property integer $ID
 * @property string $salutation
 * @property string $firstName
 * @property string $lastName
 * @property string $paxNum
 * @property string $arrivalDate
 * @property string $departureDate
 * @property string $sellingPrice
 * @property integer $agent
 * @property string $notes
 * @property integer $editedBy
 * @property integer $deleted
 */
class Client extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Client the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'client';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('deleted', 'required'),
			array('agent, editedBy, deleted', 'numerical', 'integerOnly'=>true),
			array('salutation, firstName, lastName, paxNum, sellingPrice, notes', 'length', 'max'=>255),
			array('arrivalDate, departureDate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, salutation, firstName, lastName, paxNum, arrivalDate, departureDate, sellingPrice, agent, notes, editedBy, deleted', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'salutation' => 'Salutation',
			'firstName' => 'First Name',
			'lastName' => 'Last Name',
			'paxNum' => 'Pax Num',
			'arrivalDate' => 'Arrival Date',
			'departureDate' => 'Departure Date',
			'sellingPrice' => 'Selling Price',
			'agent' => 'Agent',
			'notes' => 'Notes',
			'editedBy' => 'Edited By',
			'deleted' => 'Deleted',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('salutation',$this->salutation,true);
		$criteria->compare('firstName',$this->firstName,true);
		$criteria->compare('lastName',$this->lastName,true);
		$criteria->compare('paxNum',$this->paxNum,true);
		$criteria->compare('arrivalDate',$this->arrivalDate,true);
		$criteria->compare('departureDate',$this->departureDate,true);
		$criteria->compare('sellingPrice',$this->sellingPrice,true);
		$criteria->compare('agent',$this->agent);
		$criteria->compare('notes',$this->notes,true);
		$criteria->compare('editedBy',$this->editedBy);
		$criteria->compare('deleted',$this->deleted);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}