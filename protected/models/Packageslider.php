<?php

/**
 * This is the model class for table "packageslider".
 *
 * The followings are the available columns in table 'packageslider':
 * @property integer $ID
 * @property integer $packageID
 * @property integer $packageDescription
 * @property string $photoFull
 * @property string $photoDescription
 * @property string $url
 * @property integer $order
 * @property integer $status
 * @property string $text
 */
class Packageslider extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Packageslider the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'packageslider';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status', 'required'),
			array('packageID, packageDescription, order, status', 'numerical', 'integerOnly'=>true),
			array('photoFull, photoDescription, url, text', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, packageID, packageDescription, photoFull, photoDescription, url, order, status, text', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'packageID' => 'Package',
			'packageDescription' => 'Package Description',
			'photoFull' => 'Photo Full',
			'photoDescription' => 'Photo Description',
			'url' => 'Url',
			'order' => 'Order',
			'status' => 'Status',
			'text' => 'Text',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('packageID',$this->packageID);
		$criteria->compare('packageDescription',$this->packageDescription);
		$criteria->compare('photoFull',$this->photoFull,true);
		$criteria->compare('photoDescription',$this->photoDescription,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('order',$this->order);
		$criteria->compare('status',$this->status);
		$criteria->compare('text',$this->text,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}