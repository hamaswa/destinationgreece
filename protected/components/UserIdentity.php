<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 * 
 * @author Destination Greece Developers <destinationgreece-dev@masoodware.com>
 */
class UserIdentity extends CUserIdentity {

  private $id;

  public function authenticate() {
    if (!isset($this->username)) {
      return !($this->errorCode = self::ERROR_USERNAME_INVALID);
    }

    if (!isset($this->password)) {
      return !($this->errorCode = self::ERROR_PASSWORD_INVALID);
    }

    foreach (array('email1', 'email2', 'phone1', 'phone2', 'cell1', 'cell2') as $i) {
      $found = User::model()->findByAttributes(array($i => $this->username));
      if ($found) {
        break;
      }
    }

    if ($found === null) {
      return !($this->errorCode = self::ERROR_USERNAME_INVALID);
    }

    if ($found->password != $this->password) {
      return !($this->errorCode = self::ERROR_PASSWORD_INVALID);
    }

    $this->id = $found->ID;

    // Cleanup this value to remove any potential confusion, because
    // we have overridden some default behavior in {@link WebUser::getName}
    $this->username = null;

    return !($this->errorCode = self::ERROR_NONE);
  }

  public function getId() {
    return $this->id;
  }

}
