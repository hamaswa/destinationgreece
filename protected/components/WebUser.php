<?php

/**
 * @author Destination Greece Developers <destinationgreece-dev@masoodware.com>
 */
class WebUser extends CWebUser {

  public function getName() {
    if (Yii::app()->user->isGuest) {
      return Yii::app()->user->guestName;
    } else {
      $u = User::model()->findByPk(Yii::app()->user->id);
      return $u->firstName . ' ' . $u->lastName;
    }
  }

  /**
   * Stripped down version of orignal {@link CWebUser::checkAccess} to
   * disable caching
   */
  public function checkAccess($operation, $params = array(), $allowCaching = false) {
    return Yii::app()->getAuthManager()->checkAccess($operation, $this->getId(), $params);
  }
}
