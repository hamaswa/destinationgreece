<?php

/**
 * @author Destination Greece Developers <destinationgreece-dev@masoodware.com>
 */
class AuthManager extends CAuthManager {

  public function addItemChild($itemName, $childName) {
    throw new CException('Not implemented');
  }

  public function assign($itemName, $userId, $bizRule = null, $data = null) {
    throw new CException('Not implemented');
  }

  public function checkAccess($itemName, $userId, $params = array()) {
    foreach ($this->getAuthItems(null, $userId) as $item) {
      if (!strcasecmp($item->getName(), trim($itemName))) {
        return true;
      }
    }

    return false;
  }

  public function clearAll() {
    throw new CException('Not implemented');
  }

  public function clearAuthAssignments() {
    throw new CException('Not implemented');
  }

  public function createAuthItem($name, $type, $description = '', $bizRule = null, $data = null) {
    throw new CException('Not implemented');
  }

  public function getAuthAssignment($itemName, $userId) {
    throw new CException('Not implemented');
  }

  public function getAuthAssignments($userId) {
    throw new CException('Not implemented');
  }

  public function getAuthItem($name) {
    throw new CException('Not implemented');
  }

  public function getAuthItems($type = null, $userId = 0) {
    $items = array();

    if ($type === null || $type === CAuthItem::TYPE_ROLE) {
      $found = null;

      $found = Admin::model()->findByPk($userId);
      if ($found) {
        $items[] = new CAuthItem($this, 'admin', CAuthItem::TYPE_ROLE);
      }

      $found = Operator::model()->findByPk($userId);
      if ($found) {
        $items[] = new CAuthItem($this, 'operator', CAuthItem::TYPE_ROLE);
      }

      $found = Agent::model()->findByPk($userId);
      if ($found) {
        $items[] = new CAuthItem($this, 'agent', CAuthItem::TYPE_ROLE);
      }
    }

    return $items;
  }

  public function getItemChildren($itemName) {
    throw new CException('Not implemented');
  }

  public function hasItemChild($itemName, $childName) {
    throw new CException('Not implemented');
  }

  public function isAssigned($itemName, $userId) {
    throw new CException('Not implemented');
  }

  public function removeAuthItem($name) {
    throw new CException('Not implemented');
  }

  public function removeItemChild($itemName, $childName) {
    throw new CException('Not implemented');
  }

  public function revoke($itemName, $userId) {
    throw new CException('Not implemented');
  }

  public function save() {
    throw new CException('Not implemented');
  }

  public function saveAuthAssignment($assignment) {
    throw new CException('Not implemented');
  }

  public function saveAuthItem($item, $oldName = null) {
    throw new CException('Not implemented');
  }

}
