<?php

/**
 * Sample initialization file
 * 
 * This file is loaded before anything else. You are free to execute
 * any PHP code here.
 * 
 * Create a copy of this file into same directory and name it 'initialize.php'
 */

// set the correct path to Yii framework
require_once('/path/to/yii-1.1.13/framework/yii.php');

// enable debug mode to see errors on web browser
defined('YII_DEBUG') or define('YII_DEBUG', true);

// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
