<?php
/* @var $this ShorexController */
/* @var $model Shorex */

$this->breadcrumbs=array(
	'Shorexes'=>array('index'),
	$model->name=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Shorex', 'url'=>array('index')),
	array('label'=>'Create Shorex', 'url'=>array('create')),
	array('label'=>'View Shorex', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Shorex', 'url'=>array('admin')),
);
?>

<h1>Update Shorex <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>