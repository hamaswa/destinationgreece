<?php
/* @var $this ShorexController */
/* @var $model Shorex */

$this->breadcrumbs=array(
	'Shorexes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Shorex', 'url'=>array('index')),
	array('label'=>'Manage Shorex', 'url'=>array('admin')),
);
?>

<div style="float:left;margin-left:15px;"><h3>Add Shorex</h3></div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>