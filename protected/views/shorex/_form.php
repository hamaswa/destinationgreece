<?php
/* @var $this ShorexController */
/* @var $model Shorex */
/* @var $form CActiveForm */
?>
<div class="fifteen columns hoteladd">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'shorex-form',
        'enableAjaxValidation' => false,
            ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="four columns">
        <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Ener the Shorex Name')); ?>
        <?php echo $form->error($model, 'name'); ?>
    </div>
    <div class="four columns">
        <?php echo $form->textField($model, 'numNights', array('size' => 60, 'maxlength' => 255, 'placeholder' => '# of Nights')); ?>
        <?php echo $form->error($model, 'numNights'); ?>
    </div>
<div class="four columns">
        <?php echo $form->textField($model, 'port', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Port')); ?>
        <?php echo $form->error($model, 'port'); ?>
    </div>
    <div class="four columns">
        <?php echo $form->textField($model, 'tourDuration', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Tour Duration')); ?>
        <?php echo $form->error($model, 'tourDuration'); ?>
    </div>
    <div class="four columns">  
        <input type="text" id="provider" name="" maxlength="255" size="60" placeholder="Provider Name" />
        <input type="hidden" id="providerID" name="Shorex[provider]"/>
        <?php echo $form->error($model, 'Shorex[provider]'); ?>
    </div>
     <div class="four columns">  
        <input type="text" id="country" name="" maxlength="255" size="60" placeholder="Country" />
        <input type="hidden" id="countryID" name="Shorex[countryID]"/>
        <?php echo $form->error($model, 'countryID'); ?>
    </div>
    <div class="four columns" >
        <?php
        $sql = 'SELECT ID,  name FROM currency order by name';
        $cmd = Yii::app()->db->createCommand($sql);
        $res = $cmd->queryAll();
        $arr = array();
        $arr[0] = 'Currency';
        foreach ($res as $cat) {
            $arr[$cat['ID']] = $cat['name'];
        }
        echo $form->dropDownList($model, 'currency', $arr, array('id' => 'currency', 'class' => 'three columns', 'style' => 'max-width:218px;float:none;margin-left:0'));
        ?>
        <?php echo $form->error($model, 'currency'); ?> 
    </div>

    <div class="four columns">
        <?php $arrStatus = array('' => 'Show the Shorex', 1 => 'Yes', 0 => 'No'); ?>
        <?php echo $form->dropdownList($model, 'status', $arrStatus, array()); ?>
        <?php echo $form->error($model, 'status'); ?>
    </div>
    <div class="clear"></div>
    <div class="four columns">
    <select multiple="multiple" name="tourtype" id="tourtype" style = 'max-width:218px;float:none;margin-left:0' class="four columns">
    <option>Tour Type 1</option>
    <option>Tour Type 2</option><option>Tour Type 2</option><option>Tour Type 2</option><option>Tour Type 2</option><option>Tour Type 2</option><option>Tour Type 2</option>
    </select>
    
   
    </div>
    <div class="clear"></div>

    <div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
        <ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header">
            <li class="ui-state-default ui-tabs-selected ui-state-active"><a  class="two columns" href="#rates">Rates</a></li>
            <li class="ui-state-default"><a class="two columns"  href="#itinerary">Itinerary</a></li>
            <li class="ui-state-default"><a class="two columns"  href="#summary">Summary</a></li>
            <li class="ui-state-default"><a class="two columns"  href="#services">Services</a></li>
            <li class="ui-state-default"><a class="two columns"  href="#photos">Photos</a></li>
            <li class="ui-state-default"><a  class="two columns" href="#terms">Terms</a></li>
            <li class="ui-state-default"><a class="two columns"  href="#contact">Contact</a></li>
        </ul>
        <div id="rates" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
                    <?php
                    echo $form->textField($model, 'entranceFee', array('size' => 60, 'maxlength' => 255, 'class'=>'three columns', 'placeholder' => 'Entrance Fee(if not included)'));
                    echo $form->error($model, 'entranceFee');
                    ?>
                    <?php
                    echo $form->textField($model, 'lunchFee', array('size' => 60, 'maxlength' => 255,  'class'=>'three columns', 'placeholder' => 'Lunch(es)(if not included)'));
                    echo $form->error($model, 'lunchFee');
                    ?>
			<div class="clear"></div>
            <div id="rate-div0">
                    <input type="text" name="fromDate0[]"  placeholder="From Date" class="fromDate two columns" />
                    <input type="text" name="toDate0[]"  placeholder="To Date" class="toDate two columns"/>
                <div class="remove shorexremove">
                    <input type="text" name="standard0[]" placeholder="Standard" class="one columns" />
                    <input type="text" name="sgl30[]" placeholder="3 * sgl" class="one columns" />
                    <input type="text" name="dbl30[]" placeholder="3 * dbl" class="one columns" />
                    <input type="text" name="sgl40[]" placeholder="4 * sgl" class="one columns" />
                    <input type="text" name="dbl40[]" placeholder="4 * dbl" class="one columns" />
                    <input type="text" name="sgl50[]" placeholder="5 * sgl" class="one columns" />
                    <input type="text" name="dbl50[]" placeholder="5 * dbl" class="one columns" />
                    <span class="buttonfix">&nbsp;</span>
                </div>
            </div>
            <span class="buttonaddrate"  onclick="addShorexRate('0');">
                <a class="button large_button add"><span></span>rate</a></span>
            <hr />
            <div id="roomtype-div"></div>
            <span style="cursor:pointer;float:right;margin-top:15px;" id="add-roomtype"><a class="button large_button add"><span></span>Dates</a></span> 
        </div>
        <div id="itinerary" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
            <div class="row"> <?php echo $form->textArea($model, 'itinerary', array('id' => 'itinerary', 'class' => 'ckeditor')); ?> 
                <?php echo $form->error($model, 'itinerary'); ?> 
            </div>
        </div>
         <div id="summary" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
            <div class="row"> <?php echo $form->textArea($model, 'summary', array('id' => 'summary', 'class' => 'ckeditor')); ?> 
                <?php echo $form->error($model, 'summary'); ?> 
            </div>
        </div>
         <div id="services" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
            <div class="row"> <?php echo $form->textArea($model, 'services', array('id' => 'services', 'class' => 'ckeditor')); ?> 
                <?php echo $form->error($model, 'services'); ?> 
            </div>
        </div>
        <div id="photos"  class="ui-tabs-panel ui-widget-content ui-corner-bottom">
            <div id="photo-div"> </div>
            <div id="diplay-photo">
                <ul id="photo-list">


                </ul>
            </div>
            <span class="clear"></span>
            <?php
            $this->widget('ext.coco.CocoWidget'
                    , array(
                'id' => 'cocowidget1',
                'onCompleted' => "function(id,filename,jsoninfo){photoUpdate(jsoninfo,'static/uploads/shorex/')}",
                'onCancelled' => 'function(id,filename){ alert("cancelled"); }',
                'onMessage' => 'function(m){ alert(m); }',
                'allowedExtensions' => array('jpeg', 'jpg', 'gif', 'png'), // server-side mime-type validated
                'sizeLimit' => 2000000, // limit in server-side and in client-side
                'uploadDir' => 'static/uploads/shorex', // coco will @mkdir it
                'receptorClassName' => 'application.models.Photo',
                'methodName' => 'photoUploader',
                'maxUploads' => -1, // defaults to -1 (unlimited)
                'maxUploadsReachMessage' => 'No more files allowed', // if empty, no message is shown
                'multipleFileSelection' => true, // true or false, defaults: true
            ));
            ?>
        </div>           
        <div id="terms"  class="ui-tabs-panel ui-widget-content ui-corner-bottom"> 
            <?php echo $form->textArea($model, 'terms', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Terms', 'id' => 'hterms', 'class' => 'ckeditor')); ?> <?php echo $form->error($model, 'terms'); ?> 
        </div>
        <div id="contact"  class="ui-tabs-panel ui-widget-content ui-corner-bottom">
            <div class="eleven columns">                  
                <div class="four columns"  style="width: 220px; padding-top: 0px;" >
                    <?php
                    echo $form->textField($model, 'email1', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Email 1'));
                    echo $form->error($model, 'email1');
                    ?>
                </div>
                <div class="four columns"  style="width: 220px; padding-top: 0px;" >
                    <?php
                    echo $form->textField($model, 'email2', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Email 2'));
                    echo $form->error($model, 'email2');
                    ?>
                </div>
            </div>
            <div class="four columns"  style="width: 220px; padding-top: 0px;" >
                <?php
                echo $form->textField($model, 'zip', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Zip'));
                echo $form->error($model, 'zip');
                ?>
            </div>
            <div class="four columns"  style="width: 220px; padding-top: 0px;" >
                <?php
                echo $form->textField($model, 'fax', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Fax'));
                echo $form->error($model, 'fax');
                ?>
            </div>
            <div class="four columns"  style="width: 220px; padding-top: 0px;" >
                <?php
                echo $form->textField($model, 'contactName', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Contact Name'));
                echo $form->error($model, 'contactName');
                ?>
            </div>
            <div class="four columns"  style="width: 220px; padding-top: 0px;" >
                <?php
                echo $form->textField($model, 'cell', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Cell Phone'));
                echo $form->error($model, 'cell');
                ?>
            </div>
        </div>
    </div>


    <div class="clear"></div>
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Add Tour' : 'Edit Tour'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->

<script>
    $("#add-roomtype").live('click', function(){
        var numRoomTypes = $('.fromDate').length
        var str="";
        var clear="";

        $('input:text').each(function() {
            $(this).attr('value', $(this).val());
        });
		var del='<span class="buttonfix">&nbsp;</span>';
        $('#rate-div0 :input').not(':button,:hidden').each(function(){

            if($(this).attr("placeholder")=="Standard"){
                str+='<div class="remove shorexremove">'+clear+'<input type="text" name="standard'+numRoomTypes+'[]"  placeholder="Standard" class="one columns" />';
            }
            if($(this).attr("placeholder")=="3 * sgl"){
                str+='<input type="text" name="sgl3'+numRoomTypes+'[]"  placeholder="3 * sgl" class="one columns" />';
            }
            if($(this).attr("placeholder")=="3 * dbl"){
                str+='<input type="text" name="dbl3'+numRoomTypes+'[]"  placeholder="3 * dbl" class="one columns" />';
            }
            if($(this).attr("placeholder")=="4 * sgl"){
                str+='<input type="text" name="sgl4'+numRoomTypes+'[]"  placeholder="4 * sgl" class="one columns" />';
            }
            if($(this).attr("placeholder")=="4 * dbl"){
                str+='<input type="text" name="dbl4'+numRoomTypes+'[]"  placeholder="4 * dbl" class="one columns" />';
            }
            if($(this).attr("placeholder")=="5 * sgl"){
                str+='<input type="text" name="sgl5'+numRoomTypes+'[]"  placeholder="5 * sgl" class="one columns" />';
            }
            if($(this).attr("placeholder")=="5 * dbl"){
                str+='<input type="text" name="dbl5'+numRoomTypes+'[]"  placeholder="5 * dbl" class="one columns" />'+del+'</div>';
			del='<span class="buttonclear"></span><span class="removerate" id="removethis"><a class="button large_button delete"><span></span>rate</a></span>';
            clear='<div class="clear"></div>';
            }
        });
		
        var html='<div class="removeroomtype"><div class="clear"></div>';
      //  html+='<input class="roomtype two columns"  type="text" placeholder="Type of shorex" name="shorextype[]">';
        html+='<div id="rate-div'+numRoomTypes+'"><input type="text" value="'+ $(this).val() +'" name="fromDate[]"  placeholder="From Date" class="fromDate two columns" /><input type="text"  value="'+ $(this).val() +'" name="toDate[]"  placeholder="To Date" class="toDate two columns" />' + str + '</div>';
        html+='<span class="buttonaddrate"  onclick="addShorexRate(\''+numRoomTypes+'\');"><a class="button large_button add"><span></span>rate</a></span><span style="cursor:pointer;clear: both;float: right;margin-bottom: 10px;" id="removeroomtype"><a class="button large_button delete"><span></span>Dates</a></span><hr></div>';

        $("#roomtype-div").append(html);
    });
    var rangeDate=0;
    $('body').on('focus',".fromDate", function(){
        if($(this).parent('.remove').prev('.remove').find(".toDate").val()){
            var prvDate=$(this).parent('.remove').prev('.remove').find(".toDate").val();
            rangeDate = new Date(prvDate);
            rangeDate.setDate(rangeDate.getDate() + 1);
        }
        $(this).datepicker({
            dateFormat: 'yy-mm-dd',
            minDate:rangeDate,
            onSelect: function(dateStr) {
                var nextDayDate = new Date(dateStr);
                nextDayDate.setDate(nextDayDate.getDate() + 1);
                $(this).next('.toDate').datepicker({
                    dateFormat: 'yy-mm-dd',
                    minDate: nextDayDate
                }).datepicker('setDate', nextDayDate);
            }
        });
    });  
    $('#removethis').live('click',function(e){
        $(this).parents('.remove').remove();

    });
    $('#removeroomtype').live('click',function(e){
        $(this).parents('.removeroomtype').remove();

    });


</script>



<script>
    $('body').on('focus',".roomtype", function(){
        $(this).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "<?php echo Yii::app()->request->baseUrl; ?>/autocom/tourtype/",
                    dataType: "json",
                    data: {
                        featureClass: "P",
                        style: "full",
                        maxRows: 12,
                        term: request.term
                    },
                    success: function( data ) {
                        response( $.map( data, function( item ) {
                            return {
                                label: item.name,
                                value: item.id
                            }
                        }));
                    }
                });
            },
            minLength: 2,
            select: function (event, ui) {
                $('#'+$(this).attr('name')).val(ui.item.value);
                // update what is displayed in the textbox
                this.value = ui.item.label; 
                return false;
            }
        });
    });
</script> 
<script>
    $().ready(function(){
        $("#provider").autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "<?php echo Yii::app()->request->baseUrl; ?>/autocom/provider/",
                    dataType: "json",
                    data: {
                        featureClass: "P",
                        style: "full",
                        maxRows: 12,
                        term: request.term
                    },
                    success: function( data ) {
                        response( $.map( data, function( item ) {
                            return {
                                label: item.item,
                                value: item.id
                            }
                        }));
                    }
                });
            },
            minLength: 2,
            select: function (event, ui) {
                //alert(ui.item.value);
                $(this).val(ui.item.label);
                // update what is displayed in the textbox
                $("#providerID").val(ui.item.value); 
                return false;
            }
        });
    });
</script>
<script>
    $().ready(function(){
        $("#country").autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "<?php echo Yii::app()->request->baseUrl; ?>/autocom/country/",
                    dataType: "json",
                    data: {
                        featureClass: "P",
                        style: "full",
                        maxRows: 12,
                        term: request.term
                    },
                    success: function( data ) {
                        response( $.map( data, function( item ) {
                            return {
                                label: item.item,
                                value: item.id
                            }
                        }));
                    }
                });
            },
            minLength: 2,
            select: function (event, ui) {
                //alert(ui.item.value);
                $(this).val(ui.item.label);
                // update what is displayed in the textbox
                $("#countryID").val(ui.item.value); 
                return false;
            }
        });
    });
</script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/ajaxupload.3.5.js" ></script> 
