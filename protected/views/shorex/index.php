<?php
/* @var $this ShorexController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Shorexes',
);

$this->menu=array(
	array('label'=>'Create Shorex', 'url'=>array('create')),
	array('label'=>'Manage Shorex', 'url'=>array('admin')),
);
?>

<h1>Shorexes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
