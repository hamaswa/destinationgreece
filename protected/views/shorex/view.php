<?php
/* @var $this ShorexController */
/* @var $model Shorex */

$this->breadcrumbs=array(
	'Shorexes'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Shorex', 'url'=>array('index')),
	array('label'=>'Create Shorex', 'url'=>array('create')),
	array('label'=>'Update Shorex', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Shorex', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Shorex', 'url'=>array('admin')),
);
?>

<h1>View Shorex #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'name',
		'provider',
		'tourDuration',
		'port',
		'countryID',
		'itinerary',
		'description',
		'terms',
		'contact',
		'status',
		'numNights',
		'currency',
	),
)); ?>
