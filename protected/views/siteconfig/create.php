<?php
/* @var $this SiteconfigController */
/* @var $model Siteconfig */

$this->breadcrumbs=array(
	'Siteconfigs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Siteconfig', 'url'=>array('index')),
	array('label'=>'Manage Siteconfig', 'url'=>array('admin')),
);
?>

<h1>Create Siteconfig</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>