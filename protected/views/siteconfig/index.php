<?php
/* @var $this SiteconfigController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Siteconfigs',
);

$this->menu=array(
	array('label'=>'Create Siteconfig', 'url'=>array('create')),
	array('label'=>'Manage Siteconfig', 'url'=>array('admin')),
);
?>

<h1>Siteconfigs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
