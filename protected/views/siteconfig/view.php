<?php
/* @var $this SiteconfigController */
/* @var $model Siteconfig */

$this->breadcrumbs=array(
	'Siteconfigs'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Siteconfig', 'url'=>array('index')),
	array('label'=>'Create Siteconfig', 'url'=>array('create')),
	array('label'=>'Update Siteconfig', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Siteconfig', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Siteconfig', 'url'=>array('admin')),
);
?>

<h1>View Siteconfig #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'option',
	),
)); ?>
