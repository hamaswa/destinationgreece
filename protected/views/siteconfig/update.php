<?php
/* @var $this SiteconfigController */
/* @var $model Siteconfig */

$this->breadcrumbs=array(
	'Siteconfigs'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Siteconfig', 'url'=>array('index')),
	array('label'=>'Create Siteconfig', 'url'=>array('create')),
	array('label'=>'View Siteconfig', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Siteconfig', 'url'=>array('admin')),
);
?>

<h1>Update Siteconfig <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>