<?php
/* @var $this NongreecepackageController */
/* @var $data Nongreecepackage */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('departson')); ?>:</b>
	<?php echo CHtml::encode($data->departson); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('visiting')); ?>:</b>
	<?php echo CHtml::encode($data->visiting); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('packageBasis')); ?>:</b>
	<?php echo CHtml::encode($data->packageBasis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numNights')); ?>:</b>
	<?php echo CHtml::encode($data->numNights); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numDays')); ?>:</b>
	<?php echo CHtml::encode($data->numDays); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('currency')); ?>:</b>
	<?php echo CHtml::encode($data->currency); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('summary')); ?>:</b>
	<?php echo CHtml::encode($data->summary); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dayToDayItenirary')); ?>:</b>
	<?php echo CHtml::encode($data->dayToDayItenirary); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('servicesIncluded')); ?>:</b>
	<?php echo CHtml::encode($data->servicesIncluded); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('map')); ?>:</b>
	<?php echo CHtml::encode($data->map); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datesAndRates')); ?>:</b>
	<?php echo CHtml::encode($data->datesAndRates); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('terms')); ?>:</b>
	<?php echo CHtml::encode($data->terms); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('entraceFee')); ?>:</b>
	<?php echo CHtml::encode($data->entraceFee); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lunch')); ?>:</b>
	<?php echo CHtml::encode($data->lunch); ?>
	<br />

	*/ ?>

</div>