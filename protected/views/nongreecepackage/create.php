<?php
/* @var $this NongreecepackageController */
/* @var $model Nongreecepackage */

$this->breadcrumbs=array(
	'Nongreecepackages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Nongreecepackage', 'url'=>array('index')),
	array('label'=>'Manage Nongreecepackage', 'url'=>array('admin')),
);
?>

<h1>Create Nongreecepackage</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>