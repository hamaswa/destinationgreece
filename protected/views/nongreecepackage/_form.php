<?php
/* @var $this NongreecepackageController */
/* @var $model Nongreecepackage */
/* @var $form CActiveForm */
?>

<div class="fifteen columns  form hoteladd">


    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'nongreecepackage-form',
        'enableAjaxValidation' => false,
            ));
    ?>


    <?php echo $form->errorSummary($model); ?>

    <div class="four columns" style="width:220px;">
        <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Please Enter a non-greece package')); ?>
        <?php echo $form->error($model, 'name'); ?>
    </div>

    <div class="four columns" style="width:220px;">
        <?php echo $form->textField($model, 'departson', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Departs on Date')); ?>
        <?php echo $form->error($model, 'departson'); ?>
    </div>

    <div class="four columns" style="width:220px;">
        <?php echo $form->textField($model, 'visiting', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Visiting')); ?>
        <?php echo $form->error($model, 'visiting'); ?>
    </div>

    <div class="four columns" style="width:220px;">
        <?php echo $form->textField($model, 'packageBasis', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Package Basis')); ?>
        <?php echo $form->error($model, 'packageBasis'); ?>
    </div>

    <div class="four columns" style="width:220px;">
        <?php echo $form->textField($model, 'numNights', array('placeholder' => '# of Nights')); ?>
        <?php echo $form->error($model, 'numNights'); ?>
    </div>

    <div class="four columns" style="width:220px;">
        <?php echo $form->textField($model, 'numDays', array('placeholder' => '# of Days')); ?>
        <?php echo $form->error($model, 'numDays'); ?>
    </div>

    <div class="four columns" style="width:220px;">
        <?php
        $sql = 'SELECT ID,  name FROM currency order by name';
        $cmd = Yii::app()->db->createCommand($sql);
        $res = $cmd->queryAll();
        $arr = array();
        $arr[0] = 'Rates in';
        foreach ($res as $cat) {
            $arr[$cat['ID']] = $cat['name'];
        }
        echo $form->dropDownList($model, 'currency', $arr, array('id' => 'currency',  'style' => 'max-width:218px;float:none;'));
        ?>
        <?php echo $form->error($model, 'currency'); ?>
    </div>

    <div class="four columns" style="width:220px;">
        <?php $arrStatus = array('' => 'Show the Package', 1 => 'Yes', 0 => 'No'); ?>
        <?php echo $form->dropdownList($model, 'status', $arrStatus, array()); ?>
        <?php echo $form->error($model, 'status'); ?>
    </div>




    <div class="clear"></div>
    <div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
        <ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header">
            <li class="ui-state-default"><a  class="two columns" href="#rates">Date and Rates</a></li>
            <li class="ui-state-default"><a  class="two columns" href="#summary">Summary</a></li>
            <li class="ui-state-default"><a  class="two columns"  href="#itinerary">Itinerary</a></li>
            <li class="ui-state-default"><a  class="two columns" href="#services">Services</a></li>
            <li class="ui-state-default"><a  class="two columns" href="#photos">Photos</a></li>
            <li class="ui-state-default"><a  class="two columns" href="#map">Map</a></li>
            <li class="ui-state-default"><a  class="two columns" href="#terms">Terms</a></li>
        </ul>

        <div id="summary" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
            <?php echo $form->textArea($model, 'summary', array('rows' => 6, 'cols' => 50)); ?>
            <?php echo $form->error($model, 'summary'); ?>
        </div>
        <div id="itinerary" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
            <div class="row"> 
                <?php echo $form->textArea($model, 'dayToDayItenirary', array('id' => 'hdescription', 'class' => 'ckeditor')); ?> 
                <?php echo $form->error($model, 'dayToDayItenirary'); ?> 
            </div>
        </div>         
        <div id="services" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
            <?php echo $form->textArea($model, 'servicesIncluded', array('rows' => 6, 'cols' => 50)); ?>
            <?php echo $form->error($model, 'servicesIncluded'); ?>
        </div>
        <div id="photos" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">

            <?php
            $this->widget('ext.coco.CocoWidget'
                    , array(
                'id' => 'cocowidget1',
                'onCompleted' => "function(id,filename,jsoninfo){photoUpdate(jsoninfo,'static/uploads/greecpackage/')}",
                'onCancelled' => 'function(id,filename){ alert("cancelled"); }',
                'onMessage' => 'function(m){ alert(m); }',
                'allowedExtensions' => array('jpeg', 'jpg', 'gif', 'png'), // server-side mime-type validated
                'sizeLimit' => 2000000, // limit in server-side and in client-side
                'uploadDir' => 'static/uploads/greecpackage', // coco will @mkdir it
                // this arguments are used to send a notification
                // on a specific class when a new file is uploaded,
                'receptorClassName' => 'application.models.Photo',
                'methodName' => 'photoUploader',
                //'userdata' => $model->primaryKey,
                // controls how many files must be uploaded
                // 'defaultControllerName' => 'photo',
                // 'defaultActionName' => 'upload',
                'maxUploads' => -1, // defaults to -1 (unlimited)
                'maxUploadsReachMessage' => 'No more files allowed', // if empty, no message is shown
                // controls how many files the can select (not upload, for uploads see also: maxUploads)
                'multipleFileSelection' => true, // true or false, defaults: true
            ));
            ?>

        </div>
        <div id="map" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
            <?php echo $form->textArea($model, 'map', array('rows' => 6, 'cols' => 50)); ?>
            <?php echo $form->error($model, 'map'); ?>

        </div>
        <div id="terms"  class="ui-tabs-panel ui-widget-content ui-corner-bottom"> 
            <?php echo $form->textArea($model, 'terms', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Terms', 'id' => 'hterms', 'class' => 'ckeditor')); ?> 
            <?php echo $form->error($model, 'terms'); ?> 
        </div>
        <div id="rates" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
            <div class="four columns">
                <?php echo $form->textField($model, 'entraceFee', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Entrance Fee')); ?>
                <?php echo $form->error($model, 'entraceFee'); ?>
            </div>

            <div class="four columns">
                <?php echo $form->textField($model, 'lunch', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Lunch')); ?>
                <?php echo $form->error($model, 'lunch'); ?>
            </div>
            <div class="clear"></div>
            <div id="rate-div">
                <div class="remove">
                    <input type="text" name="fromDate[]"  placeholder="From Date" class="fromDate two columns" />
                    <input type="text" name="toDate[]"  placeholder="To Date" class="toDate two columns"/>
                    <div class="shorexremove"><input type="text" name="sgl4[]" placeholder="3 * Sgl" class="one columns" />
                    <input type="text" name="dbl3[]" placeholder="3 * Dbl" class="one columns" />
                    <input type="text" name="trp3[]" placeholder="3 * Trp" class="one columns" />
                    <input type="text" name="sgl4[]" placeholder="4 * Sgl" class="one columns" />
                    <input type="text" name="dbl4[]" placeholder="4 * Dbl" class="one columns" />
                    <input type="text" name="trp4[]" placeholder="4 * Trp" class="one columns" /><div class="clear"></div>
                    <input type="text" name="sgl5[]" placeholder="5 * Sgl" class="one columns" />
                    <input type="text" name="dbl5[]" placeholder="5 * Dbl" class="one columns" />
                    <input type="text" name="trp5[]" placeholder="5 * Trp" class="one columns" />
                    <input type="text" name="sgldl[]" placeholder="Deluxe Sgl" class="one columns" />
                    <input type="text" name="dbldl[]" placeholder="Deluxe Dbl" class="one columns" />
                    <input type="text" name="trpdl[]" placeholder="Deluxe Trp" class="one columns" /></div>
                    <span class="buttonfix">&nbsp;</span>
                </div>
            </div>            
            <span class="buttonaddrate"  id="add-rate"><a class="button large_button add"><span></span>Rate</a></span> 
        </div>
        <hr class="end-break">
    </div>
    <div class="clear">&nbsp;</div>


    <div class="clear"></div>
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $("#add-rate").live('click', function(){
        var numRoomTypes = $('.roomtype').length
        var str="";
        var clear="";

        $('input:text').each(function() {
            $(this).attr('value', $(this).val());
        });

        str='<div><div class="clear"></div><hr class="end-break">'
        str+='<input type="text" name="fromDate[]"  placeholder="From Date" class="fromDate two columns" />';
        str+='<input type="text" name="toDate[]"  placeholder="To Date" class="toDate two columns"/>';
        str+='<div class="shorexremove"><input type="text" name="sgl4[]" placeholder="3 * Sgl" class="one columns" />';
        str+='<input type="text" name="dbl3[]" placeholder="3 * Dbl" class="one columns" />';
        str+='<input type="text" name="trp3[]" placeholder="3 * Trp" class="one columns" />';
        str+='<input type="text" name="sgl4[]" placeholder="4 * Sgl" class="one columns" />';
        str+='<input type="text" name="dbl4[]" placeholder="4 * Dbl" class="one columns" />';
        str+='<input type="text" name="trp4[]" placeholder="4 * Trp" class="one columns" /><div class="clear"></div>';
        str+='<input type="text" name="sgl5[]" placeholder="5 * Sgl" class="one columns" />';
        str+='<input type="text" name="dbl5[]" placeholder="5 * Dbl" class="one columns" />';
        str+='<input type="text" name="trp5[]" placeholder="5 * Trp" class="one columns" />';
        str+='<input type="text" name="sgldl[]" placeholder="Deluxe Sgl" class="one columns" />';
        str+='<input type="text" name="dbldl[]" placeholder="Deluxe Dbl" class="one columns" />';
        str+='<input type="text" name="trpdl[]" placeholder="Deluxe Trp" class="one columns" /></div>';
        str+='<span class="removerate" id="removethis">';
        str+='<a class="button large_button delete"><span></span>Rate</a></span></div>';
        $("#rate-div").append(str);
    });
    var rangeDate=0;
    $('body').on('focus',".fromDate", function(){
        if($(this).parent('.remove').prev('.remove').find(".toDate").val()){
            var prvDate=$(this).parent('.remove').prev('.remove').find(".toDate").val();
            rangeDate = new Date(prvDate);
            rangeDate.setDate(rangeDate.getDate() + 1);
        }
        $(this).datepicker({
            dateFormat: 'yy-mm-dd',
            minDate:rangeDate,
            onSelect: function(dateStr) {
                var nextDayDate = new Date(dateStr);
                nextDayDate.setDate(nextDayDate.getDate() + 1);
                $(this).next('.toDate').datepicker({
                    dateFormat: 'yy-mm-dd',
                    minDate: nextDayDate
                }).datepicker('setDate', nextDayDate);
            }
        });
    });

    $('#removerate').live('click',function(e){
        $(this).parent('div').remove();

    });


</script>