<?php
/* @var $this NongreecepackageController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Nongreecepackages',
);

$this->menu=array(
	array('label'=>'Create Nongreecepackage', 'url'=>array('create')),
	array('label'=>'Manage Nongreecepackage', 'url'=>array('admin')),
);
?>

<h1>Nongreecepackages</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
