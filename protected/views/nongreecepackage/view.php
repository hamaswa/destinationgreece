<?php
/* @var $this NongreecepackageController */
/* @var $model Nongreecepackage */

$this->breadcrumbs=array(
	'Nongreecepackages'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Nongreecepackage', 'url'=>array('index')),
	array('label'=>'Create Nongreecepackage', 'url'=>array('create')),
	array('label'=>'Update Nongreecepackage', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Nongreecepackage', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Nongreecepackage', 'url'=>array('admin')),
);
?>

<h1>View Nongreecepackage #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'name',
		'departson',
		'visiting',
		'packageBasis',
		'numNights',
		'numDays',
		'currency',
		'status',
		'summary',
		'dayToDayItenirary',
		'servicesIncluded',
		'map',
		'datesAndRates',
		'terms',
		'entraceFee',
		'lunch',
	),
)); ?>
