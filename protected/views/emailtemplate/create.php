<?php
/* @var $this EmailtemplateController */
/* @var $model Emailtemplate */

$this->breadcrumbs=array(
	'Emailtemplates'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Emailtemplate', 'url'=>array('index')),
	array('label'=>'Manage Emailtemplate', 'url'=>array('admin')),
);
?>

<h1>Create Emailtemplate</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>