<?php
/* @var $this EmailtemplateController */
/* @var $model Emailtemplate */

$this->breadcrumbs=array(
	'Emailtemplates'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Emailtemplate', 'url'=>array('index')),
	array('label'=>'Create Emailtemplate', 'url'=>array('create')),
	array('label'=>'Update Emailtemplate', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Emailtemplate', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Emailtemplate', 'url'=>array('admin')),
);
?>

<h1>View Emailtemplate #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'template',
		'type',
	),
)); ?>
