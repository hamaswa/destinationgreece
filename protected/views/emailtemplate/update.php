<?php
/* @var $this EmailtemplateController */
/* @var $model Emailtemplate */

$this->breadcrumbs=array(
	'Emailtemplates'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Emailtemplate', 'url'=>array('index')),
	array('label'=>'Create Emailtemplate', 'url'=>array('create')),
	array('label'=>'View Emailtemplate', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Emailtemplate', 'url'=>array('admin')),
);
?>

<h1>Update Emailtemplate <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>