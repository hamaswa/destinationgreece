<?php
/* @var $this EmailtemplateController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Emailtemplates',
);

$this->menu=array(
	array('label'=>'Create Emailtemplate', 'url'=>array('create')),
	array('label'=>'Manage Emailtemplate', 'url'=>array('admin')),
);
?>

<h1>Emailtemplates</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
