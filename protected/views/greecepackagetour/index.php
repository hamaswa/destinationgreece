<?php
/* @var $this GreecepackagetourController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Greecepackagetours',
);

$this->menu=array(
	array('label'=>'Create Greecepackagetour', 'url'=>array('create')),
	array('label'=>'Manage Greecepackagetour', 'url'=>array('admin')),
);
?>

<h1>Greecepackagetours</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
