<?php
/* @var $this GreecepackagetourController */
/* @var $model Greecepackagetour */

$this->breadcrumbs=array(
	'Greecepackagetours'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Greecepackagetour', 'url'=>array('index')),
	array('label'=>'Create Greecepackagetour', 'url'=>array('create')),
	array('label'=>'Update Greecepackagetour', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Greecepackagetour', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Greecepackagetour', 'url'=>array('admin')),
);
?>

<h1>View Greecepackagetour #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'gpID',
		'tour',
		'numOverNights',
	),
)); ?>
