<?php
/* @var $this GreecepackagetourController */
/* @var $data Greecepackagetour */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gpID')); ?>:</b>
	<?php echo CHtml::encode($data->gpID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tour')); ?>:</b>
	<?php echo CHtml::encode($data->tour); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numOverNights')); ?>:</b>
	<?php echo CHtml::encode($data->numOverNights); ?>
	<br />


</div>