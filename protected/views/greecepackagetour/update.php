<?php
/* @var $this GreecepackagetourController */
/* @var $model Greecepackagetour */

$this->breadcrumbs=array(
	'Greecepackagetours'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Greecepackagetour', 'url'=>array('index')),
	array('label'=>'Create Greecepackagetour', 'url'=>array('create')),
	array('label'=>'View Greecepackagetour', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Greecepackagetour', 'url'=>array('admin')),
);
?>

<h1>Update Greecepackagetour <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>