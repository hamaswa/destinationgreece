<?php
/* @var $this GreecepackagetourController */
/* @var $model Greecepackagetour */

$this->breadcrumbs=array(
	'Greecepackagetours'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Greecepackagetour', 'url'=>array('index')),
	array('label'=>'Manage Greecepackagetour', 'url'=>array('admin')),
);
?>

<h1>Create Greecepackagetour</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>