<?php
/* @var $this GreecepackagetourController */
/* @var $model Greecepackagetour */
/* @var $form CActiveForm */
?>

   <div class="eleven columns" style="margin-left:20% !important;margin-right:20% !important;">
      <h3 class="form-heading">Add Agent</h3>
      <div id="response"></div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'greecepackagetour-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="five columns">
		<?php echo $form->labelEx($model,'gpID'); ?>
		<?php echo $form->textField($model,'gpID'); ?>
		<?php echo $form->error($model,'gpID'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'tour'); ?>
		<?php echo $form->textField($model,'tour'); ?>
		<?php echo $form->error($model,'tour'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'numOverNights'); ?>
		<?php echo $form->textField($model,'numOverNights'); ?>
		<?php echo $form->error($model,'numOverNights'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->