<?php
/* @var $this GreecepackagetransferController */
/* @var $model Greecepackagetransfer */
/* @var $form CActiveForm */
?>

   <div class="eleven columns" style="margin-left:20% !important;margin-right:20% !important;">
      <h3 class="form-heading">Add Agent</h3>
      <div id="response"></div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'greecepackagetransfer-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="five columns">
		<?php echo $form->labelEx($model,'gpID'); ?>
		<?php echo $form->textField($model,'gpID'); ?>
		<?php echo $form->error($model,'gpID'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'transfer'); ?>
		<?php echo $form->textField($model,'transfer'); ?>
		<?php echo $form->error($model,'transfer'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'numOverNights'); ?>
		<?php echo $form->textField($model,'numOverNights'); ?>
		<?php echo $form->error($model,'numOverNights'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->