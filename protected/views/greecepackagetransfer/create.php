<?php
/* @var $this GreecepackagetransferController */
/* @var $model Greecepackagetransfer */

$this->breadcrumbs=array(
	'Greecepackagetransfers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Greecepackagetransfer', 'url'=>array('index')),
	array('label'=>'Manage Greecepackagetransfer', 'url'=>array('admin')),
);
?>

<h1>Create Greecepackagetransfer</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>