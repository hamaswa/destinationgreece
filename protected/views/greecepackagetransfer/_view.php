<?php
/* @var $this GreecepackagetransferController */
/* @var $data Greecepackagetransfer */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gpID')); ?>:</b>
	<?php echo CHtml::encode($data->gpID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('transfer')); ?>:</b>
	<?php echo CHtml::encode($data->transfer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numOverNights')); ?>:</b>
	<?php echo CHtml::encode($data->numOverNights); ?>
	<br />


</div>