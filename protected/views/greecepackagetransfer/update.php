<?php
/* @var $this GreecepackagetransferController */
/* @var $model Greecepackagetransfer */

$this->breadcrumbs=array(
	'Greecepackagetransfers'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Greecepackagetransfer', 'url'=>array('index')),
	array('label'=>'Create Greecepackagetransfer', 'url'=>array('create')),
	array('label'=>'View Greecepackagetransfer', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Greecepackagetransfer', 'url'=>array('admin')),
);
?>

<h1>Update Greecepackagetransfer <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>