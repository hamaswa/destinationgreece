<?php
/* @var $this GreecepackagetransferController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Greecepackagetransfers',
);

$this->menu=array(
	array('label'=>'Create Greecepackagetransfer', 'url'=>array('create')),
	array('label'=>'Manage Greecepackagetransfer', 'url'=>array('admin')),
);
?>

<h1>Greecepackagetransfers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
