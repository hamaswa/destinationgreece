<?php
/* @var $this GreecepackagetransferController */
/* @var $model Greecepackagetransfer */

$this->breadcrumbs=array(
	'Greecepackagetransfers'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Greecepackagetransfer', 'url'=>array('index')),
	array('label'=>'Create Greecepackagetransfer', 'url'=>array('create')),
	array('label'=>'Update Greecepackagetransfer', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Greecepackagetransfer', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Greecepackagetransfer', 'url'=>array('admin')),
);
?>

<h1>View Greecepackagetransfer #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'gpID',
		'transfer',
		'numOverNights',
	),
)); ?>
