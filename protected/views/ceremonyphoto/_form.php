<?php
/* @var $this CeremonyphotoController */
/* @var $model Ceremonyphoto */
/* @var $form CActiveForm */
?>

   <div class="eleven columns" style="margin-left:20% !important;margin-right:20% !important;">
      <h3 class="form-heading">Add Agent</h3>
      <div id="response"></div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ceremonyphoto-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="five columns">
		<?php echo $form->labelEx($model,'ID'); ?>
		<?php echo $form->textField($model,'ID'); ?>
		<?php echo $form->error($model,'ID'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'photoid'); ?>
		<?php echo $form->textField($model,'photoid'); ?>
		<?php echo $form->error($model,'photoid'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->