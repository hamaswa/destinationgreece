<?php
/* @var $this CeremonyphotoController */
/* @var $model Ceremonyphoto */

$this->breadcrumbs=array(
	'Ceremonyphotos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Ceremonyphoto', 'url'=>array('index')),
	array('label'=>'Manage Ceremonyphoto', 'url'=>array('admin')),
);
?>

<h1>Create Ceremonyphoto</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>