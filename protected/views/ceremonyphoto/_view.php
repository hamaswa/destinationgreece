<?php
/* @var $this CeremonyphotoController */
/* @var $data Ceremonyphoto */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('photoid')); ?>:</b>
	<?php echo CHtml::encode($data->photoid); ?>
	<br />


</div>