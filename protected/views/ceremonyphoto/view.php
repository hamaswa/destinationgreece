<?php
/* @var $this CeremonyphotoController */
/* @var $model Ceremonyphoto */

$this->breadcrumbs=array(
	'Ceremonyphotos'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Ceremonyphoto', 'url'=>array('index')),
	array('label'=>'Create Ceremonyphoto', 'url'=>array('create')),
	array('label'=>'Update Ceremonyphoto', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Ceremonyphoto', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Ceremonyphoto', 'url'=>array('admin')),
);
?>

<h1>View Ceremonyphoto #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'photoid',
	),
)); ?>
