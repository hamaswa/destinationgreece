<?php
/* @var $this CeremonyphotoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ceremonyphotos',
);

$this->menu=array(
	array('label'=>'Create Ceremonyphoto', 'url'=>array('create')),
	array('label'=>'Manage Ceremonyphoto', 'url'=>array('admin')),
);
?>

<h1>Ceremonyphotos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
