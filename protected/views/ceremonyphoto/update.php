<?php
/* @var $this CeremonyphotoController */
/* @var $model Ceremonyphoto */

$this->breadcrumbs=array(
	'Ceremonyphotos'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Ceremonyphoto', 'url'=>array('index')),
	array('label'=>'Create Ceremonyphoto', 'url'=>array('create')),
	array('label'=>'View Ceremonyphoto', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Ceremonyphoto', 'url'=>array('admin')),
);
?>

<h1>Update Ceremonyphoto <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>