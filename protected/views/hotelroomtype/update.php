<?php
/* @var $this HotelroomtypeController */
/* @var $model Hotelroomtype */

$this->breadcrumbs=array(
	'Hotelroomtypes'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Hotelroomtype', 'url'=>array('index')),
	array('label'=>'Create Hotelroomtype', 'url'=>array('create')),
	array('label'=>'View Hotelroomtype', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Hotelroomtype', 'url'=>array('admin')),
);
?>

<h1>Update Hotelroomtype <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>