<?php
/* @var $this HotelroomtypeController */
/* @var $model Hotelroomtype */

$this->breadcrumbs=array(
	'Hotelroomtypes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Hotelroomtype', 'url'=>array('index')),
	array('label'=>'Manage Hotelroomtype', 'url'=>array('admin')),
);
?>

<h1>Create Hotelroomtype</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>