<?php
/* @var $this HotelroomtypeController */
/* @var $data Hotelroomtype */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hotelID')); ?>:</b>
	<?php echo CHtml::encode($data->hotelID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('roomType')); ?>:</b>
	<?php echo CHtml::encode($data->roomType); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numAdults')); ?>:</b>
	<?php echo CHtml::encode($data->numAdults); ?>
	<br />


</div>