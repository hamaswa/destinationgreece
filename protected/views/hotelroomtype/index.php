<?php
/* @var $this HotelroomtypeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Hotelroomtypes',
);

$this->menu=array(
	array('label'=>'Create Hotelroomtype', 'url'=>array('create')),
	array('label'=>'Manage Hotelroomtype', 'url'=>array('admin')),
);
?>

<h1>Hotelroomtypes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
