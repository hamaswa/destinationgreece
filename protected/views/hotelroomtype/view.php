<?php
/* @var $this HotelroomtypeController */
/* @var $model Hotelroomtype */

$this->breadcrumbs=array(
	'Hotelroomtypes'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Hotelroomtype', 'url'=>array('index')),
	array('label'=>'Create Hotelroomtype', 'url'=>array('create')),
	array('label'=>'Update Hotelroomtype', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Hotelroomtype', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Hotelroomtype', 'url'=>array('admin')),
);
?>

<h1>View Hotelroomtype #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'hotelID',
		'roomType',
		'numAdults',
	),
)); ?>
