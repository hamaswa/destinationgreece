<?php
/* @var $this HotelroomtypeController */
/* @var $model Hotelroomtype */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID'); ?>
		<?php echo $form->textField($model,'ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hotelID'); ?>
		<?php echo $form->textField($model,'hotelID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'roomType'); ?>
		<?php echo $form->textField($model,'roomType'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'numAdults'); ?>
		<?php echo $form->textField($model,'numAdults'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->