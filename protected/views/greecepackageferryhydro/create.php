<?php
/* @var $this GreecepackageferryhydroController */
/* @var $model Greecepackageferryhydro */

$this->breadcrumbs=array(
	'Greecepackageferryhydros'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Greecepackageferryhydro', 'url'=>array('index')),
	array('label'=>'Manage Greecepackageferryhydro', 'url'=>array('admin')),
);
?>

<h1>Create Greecepackageferryhydro</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>