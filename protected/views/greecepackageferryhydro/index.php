<?php
/* @var $this GreecepackageferryhydroController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Greecepackageferryhydros',
);

$this->menu=array(
	array('label'=>'Create Greecepackageferryhydro', 'url'=>array('create')),
	array('label'=>'Manage Greecepackageferryhydro', 'url'=>array('admin')),
);
?>

<h1>Greecepackageferryhydros</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
