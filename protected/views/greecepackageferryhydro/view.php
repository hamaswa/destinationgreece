<?php
/* @var $this GreecepackageferryhydroController */
/* @var $model Greecepackageferryhydro */

$this->breadcrumbs=array(
	'Greecepackageferryhydros'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Greecepackageferryhydro', 'url'=>array('index')),
	array('label'=>'Create Greecepackageferryhydro', 'url'=>array('create')),
	array('label'=>'Update Greecepackageferryhydro', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Greecepackageferryhydro', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Greecepackageferryhydro', 'url'=>array('admin')),
);
?>

<h1>View Greecepackageferryhydro #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'gpid',
		'transfer',
		'numOverNights',
	),
)); ?>
