<?php
/* @var $this GreecepackageferryhydroController */
/* @var $model Greecepackageferryhydro */

$this->breadcrumbs=array(
	'Greecepackageferryhydros'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Greecepackageferryhydro', 'url'=>array('index')),
	array('label'=>'Create Greecepackageferryhydro', 'url'=>array('create')),
	array('label'=>'View Greecepackageferryhydro', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Greecepackageferryhydro', 'url'=>array('admin')),
);
?>

<h1>Update Greecepackageferryhydro <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>