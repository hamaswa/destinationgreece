<?php
/* @var $this GreecepackageferryhydroController */
/* @var $model Greecepackageferryhydro */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID'); ?>
		<?php echo $form->textField($model,'ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'gpid'); ?>
		<?php echo $form->textField($model,'gpid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'transfer'); ?>
		<?php echo $form->textField($model,'transfer'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'numOverNights'); ?>
		<?php echo $form->textField($model,'numOverNights'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->