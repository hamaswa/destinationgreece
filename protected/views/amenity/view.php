<?php
/* @var $this AmenityController */
/* @var $model Amenity */

$this->breadcrumbs=array(
	'Amenities'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Amenity', 'url'=>array('index')),
	array('label'=>'Create Amenity', 'url'=>array('create')),
	array('label'=>'Update Amenity', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Amenity', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Amenity', 'url'=>array('admin')),
);
?>

<h1>View Amenity #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'name',
	),
)); ?>
