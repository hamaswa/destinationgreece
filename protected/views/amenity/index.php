<?php
/* @var $this AmenityController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Amenities',
);

$this->menu=array(
	array('label'=>'Create Amenity', 'url'=>array('create')),
	array('label'=>'Manage Amenity', 'url'=>array('admin')),
);
?>

<h1>Amenities</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
