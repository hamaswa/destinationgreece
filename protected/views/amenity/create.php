<?php
/* @var $this AmenityController */
/* @var $model Amenity */

$this->breadcrumbs=array(
	'Amenities'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Amenity', 'url'=>array('index')),
	array('label'=>'Manage Amenity', 'url'=>array('admin')),
);
?>

<h1>Create Amenity</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>