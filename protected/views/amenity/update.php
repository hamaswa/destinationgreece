<?php
/* @var $this AmenityController */
/* @var $model Amenity */

$this->breadcrumbs=array(
	'Amenities'=>array('index'),
	$model->name=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Amenity', 'url'=>array('index')),
	array('label'=>'Create Amenity', 'url'=>array('create')),
	array('label'=>'View Amenity', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Amenity', 'url'=>array('admin')),
);
?>

<h1>Update Amenity <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>