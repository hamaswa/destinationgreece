<?php
/* @var $this CruiseController */
/* @var $model Cruise */

$this->breadcrumbs=array(
	'Cruises'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Cruise', 'url'=>array('index')),
	array('label'=>'Create Cruise', 'url'=>array('create')),
	array('label'=>'Update Cruise', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Cruise', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Cruise', 'url'=>array('admin')),
);
?>

<h1>View Cruise #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'provider',
		'cruiseName',
		'shipName',
		'departureDates',
		'numNights',
		'reservationEmail',
		'reserationPhone',
		'cruiseLine',
		'portTax',
		'portsOfCall',
		'currency',
		'status',
		'itinerary',
		'shipInfo',
		'shorex',
		'terms',
		'contact',
	),
)); ?>
