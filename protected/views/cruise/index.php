<?php
/* @var $this CruiseController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cruises',
);

$this->menu=array(
	array('label'=>'Create Cruise', 'url'=>array('create')),
	array('label'=>'Manage Cruise', 'url'=>array('admin')),
);
?>

<h1>Cruises</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
