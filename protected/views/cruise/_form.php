<?php
/* @var $this CruiseController */
/* @var $cruise Cruise */
/* @var $form CActiveForm */
?>

<div class="fifteen columns hoteladd form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'cruise-form',
        'enableAjaxValidation' => false,
            ));
    ?>
    <div class="four columns" style="width: 220px; padding-top: 0px;" >
        <?php echo $form->textField($cruise, 'cruiseName', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Cruise Name *')); ?>
        <?php echo $form->error($cruise, 'cruiseName'); ?>
    </div>

    <div class="four columns" style="width: 220px; padding-top: 0px;" >
        <?php echo $form->textField($cruise, 'shipName', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Ship Name *')); ?>
        <?php echo $form->error($cruise, 'shipName'); ?>
    </div>


    <div class="four columns" style="width: 220px; padding-top: 0px;" >
        <?php echo $form->textField($cruise, 'numNights', array('placeholder' => '# of Nights')); ?>
        <?php echo $form->error($cruise, 'numNights'); ?>
    </div>
    
    <div class="four columns" style="width: 220px; padding-top: 0px;" >
        <?php echo $form->textArea($cruise, 'departureDates', array('size' => 60, 'maxlength' => 255,'placeholder'=>'Departure Dates (mm/dd,mm/dd)')); ?>
        <?php echo $form->error($cruise, 'departureDates'); ?>
    </div>

    <div class="four columns" style="width: 220px; padding-top: 0px;" >
        <?php echo $form->textArea($cruise, 'portsOfCall', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Ports of Call')); ?>
        <?php echo $form->error($cruise, 'portsOfCall'); ?>
    </div>

    <div class="four columns" style="width: 220px; padding-top: 0px;" >
        <?php echo $form->textField($cruise, 'cruiseLine', array('size' => 60, 'maxlength' => 255,'placeholder'=>'Cruise Line')); ?>
        <?php echo $form->error($cruise, 'cruiseLine'); ?>
    </div>

    <div class="four columns" style="width: 220px; padding-top: 0px;" >
        <?php echo $form->textField($cruise, 'portTax', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Port Taxes')); ?>
        <?php echo $form->error($cruise, 'portTax'); ?>
    </div>
	<div class="clear"></div>

    <div class="four columns" style="width: 220px; padding-top: 0px;" >
        <?php
        $sql = 'SELECT ID,  name FROM currency order by name';
        $cmd = Yii::app()->db->createCommand($sql);
        $res = $cmd->queryAll();
        $arr = array();
        $arr[0] = 'Currency';
        foreach ($res as $cat) {
            $arr[$cat['ID']] = $cat['name'];
        }
        echo $form->dropDownList($cruise, 'currency', $arr, array('id' => 'currency', 'class' => 'three columns', 'style' => 'max-width:218px;float:none;margin-left:0'));
        ?>
        <?php echo $form->error($cruise, 'currency'); ?> 
    </div>

    <div class="four columns" style="width: 220px; padding-top: 0px;" >
        <?php $arrStatus=array(''=>'Show the Cruise',1=>'Yes',0=>'No');?>
        <?php echo $form->dropdownList($cruise ,'status',$arrStatus,array()); ?>
        <?php echo $form->error($cruise, 'status'); ?>
    </div>
    <div class="clear"></div>
    <div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
        <ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header">
            <li class="ui-state-default ui-tabs-selected ui-state-active"><a  class="two columns" href="#rates">Rates</a></li>
            <li class="ui-state-default"><a class="two columns"  href="#description">Itinerary</a></li>
            <li class="ui-state-default"><a class="two columns"  href="#facilities">Ship Info</a></li>
            <li class="ui-state-default"><a class="two columns"  href="#amenities">Shorex</a></li>
            <li class="ui-state-default"><a class="two columns"  href="#photos">Photos</a></li>
            <li class="ui-state-default"><a  class="two columns" href="#terms">Terms</a></li>
            <li class="ui-state-default"><a class="two columns"  href="#contact">Contact</a></li>
        </ul>
        <div id="rates" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">

            <input type="text" name="roomtype1" class="roomtype two columns" placeholder="Cabin type"/>
            <input type="hidden" name="roomtype[]" id="roomtype1"/>
            <input type="text" name="numberofadults[]" placeholder="# of adults" class="one columns"/>
            <div id="rate-div0">
                <div class="remove hotelremove">
                    <input type="text" name="fromDate0[]"  placeholder="From Date" class="fromDate two columns" />
                    <input type="text" name="toDate0[]"  placeholder="To Date" class="toDate two columns"/>
                    <input type="text" name="rate0[]" placeholder="Rate" class="one columns" />
                    <span class="buttonfix">&nbsp;</span>
                </div>
            </div>
            <span class="buttonaddrate"  onclick="addRate('0');">
                <a class="button large_button add"><span></span>rate</a></span>
            <hr />
            <div id="roomtype-div"></div>
            <span style="cursor:pointer;float:right;margin-top:15px;" id="add-roomtype"><a class="button large_button add"><span></span>cabin</a></span> 

        </div>
        <div id="description" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
            <div class="row"> <?php echo $form->textArea($cruise, 'itinerary', array('id' => 'itinerary', 'class' => 'ckeditor')); ?> 
                <?php echo $form->error($cruise, 'itinerary'); ?> 
            </div>
        </div>
        <div id="facilities" class="ui-tabs-panel ui-widget-content ui-corner-bottom"> 
            <div class="ten columns"><?php echo $form->textArea($cruise, 'shipInfo', array('id' => 'facility', 'placeholder' => 'Facility', 'class' => 'ckeditor')); ?> 
                <?php echo $form->error($cruise, 'shipInfo'); ?>
            </div>

        </div>
        <div id="amenities" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide"> 
            <div class="ten columns">
                <?php echo $form->textArea($cruise, 'shorex', array('id' => 'shorex', 'placeholder' => 'Shorex', 'class' => 'ckeditor aaa')); ?></div>

        </div>
        <div id="photos"  class="ui-tabs-panel ui-widget-content ui-corner-bottom">
            <div id="photo-div"> </div>
            <div id="diplay-photo">
                <ul id="photo-list">


                </ul>
            </div>
            <span class="clear"></span>
            <?php
            $this->widget('ext.coco.CocoWidget'
                    , array(
                'id' => 'cocowidget1',
                'onCompleted' => "function(id,filename,jsoninfo){photoUpdate(jsoninfo,'static/uploads/cruise/')}",
                'onCancelled' => 'function(id,filename){ alert("cancelled"); }',
                'onMessage' => 'function(m){ alert(m); }',
                'allowedExtensions' => array('jpeg', 'jpg', 'gif', 'png'), // server-side mime-type validated
                'sizeLimit' => 2000000, // limit in server-side and in client-side
                'uploadDir' => 'static/uploads/cruise', // coco will @mkdir it
                'receptorClassName' => 'application.models.Photo',
                'methodName' => 'photoUploader',
                'maxUploads' => -1, // defaults to -1 (unlimited)
                'maxUploadsReachMessage' => 'No more files allowed', // if empty, no message is shown
                'multipleFileSelection' => true, // true or false, defaults: true
            ));
            ?>
        </div>           
        <div id="terms"  class="ui-tabs-panel ui-widget-content ui-corner-bottom"> 
            <?php echo $form->textArea($cruise, 'terms', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Terms', 'id' => 'hterms', 'class' => 'ckeditor')); ?> <?php echo $form->error($cruise, 'terms'); ?> 
        </div>
        <div id="contact"  class="ui-tabs-panel ui-widget-content ui-corner-bottom">
            <div class="eleven columns">                  
                    <div class="four columns"  style="width: 220px; padding-top: 0px;" >
                        <?php
                        echo $form->textField($cruise, 'email1', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Email 1'));
                        echo $form->error($cruise, 'email1');
                        ?>
                    </div>
                    <div class="four columns"  style="width: 220px; padding-top: 0px;" >
                        <?php
                        echo $form->textField($cruise, 'email2', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Email 2'));
                        echo $form->error($cruise, 'email2');
                        ?>
                    </div>
                <div class="four columns"  style="width: 220px; padding-top: 0px;" >
                    <?php
                    echo $form->textField($cruise, 'zip', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Zip'));
                    echo $form->error($cruise, 'zip');
                    ?>
                </div>
                <div class="four columns"  style="width: 220px; padding-top: 0px;" >
                    <?php
                    echo $form->textField($cruise, 'fax', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Fax'));
                    echo $form->error($cruise, 'fax');
                    ?>
                </div>
                <div class="four columns"  style="width: 220px; padding-top: 0px;" >
                    <?php
                    echo $form->textField($cruise, 'contactName', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Contact Name'));
                    echo $form->error($cruise, 'contactName');
                    ?>
                </div>
                <div class="four columns"  style="width: 220px; padding-top: 0px;" >
                    <?php
                    echo $form->textField($cruise, 'cell', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Cell Phone'));
                    echo $form->error($cruise, 'cell');
                    ?>
                </div>
                <div class="four columns">
                    <?php echo $form->textField($cruise, 'reservationEmail', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Reservation Email')); ?>
                    <?php echo $form->error($cruise, 'reservationEmail'); ?>
                </div>
                <div class="four columns">
                    <?php echo $form->textField($cruise, 'reserationPhone', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Reservation Phone')); ?>
                    <?php echo $form->error($cruise, 'reserationPhone'); ?>
                </div>
            </div>
        </div>
    </div> 


    <div class="clear"></div>
    <hr style="margin-top: 10px;max-width: 870px;" />
    <div style="float: right;margin-right: 20px;" >
        <?php echo CHtml::submitButton($cruise->isNewRecord ? 'Add Cruise' : 'Edit Cruise'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $("#add-roomtype").live('click', function(){
        var numRoomTypes = $('.roomtype').length
        var str="";
        var clear="";

        $('input:text').each(function() {
            $(this).attr('value', $(this).val());
        });
		var del='';

        $('#rate-div0 :input').not(':button,:hidden').each(function(){

            if($(this).attr("placeholder")=="From Date"){
                str+='<div class="remove hotelremove">'+ clear +'<input type="text" value="'+ $(this).val() +'" name="fromDate'+numRoomTypes+'[]"  placeholder="From Date" class="fromDate two columns" />';
            }
            if($(this).attr("placeholder")=="To Date"){
                str+='<input type="text"  value="'+ $(this).val() +'" name="toDate'+numRoomTypes+'[]"  placeholder="To Date" class="toDate two columns" />';
            }
            if($(this).attr("placeholder")=="Rate"){
                str+='<input type="text" name="rate'+numRoomTypes+'[]"  placeholder="Rate" class="one columns" />'+del+'</div>';
				del='<span class="buttonclear"></span><span class="removerate" id="removethis"><a class="button large_button delete"><span></span>rate</a></span>';
            }
            clear='<div class="clear"></div>';
        });
		
        var html='<div class="removeroomtype"><div class="clear"></div>';
        html+='<input class="roomtype two columns"  type="text" placeholder="Cabin type" name="roomtype[]">';
        html+='<input type="text" placeholder="# of adults" name="numberofadults[]" class="one columns" />';

        html+='<div id="rate-div'+numRoomTypes+'">' + str + '</div>';
        html+='<span class="buttonaddrate"  onclick="addRate(\''+numRoomTypes+'\');"><a class="button large_button add"><span></span>rate</a></span><span style="cursor:pointer;float: left;position: absolute;right: 10px;" id="removeroomtype"><a class="button large_button delete"><span></span>cabin</a></span><hr></div>';

        $("#roomtype-div").append(html);
    });
    var rangeDate=0;
    $('body').on('focus',".fromDate", function(){
        if($(this).parent('.remove').prev('.remove').find(".toDate").val()){
            var prvDate=$(this).parent('.remove').prev('.remove').find(".toDate").val();
            rangeDate = new Date(prvDate);
            rangeDate.setDate(rangeDate.getDate() + 1);
        }
        $(this).datepicker({
            dateFormat: 'yy-mm-dd',
            minDate:rangeDate,
            onSelect: function(dateStr) {
                var nextDayDate = new Date(dateStr);
                nextDayDate.setDate(nextDayDate.getDate() + 1);
                $(this).next('.toDate').datepicker({
                    dateFormat: 'yy-mm-dd',
                    minDate: nextDayDate
                }).datepicker('setDate', nextDayDate);
            }
        });
    });
	
	
    
    $('#removethis').live('click',function(e){
        $(this).parents('.remove').remove();

    });
    $('#removeroomtype').live('click',function(e){
        $(this).parents('.removeroomtype').remove();

    });


</script>



<script>
    $('body').on('focus',".roomtype", function(){
        $(this).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "<?php echo Yii::app()->request->baseUrl; ?>/autocom/cabintype/",
                    dataType: "json",
                    data: {
                        featureClass: "P",
                        style: "full",
                        maxRows: 12,
                        name_startsWith: request.term
                    },
                    success: function( data ) {
                        response( $.map( data, function( item ) {
                            return {
                                label: item.name,
                                value: item.id
                            }
                        }));
                    }
                });
            },
            minLength: 2,
            select: function (event, ui) {
                $('#'+$(this).attr('name')).val(ui.item.value);
                // update what is displayed in the textbox
                this.value = ui.item.label; 
                return false;
            }
        });
    });
</script> 
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/ajaxupload.3.5.js" ></script> 
