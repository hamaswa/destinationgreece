<?php
/* @var $this CruiseController */
/* @var $model Cruise */

$this->breadcrumbs=array(
	'Cruises'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Cruise', 'url'=>array('index')),
	array('label'=>'Create Cruise', 'url'=>array('create')),
	array('label'=>'View Cruise', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Cruise', 'url'=>array('admin')),
);
?>

<h1>Update Cruise <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>