<?php
/* @var $this CruiseController */
/* @var $data Cruise */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('provider')); ?>:</b>
	<?php echo CHtml::encode($data->provider); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cruiseName')); ?>:</b>
	<?php echo CHtml::encode($data->cruiseName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shipName')); ?>:</b>
	<?php echo CHtml::encode($data->shipName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('departureDates')); ?>:</b>
	<?php echo CHtml::encode($data->departureDates); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numNights')); ?>:</b>
	<?php echo CHtml::encode($data->numNights); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reservationEmail')); ?>:</b>
	<?php echo CHtml::encode($data->reservationEmail); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('reserationPhone')); ?>:</b>
	<?php echo CHtml::encode($data->reserationPhone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cruiseLine')); ?>:</b>
	<?php echo CHtml::encode($data->cruiseLine); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('portTax')); ?>:</b>
	<?php echo CHtml::encode($data->portTax); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('portsOfCall')); ?>:</b>
	<?php echo CHtml::encode($data->portsOfCall); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('currency')); ?>:</b>
	<?php echo CHtml::encode($data->currency); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('itinerary')); ?>:</b>
	<?php echo CHtml::encode($data->itinerary); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shipInfo')); ?>:</b>
	<?php echo CHtml::encode($data->shipInfo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shorex')); ?>:</b>
	<?php echo CHtml::encode($data->shorex); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('terms')); ?>:</b>
	<?php echo CHtml::encode($data->terms); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contact')); ?>:</b>
	<?php echo CHtml::encode($data->contact); ?>
	<br />

	*/ ?>

</div>