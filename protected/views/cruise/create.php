<?php
/* @var $this CruiseController */
/* @var $cruise Cruise */

$this->breadcrumbs=array(
	'Cruises'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Cruise', 'url'=>array('index')),
	array('label'=>'Manage Cruise', 'url'=>array('admin')),
);
?>

<div style="float:left;margin-left:15px;"><h3>Add Cruise</h3></div>

<?php echo $this->renderPartial('_form', array('cruise'=>$cruise)); ?>