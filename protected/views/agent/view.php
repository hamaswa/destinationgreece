<?php
/* @var $this AgentController */
/* @var $model Agent */

$this->breadcrumbs=array(
	'Agents'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Agent', 'url'=>array('index')),
	array('label'=>'Create Agent', 'url'=>array('create')),
	array('label'=>'Update Agent', 'url'=>array('update', 'id'=>$model->agentID)),
	array('label'=>'Delete Agent', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->agentID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Agent', 'url'=>array('admin')),
);
?>

<h1>View Agent #<?php echo $model->agentID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'agentID',
		'agentName',
		'title',
		'agencyName',
		'website',
		'astaNum',
		'cliaNum',
		'iatanNum',
		'arcNum',
		'hotelsCommission',
		'transfersCommision',
		'ferriesCommision',
		'hydroCommission',
		'toursCommission',
		'ceremonyCommission',
		'shoresCommission',
		'excludeBranding',
		'extracommission',
		'tsb_datetimestamp',
		'status',
		'cruiseCommission',
		'airCommission',
		'packageCommission',
		'sendPassword',
	),
)); ?>
