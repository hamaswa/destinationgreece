<?php
/* @var $this AgentController */
/* @var $model Agent */

$this->breadcrumbs=array(
	'Agents'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Agent', 'url'=>array('index')),
	array('label'=>'Manage Agent', 'url'=>array('admin')),
);
?>

<div style="margin-left:20px;margin-bottom:20px;">
      <h3>Add Agent</h3>
</div>

<?php echo $this->renderPartial('_form', array('agent'=>$agent,'user'=>$user)); ?>