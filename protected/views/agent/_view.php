<?php
/* @var $this AgentController */
/* @var $data Agent */
?>

<div class="view">


	<b><?php echo CHtml::encode($data->getAttributeLabel('agentName')); ?>:</b>
	<?php echo CHtml::encode($data->agentName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agencyName')); ?>:</b>
	<?php echo CHtml::encode($data->agencyName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('website')); ?>:</b>
	<?php echo CHtml::encode($data->website); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('astaNum')); ?>:</b>
	<?php echo CHtml::encode($data->astaNum); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cliaNum')); ?>:</b>
	<?php echo CHtml::encode($data->cliaNum); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('iatanNum')); ?>:</b>
	<?php echo CHtml::encode($data->iatanNum); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('arcNum')); ?>:</b>
	<?php echo CHtml::encode($data->arcNum); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hotelsCommission')); ?>:</b>
	<?php echo CHtml::encode($data->hotelsCommission); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('transfersCommision')); ?>:</b>
	<?php echo CHtml::encode($data->transfersCommision); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ferriesCommision')); ?>:</b>
	<?php echo CHtml::encode($data->ferriesCommision); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hydroCommission')); ?>:</b>
	<?php echo CHtml::encode($data->hydroCommission); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('toursCommission')); ?>:</b>
	<?php echo CHtml::encode($data->toursCommission); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ceremonyCommission')); ?>:</b>
	<?php echo CHtml::encode($data->ceremonyCommission); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shoresCommission')); ?>:</b>
	<?php echo CHtml::encode($data->shoresCommission); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('excludeBranding')); ?>:</b>
	<?php echo CHtml::encode($data->excludeBranding); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('extracommission')); ?>:</b>
	<?php echo CHtml::encode($data->extracommission); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tsb_datetimestamp')); ?>:</b>
	<?php echo CHtml::encode($data->tsb_datetimestamp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cruiseCommission')); ?>:</b>
	<?php echo CHtml::encode($data->cruiseCommission); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('airCommission')); ?>:</b>
	<?php echo CHtml::encode($data->airCommission); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('packageCommission')); ?>:</b>
	<?php echo CHtml::encode($data->packageCommission); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sendPassword')); ?>:</b>
	<?php echo CHtml::encode($data->sendPassword); ?>
	<br />

	*/ ?>

</div>