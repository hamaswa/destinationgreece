<?php
/* @var $this AgentController */
/* @var $model Agent */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

<div class="form-div">
	<div class="row">
		<?php echo $form->label($model,'agentName'); ?>
		<?php echo $form->textField($model,'agentName',array('size'=>60,'maxlength'=>255)); ?>
	</div>


	<div class="row">
		<?php echo $form->label($model,'agencyName'); ?>
		<?php echo $form->textField($model,'agencyName',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'packageCommission'); ?>
		<?php echo $form->textField($model,'packageCommission',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tsb_datetimestamp'); ?>
		<?php echo $form->textField($model,'tsb_datetimestamp'); ?>
	</div>
        <div class="four columns"  style="width: 220px; padding-top: 0px;">
            <?php echo $form->textField($user, 'city', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'City')); ?>
            <?php echo $form->error($user, 'city'); ?>
        </div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->textField($model,'status',array('size'=>60,'maxlength'=>255)); ?>
	</div>



	<div class="row">
		<?php echo $form->label($model,'sendPassword'); ?>
		<?php echo $form->textField($model,'sendPassword',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>
</div>
</div><!-- search-form -->