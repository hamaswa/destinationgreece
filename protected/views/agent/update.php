<?php
/* @var $this AgentController */
/* @var $agent Agent */
/* @var $user User */

$this->breadcrumbs=array(
	'Agents'=>array('index'),
	$agent->title=>array('view','id'=>$agent->agentID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Agent', 'url'=>array('index')),
	array('label'=>'Create Agent', 'url'=>array('create')),
	array('label'=>'View Agent', 'url'=>array('view', 'id'=>$agent->agentID)),
	array('label'=>'Manage Agent', 'url'=>array('admin')),
);
?>
<div style="float:left;width:120px;margin-left:15px;">
      <h3>Update Agent</h3>
</div>

<?php echo $this->renderPartial('_form', array('user'=>$user,'agent'=>$agent)); ?>