<?php
/* @var $this AgentController */
/* @var $agent Agent */
/* @var $user User */
/* @var $form CActiveForm */
?>
<script type="text/javascript">
    /* <![CDATA[ */
    jQuery(function(){				
        jQuery("#Agent_agentName").validate({
            expression:  "if (VAL.match(/^[a-zA-Z]+([a-zA-Z]?[\-' ]?[a-zA-Z]+)*[a-zA-Z]+$/)) return true; else return false;",
            message: "Invalid characters"
        });
        jQuery("#Agent_agencyName").validate({
            expression:  "if (VAL.match(/^[a-zA-Z]+([a-zA-Z]?[ ]?[a-zA-Z]+)*[a-zA-Z]+$/)) return true; else return false;",
            message: "Invalid characters"
        });
//        jQuery("#Agent_hostingAgency").validate({
//            expression:  "if (VAL.match(/^[a-zA-Z0-9]+$/)) return true; else return false;",
//            message: "Invalid characters"
//        });
//        jQuery("#Agent_hostingMemberNo").validate({
//            expression:  "if (VAL.match(/^[a-zA-Z0-9]+$/)) return true; else return false;",
//            message: "Invalid characters"
//        });
        jQuery("#User_email1").validate({
            expression: "if (checkEmail(VAL)) return true; else return false;",
            message: "Invalid Email"
        });
        jQuery("#User_email2").validate({
            expression: "if (VAL.match(/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/) || ! VAL) return true; else return false;",
            message: "Invalid Email"
        });
 
        jQuery("#User_phone1").validate({
            expression: "if (checkInternationalPhone(VAL)) return true; else return false;",
            message: "Invalid Number"
        });
        jQuery("#User_phone2").validate({
            expression: "if (checkInternationalPhone(VAL) || ! VAL) return true; else return false;",
            message: "Invalid Number"
        });
        jQuery("#User_cell1").validate({
            expression: "if (checkInternationalPhone(VAL) || ! VAL) return true; else return false;",
            message: "Invalid Number"
        });
        jQuery("#User_username").validate({
            expression: "if (VAL.match(/^[a-zA-Z0-9]{4,15}$/)) return true; else return false;",
            message: "Invalid username"
        });
        jQuery("#User_password").validate({
            expression: "if (VAL.match(/^[a-zA-Z0-9#\-\+@]{5,15}$/)) return true; else return false;",
            message: "Invalid Password"
        });
				
        jQuery("#User_address1").validate({
            expression: "if (VAL.match(/^[a-zA-Z0-9# ]+$/)) return true; else return false;",
            message: "Invalid Address"
        });
        jQuery("#User_zip").validate({
            expression: "if (VAL.match(/^[0-9]{5}(-\[0-9]{2,5})?$/)) return true; else return false;",
            message: "Invalid Zip"
        });
        jQuery("#User_city").validate({
            expression: "if (VAL.match(/^[a-zA-Z ]{3,25}$/)) return true; else return false;",
            message: "Invalid City Name"
        });
//        jQuery("#Agent_hotelsCommission").validate({
//            expression: "if (!isNaN(VAL) && VAL) return true; else return false;",
//            message: "Invalid Number"
//        });
//        jQuery("#Agent_hydroCommission").validate({
//            expression: "if (!isNaN(VAL) && VAL) return true; else return false;",
//            message: "Invalid Number"
//        });
//        jQuery("#Agent_transfersCommision").validate({
//            expression: "if (!isNaN(VAL) && VAL) return true; else return false;",
//            message: "Invalid Number"
//        });
//        jQuery("#Agent_ferriesCommision").validate({
//            expression: "if (!isNaN(VAL) && VAL) return true; else return false;",
//            message: "Invalid Number"
//        });
//        jQuery("#Agent_extraCommission").validate({
//            expression: "if (!isNaN(VAL) && VAL) return true; else return false;",
//            message: "Invalid Number"
//        });
//        jQuery("#Agent_toursCommission").validate({
//            expression: "if (!isNaN(VAL) && VAL) return true; else return false;",
//            message: "Invalid Number"
//        });
//        jQuery("#Agent_ceremonyCommission").validate({
//            expression: "if (!isNaN(VAL) && VAL) return true; else return false;",
//            message: "Invalid Number"
//        });
//        jQuery("#Agent_cruiseCommission").validate({
//            expression: "if (!isNaN(VAL) && VAL) return true; else return false;",
//            message: "Invalid Number"
//        });
//        jQuery("#Agent_airCommission").validate({
//            expression: "if (!isNaN(VAL) && VAL) return true; else return false;",
//            message: "Invalid Number"
//        });
//        jQuery("#Agent_extracommission").validate({
//            expression: "if (!isNaN(VAL) && VAL) return true; else return false;",
//            message: "Invalid Number"
//        });
//        jQuery("#Agent_ceremonyCmmission").validate({
//            expression: "if (!isNaN(VAL) && VAL) return true; else return false;",
//            message: "Invalid Number"
//        });
        jQuery("#Agent_packageCommission").validate({
            expression: "if (!isNaN(VAL) && VAL) return true; else return false;",
            message: "Invalid Number"
        });
        //jQuery("#Agent_shoresCommission").validate({
//            expression: "if (!isNaN(VAL) && VAL) return true; else return false;",
//            message: "Invalid Number"
//        });
//				
//        jQuery("#Agent_country").validate({
//            expression: "if (VAL.match(/^[a-zA-Z]+)) return true; else return false;",
//            message: "Please select country"
//        });
				
    });
    /* ]]> */
</script>

<div class="fifteen columns"> 
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'agent-form',
        'enableAjaxValidation' => false,
            ));
    ?>
    <div class="agentform">
<?php /*?>        <div id="response"><?php echo $form->errorSummary($user, $agent); ?></div>
<?php */?>
        <div class="four columns"  style="width: 220px; padding-top: 0px;">

            <?php echo $form->textField($agent, 'agentName', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Agent Name')); ?>
            <?php echo $form->error($agent, 'agentName'); ?>
        </div>

        <div class="four columns"  style="width: 220px; padding-top: 0px;">

            <?php echo $form->textField($agent, 'title', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Title')); ?>
            <?php echo $form->error($agent, 'title'); ?>
        </div>

        <div class="four columns"  style="width: 220px; padding-top: 0px;">

            <?php echo $form->textField($agent, 'agencyName', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Agency Name')); ?>
            <?php echo $form->error($agent, 'agencyName'); ?>
        </div>

        <div class="four columns"  style="width: 220px; padding-top: 0px;">

            <?php echo $form->textField($agent, 'website', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Agency URL')); ?>
            <?php echo $form->error($agent, 'website'); ?>
        </div>
        <div class="four columns"  style="width: 220px; padding-top: 0px;">

             <?php echo $form->textField($agent, 'hostingAgency', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Hosting Agency')); ?>
            <?php echo $form->error($agent, 'hostingAgency'); ?>
            
        </div>
        <div class="four columns"  style="width: 220px; padding-top: 0px;">
            
             <?php echo $form->textField($agent, 'hostingMemberNo', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Hosting Member No')); ?>
            <?php echo $form->error($agent, 'hostingMemberNo'); ?>
            
        </div>
        <div class="four columns"  style="width: 220px; padding-top: 0px;">
            <?php echo $form->textField($user, 'email1', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Email 1')); ?>
            <?php echo $form->error($user, 'email1'); ?>
        </div>
        <div class="four columns"  style="width: 220px; padding-top: 0px;">
            <?php echo $form->textField($user, 'email2', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Email 2')); ?>
            <?php echo $form->error($user, 'email2'); ?>
        </div>
        <div class="four columns"  style="width: 220px; padding-top: 0px;">
            <?php echo $form->textField($user, 'phone1', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Phone 1')); ?>
            <?php echo $form->error($user, 'phone1'); ?>
        </div>
        <div class="four columns"  style="width: 220px; padding-top: 0px;">
            <?php echo $form->textField($user, 'phone2', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Phone 2')); ?>
            <?php echo $form->error($user, 'phone2'); ?>
        </div>
        <div class="four columns"  style="width: 220px; padding-top: 0px;">
            <?php echo $form->textField($user, 'cell1', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Cell Phone')); ?>
            <?php echo $form->error($user, 'cell1'); ?>
        </div>
        <div class="four columns"  style="width: 220px; padding-top: 0px;">
            <?php echo $form->textField($user, 'fax', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Fax')); ?>
            <?php echo $form->error($user, 'fax'); ?>
        </div>
        <div class="four columns"  style="width: 220px; padding-top: 0px;">
            <?php echo $form->textField($user, 'address1', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Address 1')); ?>
            <?php echo $form->error($user, 'address1'); ?>
        </div>
        <div class="four columns"  style="width: 220px; padding-top: 0px;">
            <?php echo $form->textField($user, 'address2', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Address 2')); ?>
            <?php echo $form->error($user, 'address2'); ?>
        </div>

        <div class="four columns"  style="width: 220px; padding-top: 0px;">
            <?php echo $form->textField($user, 'city', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'City')); ?>
            <?php echo $form->error($user, 'city'); ?>
        </div>
        <div class="four columns"  style="width: 220px; padding-top: 0px;">
            <?php echo $form->textField($user, 'zip', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Zip')); ?>
            <?php echo $form->error($user, 'zip'); ?>
        </div>
        <div class="four columns"  style="width: 220px; padding-top: 0px;">
            <?php echo $form->dropDownList($user, 'country',array(),  array('id'=>'countrySelect','draggable'=>"true",'onchange'=>'populateState()')); ?>
            <?php echo $form->error($user, 'country'); ?>

        </div>
        <div class="four columns"  style="width: 220px; padding-top: 0px;">
            <?php echo $form->dropDownList($user, 'state',array(), array('id'=>'stateSelect')); ?>
            <?php echo $form->error($user, 'state'); ?>

        </div>
        <div class="four columns"  style="width: 220px; padding-top: 0px;">

            <?php echo $form->textField($agent, 'astaNum', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'ASTA No./TRUE No.')); ?>
            <?php echo $form->error($agent, 'astaNum'); ?>
        </div>

        <div class="four columns"  style="width: 220px; padding-top: 0px;">

            <?php echo $form->textField($agent, 'cliaNum', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'CLIA Num')); ?>
            <?php echo $form->error($agent, 'cliaNum'); ?>
        </div>

        <div class="four columns"  style="width: 220px; padding-top: 0px;">

            <?php echo $form->textField($agent, 'iatanNum', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'IATA Num')); ?>
            <?php echo $form->error($agent, 'iatanNum'); ?>
        </div>

        <div class="four columns"  style="width: 220px; padding-top: 0px;">

            <?php echo $form->textField($agent, 'arcNum', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'ARC Num')); ?>
            <?php echo $form->error($agent, 'arcNum'); ?>
        </div>
        <div class="four columns"  style="width: 220px; padding-top: 0px;">

            <?php echo $form->textField($user, 'username', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'User Name')); ?>
            <?php echo $form->error($user, 'username'); ?>
        </div>
        <div class="four columns"  style="width: 220px; padding-top: 0px;">

            <?php echo $form->passwordField($user, 'password', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Password')); ?>
            <?php echo $form->error($user, 'password'); ?>
        </div>
        <div class="agent-notes">
            <?php echo $form->textArea($user, 'notes', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Notes', 'id' => 'notes', 'class' => 'notes')); ?>
            <?php echo $form->error($user, 'notes'); ?>

            <div id="counter" style="text-align:right;"></div>
        </div>  
        <div class="agent-notes agentsubmit1">
        <div style="float:right;"> 
            <input type="reset" value="Clear" style="margin-right:10px;" id="clear" /><?php echo CHtml::submitButton($agent->isNewRecord ? 'Save' : 'Update'); ?>
        </div>
    </div> 
    </div>

    <fieldset class="agentform commissiondiv">
        <legend>Commission information</legend>
        <div class="four columns">
            <?php echo $form->textField($agent, 'packageCommission', array('size' => 60, 'maxlength' => 255, 'class'=>'packagecommission', 'placeholder' => 'Package Commission')); ?>
            <?php echo $form->error($agent, 'packageCommission'); ?>
        </div>
        <div class="four columns">

            <?php echo $form->textField($agent, 'hotelsCommission', array('size' => 60, 'disabled'=>'disabled', 'maxlength' => 255, 'class'=>'othercommission', 'placeholder' => 'Hotel Commission')); ?>
            <?php echo $form->error($agent, 'hotelsCommission'); ?>
        </div>

        <div class="four columns">
            <?php echo $form->textField($agent, 'transfersCommision', array('size' => 60, 'disabled'=>'disabled', 'maxlength' => 255, 'class'=>'othercommission', 'placeholder' => 'Transfer Commission')); ?>
            <?php echo $form->error($agent, 'transfersCommision'); ?>
        </div>
        <div class="four columns">
            <?php echo $form->textField($agent, 'ferriesCommision', array('size' => 60, 'disabled'=>'disabled', 'maxlength' => 255, 'class'=>'othercommission' ,'placeholder' => 'Ferries Commission')); ?>
            <?php echo $form->error($agent, 'ferriesCommision'); ?>
        </div>

        <div class="four columns">
            <?php echo $form->textField($agent, 'hydroCommission', array('size' => 60, 'disabled'=>'disabled', 'maxlength' => 255, 'class'=>'othercommission', 'placeholder' => 'Hydro Commission')); ?>
            <?php echo $form->error($agent, 'hydroCommission'); ?>
        </div>

        <div class="four columns">
            <?php echo $form->textField($agent, 'toursCommission', array('size' => 60, 'disabled'=>'disabled', 'maxlength' => 255, 'class'=>'othercommission' ,'placeholder' => 'Tours Commission')); ?>
            <?php echo $form->error($agent, 'toursCommission'); ?>
        </div>

        <div class="four columns">
            <?php echo $form->textField($agent, 'ceremonyCommission', array('size' => 60, 'disabled'=>'disabled', 'maxlength' => 255, 'class'=>'othercommission', 'placeholder' => 'Ceremony Commission')); ?>
            <?php echo $form->error($agent, 'ceremonyCommission'); ?>
        </div>

        <div class="four columns">
            <?php echo $form->textField($agent, 'shoresCommission', array('size' => 60, 'disabled'=>'disabled', 'maxlength' => 255, 'class'=>'othercommission', 'placeholder' => 'Shorex Commission')); ?>
            <?php echo $form->error($agent, 'shoresCommission'); ?>
        </div>

        <div class="four columns">
            <?php echo $form->textField($agent, 'extracommission', array('size' => 60, 'disabled'=>'disabled', 'maxlength' => 255, 'class'=>'othercommission' ,'placeholder' => 'Extra Commission')); ?>
            <?php echo $form->error($agent, 'extracommission'); ?>
        </div>

        <div class="four columns">
            <?php echo $form->textField($agent, 'cruiseCommission', array('size' => 60, 'disabled'=>'disabled', 'maxlength' => 255, 'class'=>'othercommission', 'placeholder' => 'Cruise Commission')); ?>
            <?php echo $form->error($agent, 'cruiseCommission'); ?>
        </div>

        <div class="four columns">
            <?php echo $form->textField($agent, 'airCommission', array('size' => 60, 'disabled'=>'disabled', 'maxlength' => 255, 'class'=>'othercommission', 'placeholder' => 'Air Commission')); ?>
            <?php echo $form->error($agent, 'airCommission'); ?>
        </div>

        <div class="four columns">
             <?php
            $sendPassword=array(''=>'Send Password','yes'=>'Yes','no'=>'No');
            ?>
           <?php echo $form->dropDownList($agent, 'sendPassword',$sendPassword, array('id'=>'sendPassword')); ?>
            <?php echo $form->error($agent, 'sendPassword'); ?>
        </div>
        <div class="four columns">
            <?php
            $status=array(''=>'Agent status',1=>'Active Account',2=>'Suspended',0=>'Banned');
            ?>
           <?php echo $form->dropDownList($agent, 'status',$status, array('id'=>'activeAccount')); ?>
            <?php echo $form->error($agent, 'status'); ?>
        </div>
        <div class="four columns">
            <label for="tsb_DateTimeStamp">TSB approval date</label>
             <?php echo $form->textField($agent, 'tsb_datetimestamp', array('size' => 60, 'maxlength' => 255,'id'=>'tsb_DateTimeStamp', 'placeholder' => 'TSB_DateTimeStamp')); ?>
            <?php echo $form->error($agent, 'tsb_DateTimeStamp'); ?>
        </div>
    </fieldset>
    <div class="agent-notes agentsubmit2">
        <div style="float:right;"> 
            <input type="reset" value="Clear" style="margin-right:10px;" id="clear" /><?php echo CHtml::submitButton($agent->isNewRecord ? 'Save' : 'Update'); ?>
        </div>
    </div>
</div>



<?php $this->endWidget(); ?>

</div><!-- form -->

<script src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/countrystate.js" type="text/javascript"></script>
<script>
    $().ready(function(){
        $( "#tsb_DateTimeStamp" ).datepicker({
            dateFormat: 'yy-mm-dd'
        }).datepicker('setDate', '0');		 		
        var characters = 2000;
        $("#counter").append("You have <strong>"+  characters+"</strong> characters remaining");
        $("#notes").keyup(function(){
            if($(this).val().length > characters){
                $(this).val($(this).val().substr(0, characters));
            }
            var remaining = characters -  $(this).val().length;
            $("#counter").html("You have <strong>"+  remaining+"</strong> characters remaining");
            if(remaining <= 10)
            {
                $("#counter").css("color","red");
            }
            else
            {
                $("#counter").css("color","black");
            }
        });
	
        $('input:reset').click(function(){
            $("input:text").next(".ValidationErrors").remove();
            $("input:text").removeClass("ErrorField");
            $("input:password").next(".ValidationErrors").remove();
            $("input:password").removeClass("ErrorField");
            $("select").next(".ValidationErrors").remove();
            $("select").removeClass("ErrorField");
        });
		$("#activeAccount").change(function(){
			if($(this).val()=='0'){
			var str='<input size="60" maxlength="255" placeholder="Reason" name="reason" id="reason" type="text">';
			$(this).parent().append(str);
			} else {
				$(this).parent().find('#reason').remove();
			}
			
		});
		$(".packagecommission").change(function(){
			if($(this).val()){
				$(".packagecommission").removeAttr('disabled');
				$(".othercommission").attr('disabled','disabled');
			} else {
				
				$(".othercommission").removeAttr('disabled');
				$(".othercommission:first").focus();
				//$(".packagecommission").attr('disabled','disabled');

			}
		});
		$(".othercommission").change(function(){
			var change=false;
			$('.othercommission input:text').each(function() {
            	if($(this).val())
				{
					change=true;
				}
        	});
			if(change){
				$(".othercommission").removeAttr('disabled');
				$(".packagecommission").attr('disabled','disabled');
			} else {
				
				$(".packagecommission").removeAttr('disabled');

			}
		});
    }); 
    initCountry('US');	
</script>