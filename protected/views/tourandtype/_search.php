<?php
/* @var $this TourandtypeController */
/* @var $model Tourandtype */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID'); ?>
		<?php echo $form->textField($model,'ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tourID'); ?>
		<?php echo $form->textField($model,'tourID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tourTypeID'); ?>
		<?php echo $form->textField($model,'tourTypeID'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->