<?php
/* @var $this TourandtypeController */
/* @var $data Tourandtype */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tourID')); ?>:</b>
	<?php echo CHtml::encode($data->tourID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tourTypeID')); ?>:</b>
	<?php echo CHtml::encode($data->tourTypeID); ?>
	<br />


</div>