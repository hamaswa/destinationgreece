<?php
/* @var $this TourandtypeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tourandtypes',
);

$this->menu=array(
	array('label'=>'Create Tourandtype', 'url'=>array('create')),
	array('label'=>'Manage Tourandtype', 'url'=>array('admin')),
);
?>

<h1>Tourandtypes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
