<?php
/* @var $this TourandtypeController */
/* @var $model Tourandtype */

$this->breadcrumbs=array(
	'Tourandtypes'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Tourandtype', 'url'=>array('index')),
	array('label'=>'Create Tourandtype', 'url'=>array('create')),
	array('label'=>'Update Tourandtype', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Tourandtype', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Tourandtype', 'url'=>array('admin')),
);
?>

<h1>View Tourandtype #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'tourID',
		'tourTypeID',
	),
)); ?>
