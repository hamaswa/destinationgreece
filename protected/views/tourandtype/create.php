<?php
/* @var $this TourandtypeController */
/* @var $model Tourandtype */

$this->breadcrumbs=array(
	'Tourandtypes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Tourandtype', 'url'=>array('index')),
	array('label'=>'Manage Tourandtype', 'url'=>array('admin')),
);
?>

<h1>Create Tourandtype</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>