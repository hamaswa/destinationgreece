<?php
/* @var $this TourandtypeController */
/* @var $model Tourandtype */

$this->breadcrumbs=array(
	'Tourandtypes'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Tourandtype', 'url'=>array('index')),
	array('label'=>'Create Tourandtype', 'url'=>array('create')),
	array('label'=>'View Tourandtype', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Tourandtype', 'url'=>array('admin')),
);
?>

<h1>Update Tourandtype <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>