<?php
/* @var $this CombopackagestructureController */
/* @var $data Combopackagestructure */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comboPackageID')); ?>:</b>
	<?php echo CHtml::encode($data->comboPackageID); ?>
	<br />


</div>