<?php
/* @var $this CombopackagestructureController */
/* @var $model Combopackagestructure */

$this->breadcrumbs=array(
	'Combopackagestructures'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Combopackagestructure', 'url'=>array('index')),
	array('label'=>'Manage Combopackagestructure', 'url'=>array('admin')),
);
?>

<h1>Create Combopackagestructure</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>