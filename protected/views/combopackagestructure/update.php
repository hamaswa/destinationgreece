<?php
/* @var $this CombopackagestructureController */
/* @var $model Combopackagestructure */

$this->breadcrumbs=array(
	'Combopackagestructures'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Combopackagestructure', 'url'=>array('index')),
	array('label'=>'Create Combopackagestructure', 'url'=>array('create')),
	array('label'=>'View Combopackagestructure', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Combopackagestructure', 'url'=>array('admin')),
);
?>

<h1>Update Combopackagestructure <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>