<?php
/* @var $this CombopackagestructureController */
/* @var $model Combopackagestructure */

$this->breadcrumbs=array(
	'Combopackagestructures'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Combopackagestructure', 'url'=>array('index')),
	array('label'=>'Create Combopackagestructure', 'url'=>array('create')),
	array('label'=>'Update Combopackagestructure', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Combopackagestructure', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Combopackagestructure', 'url'=>array('admin')),
);
?>

<h1>View Combopackagestructure #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'comboPackageID',
	),
)); ?>
