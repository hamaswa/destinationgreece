<?php
/* @var $this HotelController */
/* @var $model Hotel */

$this->breadcrumbs = array(
    'Hotels' => array('index'),
    'Create',
);

$this->menu = array(
    array('label' => 'List Hotel', 'url' => array('index')),
    array('label' => 'Manage Hotel', 'url' => array('admin')),
);
?>

<div style="margin-left:20px;margin-bottom:20px;"><h3>Add Hotel</h3></div>

<?php
echo $this->renderPartial('_form', array(
    'hotel' => $hotel,
    'hotelAmenity' => $hotelAmenity,
    'hotelFacility' => $hotelFacility,
    'hotelPhoto' => $hotelPhoto,
    'hotelRoomType' => $hotelRoomType,
    'roomType' => $roomType,
    'hotelRates' => $hotelRates,
    'hotelPhoto' => $hotelPhoto
));
?>