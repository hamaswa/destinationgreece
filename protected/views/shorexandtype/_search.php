<?php
/* @var $this ShorexandtypeController */
/* @var $model Shorexandtype */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID'); ?>
		<?php echo $form->textField($model,'ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'shorexID'); ?>
		<?php echo $form->textField($model,'shorexID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'shorexTypeID'); ?>
		<?php echo $form->textField($model,'shorexTypeID'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->