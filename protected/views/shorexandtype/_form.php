<?php
/* @var $this ShorexandtypeController */
/* @var $model Shorexandtype */
/* @var $form CActiveForm */
?>

   <div class="eleven columns" style="margin-left:20% !important;margin-right:20% !important;">
      <h3 class="form-heading">Add Agent</h3>
      <div id="response"></div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'shorexandtype-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="five columns">
		<?php echo $form->labelEx($model,'shorexID'); ?>
		<?php echo $form->textField($model,'shorexID'); ?>
		<?php echo $form->error($model,'shorexID'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'shorexTypeID'); ?>
		<?php echo $form->textField($model,'shorexTypeID'); ?>
		<?php echo $form->error($model,'shorexTypeID'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->