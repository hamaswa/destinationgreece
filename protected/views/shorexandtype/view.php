<?php
/* @var $this ShorexandtypeController */
/* @var $model Shorexandtype */

$this->breadcrumbs=array(
	'Shorexandtypes'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Shorexandtype', 'url'=>array('index')),
	array('label'=>'Create Shorexandtype', 'url'=>array('create')),
	array('label'=>'Update Shorexandtype', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Shorexandtype', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Shorexandtype', 'url'=>array('admin')),
);
?>

<h1>View Shorexandtype #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'shorexID',
		'shorexTypeID',
	),
)); ?>
