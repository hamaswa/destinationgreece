<?php
/* @var $this ShorexandtypeController */
/* @var $data Shorexandtype */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shorexID')); ?>:</b>
	<?php echo CHtml::encode($data->shorexID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shorexTypeID')); ?>:</b>
	<?php echo CHtml::encode($data->shorexTypeID); ?>
	<br />


</div>