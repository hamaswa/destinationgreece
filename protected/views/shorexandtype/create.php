<?php
/* @var $this ShorexandtypeController */
/* @var $model Shorexandtype */

$this->breadcrumbs=array(
	'Shorexandtypes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Shorexandtype', 'url'=>array('index')),
	array('label'=>'Manage Shorexandtype', 'url'=>array('admin')),
);
?>

<h1>Create Shorexandtype</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>