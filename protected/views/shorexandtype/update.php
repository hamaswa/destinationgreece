<?php
/* @var $this ShorexandtypeController */
/* @var $model Shorexandtype */

$this->breadcrumbs=array(
	'Shorexandtypes'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Shorexandtype', 'url'=>array('index')),
	array('label'=>'Create Shorexandtype', 'url'=>array('create')),
	array('label'=>'View Shorexandtype', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Shorexandtype', 'url'=>array('admin')),
);
?>

<h1>Update Shorexandtype <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>