<?php
/* @var $this ShorexandtypeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Shorexandtypes',
);

$this->menu=array(
	array('label'=>'Create Shorexandtype', 'url'=>array('create')),
	array('label'=>'Manage Shorexandtype', 'url'=>array('admin')),
);
?>

<h1>Shorexandtypes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
