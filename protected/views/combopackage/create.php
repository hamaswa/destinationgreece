<?php
/* @var $this CombopackageController */
/* @var $model Combopackage */

$this->breadcrumbs=array(
	'Combopackages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Combopackage', 'url'=>array('index')),
	array('label'=>'Manage Combopackage', 'url'=>array('admin')),
);
?>

<h1>Create Combopackage</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>