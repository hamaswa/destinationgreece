<?php
/* @var $this CombopackageController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Combopackages',
);

$this->menu=array(
	array('label'=>'Create Combopackage', 'url'=>array('create')),
	array('label'=>'Manage Combopackage', 'url'=>array('admin')),
);
?>

<h1>Combopackages</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
