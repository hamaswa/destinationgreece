<?php
/* @var $this CombopackageController */
/* @var $model Combopackage */

$this->breadcrumbs=array(
	'Combopackages'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Combopackage', 'url'=>array('index')),
	array('label'=>'Create Combopackage', 'url'=>array('create')),
	array('label'=>'Update Combopackage', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Combopackage', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Combopackage', 'url'=>array('admin')),
);
?>

<h1>View Combopackage #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'name',
		'departsOn',
		'status',
		'packageType',
		'extras',
		'summary',
		'map',
		'terms',
	),
)); ?>
