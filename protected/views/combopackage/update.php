<?php
/* @var $this CombopackageController */
/* @var $model Combopackage */

$this->breadcrumbs=array(
	'Combopackages'=>array('index'),
	$model->name=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Combopackage', 'url'=>array('index')),
	array('label'=>'Create Combopackage', 'url'=>array('create')),
	array('label'=>'View Combopackage', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Combopackage', 'url'=>array('admin')),
);
?>

<h1>Update Combopackage <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>