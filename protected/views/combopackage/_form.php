<?php
/* @var $this CombopackageController */
/* @var $model Combopackage */
/* @var $form CActiveForm */
?>

   <div class="eleven columns" style="margin-left:20% !important;margin-right:20% !important;">
      <h3 class="form-heading">Add Agent</h3>
      <div id="response"></div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'combopackage-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="five columns">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'departsOn'); ?>
		<?php echo $form->textField($model,'departsOn'); ?>
		<?php echo $form->error($model,'departsOn'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'packageType'); ?>
		<?php echo $form->textField($model,'packageType'); ?>
		<?php echo $form->error($model,'packageType'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'extras'); ?>
		<?php echo $form->textField($model,'extras',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'extras'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'summary'); ?>
		<?php echo $form->textField($model,'summary',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'summary'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'map'); ?>
		<?php echo $form->textArea($model,'map',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'map'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'terms'); ?>
		<?php echo $form->textArea($model,'terms',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'terms'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->