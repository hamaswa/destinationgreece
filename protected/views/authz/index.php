<?php
/* @var $this AuthzController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Authzs',
);

$this->menu=array(
	array('label'=>'Create Authz', 'url'=>array('create')),
	array('label'=>'Manage Authz', 'url'=>array('admin')),
);
?>

<h1>Authzs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
