<?php
/* @var $this AuthzController */
/* @var $model Authz */

$this->breadcrumbs=array(
	'Authzs'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Authz', 'url'=>array('index')),
	array('label'=>'Create Authz', 'url'=>array('create')),
	array('label'=>'View Authz', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Authz', 'url'=>array('admin')),
);
?>

<h1>Update Authz <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>