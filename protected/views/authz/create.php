<?php
/* @var $this AuthzController */
/* @var $model Authz */

$this->breadcrumbs=array(
	'Authzs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Authz', 'url'=>array('index')),
	array('label'=>'Manage Authz', 'url'=>array('admin')),
);
?>

<h1>Create Authz</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>