<?php
/* @var $this AuthzController */
/* @var $model Authz */

$this->breadcrumbs=array(
	'Authzs'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Authz', 'url'=>array('index')),
	array('label'=>'Create Authz', 'url'=>array('create')),
	array('label'=>'Update Authz', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Authz', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Authz', 'url'=>array('admin')),
);
?>

<h1>View Authz #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'userID',
		'privilege',
		'enabled',
	),
)); ?>
