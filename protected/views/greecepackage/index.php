<?php
/* @var $this GreecepackageController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Greecepackages',
);

$this->menu=array(
	array('label'=>'Create Greecepackage', 'url'=>array('create')),
	array('label'=>'Manage Greecepackage', 'url'=>array('admin')),
);
?>

<h1>Greecepackages</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
