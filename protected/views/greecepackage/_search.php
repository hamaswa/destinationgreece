<?php
/* @var $this GreecepackageController */
/* @var $model Greecepackage */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID'); ?>
		<?php echo $form->textField($model,'ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'departsonDate'); ?>
		<?php echo $form->textField($model,'departsonDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'visiting'); ?>
		<?php echo $form->textField($model,'visiting',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->textField($model,'status',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'packageClass'); ?>
		<?php echo $form->textField($model,'packageClass'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'numnights'); ?>
		<?php echo $form->textField($model,'numnights'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'numdays'); ?>
		<?php echo $form->textField($model,'numdays'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'summary'); ?>
		<?php echo $form->textArea($model,'summary',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dayToDayItinerary'); ?>
		<?php echo $form->textArea($model,'dayToDayItinerary',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'servicesIncluded'); ?>
		<?php echo $form->textArea($model,'servicesIncluded',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'map'); ?>
		<?php echo $form->textArea($model,'map',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'terms'); ?>
		<?php echo $form->textArea($model,'terms',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->