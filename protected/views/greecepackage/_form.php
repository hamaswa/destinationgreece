<?php
/* @var $this GreecepackageController */
/* @var $model Greecepackage */
/* @var $form CActiveForm */
?>

<div class="sixteen columns form hoteladd">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'greecepackage-form',
        'enableAjaxValidation' => false,
            ));
    ?>
    <?php echo $form->errorSummary($model); ?>

    <div class="four columns" style="width:220px;">
        <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Please enter package name')); ?>
        <?php echo $form->error($model, 'name'); ?>
    </div>

    <div class="four columns" style="width:220px;">
        <?php echo $form->textField($model, 'departsonDate', array('placeholder' => 'Departs on')); ?>
        <?php echo $form->error($model, 'departsonDate'); ?>
    </div>
    <div class="four columns" style="width:220px;">
        <?php echo $form->textField($model, 'visiting', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Visiting')); ?>
        <?php echo $form->error($model, 'visiting'); ?>
    </div>

    <div class="four columns" style="width:220px;">
        <?php $arrStatus = array('' => 'Show the Package', 1 => 'Yes', 0 => 'No'); ?>
        <?php echo $form->dropdownList($model, 'status', $arrStatus, array()); ?>
        <?php echo $form->error($model, 'status'); ?>
    </div>

    <div class="four columns" style="width:220px;">
        
        <input type="text" id="package" name="" maxlength="255" size="60" placeholder="Package Class" />
        <input type="hidden" id="packageID" name="Greecepackage[packageClass]"/>
        <?php echo $form->error($model, 'packageClass'); ?>
    </div>

    <div class="four columns" style="width:220px;">
        <?php echo $form->textField($model, 'numnights', array('placeholder' => '# of nights')); ?>
        <?php echo $form->error($model, 'numnights'); ?>
    </div>

    <div class="four columns" style="width:220px;">
        <?php echo $form->textField($model, 'numdays', array('placeholder' => '# of days')); ?>
        <?php echo $form->error($model, 'numdays'); ?>
    </div>
    <div class="clear"></div>
    <div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
        <ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
            <li class="ui-state-default ui-corner-top"><a class="two columns" id="overnights-tab" href="#overnights">Overnights</a></li>
            <li class="ui-state-default ui-corner-top"><a class="two columns" id="transfers-tab" href="#transfers" >Transfers</a></li>
            <li class="ui-state-default ui-corner-top"><a class="two columns" id="ferries-tab" href="#ferries" >Ferries/Hydros</a></li>
            <li class="ui-state-default ui-corner-top"><a class="two columns" id="tours-tab" href="#tours">Tours</a></li>
            <li class="ui-state-default ui-corner-top"><a class="two columns" id="cruises-tab" href="#cruises" >Cruise</a></li>
            <li class="ui-state-default ui-corner-top"><a class="two columns" id="air-tab" href="#air" >Air</a></li>
            <li class="ui-state-default ui-corner-top"><a class="two columns" id="extras-tab" href="#extras">Extras</a></li>
            <li class="ui-state-default ui-corner-top"><a class="two columns" id="summary-tab" href="#summary">Summary</a></li>
            <li class="ui-state-default ui-corner-top"><a class="two columns" id="itinerary-tab" href="#itinerary" >Itinerary</a></li>
            <li class="ui-state-default ui-corner-top"><a class="two columns" id="services-tab" href="#services" >Services</a></li>
            <li class="ui-state-default ui-corner-top"><a class="two columns" id="photos-tab" href="#photos" >Photos</a></li>
            <li class="ui-state-default ui-corner-top"><a class="two columns" id="map-tab" href="#map">Map</a></li>
            <li class="ui-state-default ui-corner-top"><a class="two columns" id="terms-tab" href="#terms">Terms</a></li>
        </ul>
        <div id="overnights" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">


			<div id="rate-div0">
                <div class="remove">
                    <input type="text" class="city three columns" placeholder="City"/>
                    <input type="hidden" class="cityID" name="city[]" />
                    <input type="text" placeholder="# of overnights" name="cityNights[]" class="two columns"/>
                    <span class="buttonfix">&nbsp;</span>
                </div>
            </div>
            <div id="overnight-div"></div>
            <span class="buttonaddrate">
                <a class="button large_button add" id="add-overnight"><span></span>Overnight</a></span>
            


        </div>
        <div id="transfers" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
            <div id="rate-div0">
                <div class="remove">
                    <input type="text" class="transfer four columns" placeholder="Transfer" />
                    <input type="hidden" class="transferID" name="transfer[]" />
                    <input type="text" placeholder="# of overnights" class="two columns" name="transferNights[]"/>
                    <span class="buttonfix">&nbsp;</span>
                </div>
            </div>
                
            <div id="transfer-div"></div>
            <span class="buttonaddrate">
                <a class="button large_button add" id="add-trasnfer"><span></span>Transfer</a></span>
           
        </div>
        <div id="ferries" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
            
             <div id="rate-div0">
                <div class="remove">
                    <input type="text" class="ferry four columns" placeholder="Ferry/Hydro"/>
                    <input type="hidden" class="ferryID" name="ferry[]" />
                    <input type="text" placeholder="# of overnights" class="two columns" name="ferryNights[]"/>
                    <span class="buttonfix">&nbsp;</span>
                </div>
            </div>
            <div id="ferry-div">

            </div>
            <span class="buttonaddrate">
                <a class="button large_button add" id="add-ferry"><span></span>Ferry</a>
            </span>
            
            
        </div>
        <div id="tours" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
            <div id="rate-div0">
                <div class="remove">
                    <input type="text" class="tour four columns" placeholder="Tour"/>
                    <input type="hidden" class="tourID" name="tour[]" />
                    <input type="text" placeholder="# of overnights" class="two columns" name="tourNights[]"/>
                    <span class="buttonfix">&nbsp;</span>
                </div>
            </div>
            <div id="tour-div">

            </div>
            <span class="buttonaddrate">
                <a class="button large_button add" id="add-tour"><span></span>Tour</a>
            </span>
        </div>
        <div id="cruises" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
            <div id="rate-div0">
                <div class="remove">
                    <input type="text" class="cruise four columns" placeholder="Cruise"/>
                    <input type="hidden" class="cruiseID" name="cruise[]" />
                    <input type="text" placeholder="# of overnights" class="two columns" name="cruiseNights[]"/>
                    <span class="buttonfix">&nbsp;</span>
                </div>
            </div>
            <div id="cruise-div">

            </div>
            <span class="buttonaddrate">
                <a class="button large_button add" id="add-cruise"><span></span>Cruise</a>
            </span>
        </div>
        <div id="air" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
            <div id="rate-div0">
                <div class="remove">
                    <input type="text" class="air four columns" placeholder="Air"/>
                    <input type="hidden" class="airID" name="air[]" />
                    <input type="text" placeholder="# of overnights" class="two columns" name="airNights[]"/>
                    <span class="buttonfix">&nbsp;</span>
                </div>
            </div>
            <div id="air-div">

            </div>
            <span class="buttonaddrate">
                <a class="button large_button add" id="add-air"><span></span>Air</a>
            </span>
        </div>
        <div id="extras" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
            <?php echo $form->textArea($model, 'extras', array('rows' => 6, 'cols' => 50)); ?>
            <?php echo $form->error($model, 'extras'); ?>
        </div>
        <div id="summary" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
            <?php echo $form->textArea($model, 'summary', array('rows' => 6, 'cols' => 50)); ?>
            <?php echo $form->error($model, 'summary'); ?>
        </div>
        <div id="itinerary" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
            <div class="row"> 
                <?php echo $form->textArea($model, 'dayToDayItinerary', array('id' => 'hdescription', 'class' => 'ckeditor')); ?> 
                <?php echo $form->error($model, 'dayToDayItinerary'); ?> 
            </div>
        </div>         
        <div id="services" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
            <?php echo $form->textArea($model, 'servicesIncluded', array('rows' => 6, 'cols' => 50)); ?>
            <?php echo $form->error($model, 'servicesIncluded'); ?>
        </div>
        <div id="photos" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">

            <?php
            $this->widget('ext.coco.CocoWidget'
                    , array(
                'id' => 'cocowidget1',
                'onCompleted' => "function(id,filename,jsoninfo){photoUpdate(jsoninfo,'static/uploads/greecpackage/')}",
                'onCancelled' => 'function(id,filename){ alert("cancelled"); }',
                'onMessage' => 'function(m){ alert(m); }',
                'allowedExtensions' => array('jpeg', 'jpg', 'gif', 'png'), // server-side mime-type validated
                'sizeLimit' => 2000000, // limit in server-side and in client-side
                'uploadDir' => 'static/uploads/greecpackage', // coco will @mkdir it
                // this arguments are used to send a notification
                // on a specific class when a new file is uploaded,
                'receptorClassName' => 'application.models.Photo',
                'methodName' => 'photoUploader',
                //'userdata' => $model->primaryKey,
                // controls how many files must be uploaded
                // 'defaultControllerName' => 'photo',
                // 'defaultActionName' => 'upload',
                'maxUploads' => -1, // defaults to -1 (unlimited)
                'maxUploadsReachMessage' => 'No more files allowed', // if empty, no message is shown
                // controls how many files the can select (not upload, for uploads see also: maxUploads)
                'multipleFileSelection' => true, // true or false, defaults: true
            ));
            ?>

        </div>
        <div id="map" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
            <?php echo $form->textArea($model, 'map', array('rows' => 6, 'cols' => 50)); ?>
            <?php echo $form->error($model, 'map'); ?>

        </div>
        <div id="terms"  class="ui-tabs-panel ui-widget-content ui-corner-bottom"> 
            <?php echo $form->textArea($model, 'terms', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Terms', 'id' => 'hterms', 'class' => 'ckeditor')); ?> 
            <?php echo $form->error($model, 'terms'); ?> 
        </div>
        <hr class="end-break">
    </div>
    <div class="clear">&nbsp;</div>
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<script>

    $("#add-overnight").click(function(){
        html='<div class="remove"><div class="clear"></div>';
        html+='<input type="text" class="city three columns" placeholder="City"/>';
        html+='<input type="hidden" class="cityID" name="city[]" />';
        html+='<input type="text" placeholder="# of overnights" class="two columns" name="numOverNights[]"/>';
        html+='<span class="removerate" id="removethis"><a class="button large_button delete"><span></span>Overnight</a></span>';
        html+='</div>';
        
        $("#overnight-div").append(html);
    });


    $("#add-trasnfer").click(function(){
        html='<div class="remove"><div class="clear"></div>';
        html+='<input type="text" class="trasnfer four columns" placeholder="Transfer"/>';
        html+='<input type="hidden" class="trasnferID" name="trasnfer[]" />';
        html+='<input type="text" placeholder="# of overnights" class="two columns" name="numOverNights[]"/>';
        html+='<span class="removerate" id="removethis"><a class="button large_button delete"><span></span>Transfer</a></span>';
        html+='</div>';
        
        $("#transfer-div").append(html);
    });


    $("#add-ferry").click(function(){
        html='<div class="remove"><div class="clear"></div>';
        html+='<input type="text" class="ferry four columns" placeholder="Ferry/Hydro"/>';
        html+='<input type="hidden" class="ferryID" name="ferry[]" />';
        html+='<input type="text" placeholder="# of overnights" class="two columns" name="numOverNights[]"/>';
        html+='<span class="removerate" id="removethis"><a class="button large_button delete"><span></span>Ferry</a></span>';
        html+='</div>';
        
        $("#ferry-div").append(html);
    });


    $("#add-tour").click(function(){
        html='<div class="removetour"><div class="clear"></div>';
        html+='<input type="text" class="tour" placeholder="Tour"/>';
        html+='<input type="hidden" class="tourID" name="tour[]" />';
        html+='<input type="text" placeholder="# of overnights" name="numOverNights[]"/>';
        html+='<span class="removerate" id="removethis"><a class="button large_button delete"><span></span>Tour</a></span>';
        html+='<div class="clear"></div>';
        html+='</div>';
        
        $("#tour-div").append(html);
    });


    $("#add-cruise").click(function(){
        html='<div class="remove"><div class="clear"></div>';
        html+='<input type="text" class="cruise four columns" placeholder="Cruise"/>';
        html+='<input type="hidden" class="cruiseID" name="cruise[]" />';
        html+='<input type="text" placeholder="# of overnights" class="two columns" name="numOverNights[]"/>';
        html+='<span class="removerate" id="removethis"><a class="button large_button delete"><span></span>Cruise</a></span>';

        html+='</div>';
        
        $("#cruise-div").append(html);
    });


    $("#add-air").click(function(){
        html='<div class="remove"><div class="clear"></div>';
        html+='<input type="text" class="air four columns" placeholder="Air"/>';
        html+='<input type="hidden" class="airID" name="air[]" />';
        html+='<input type="text" placeholder="# of overnights" class="two columns" name="numOverNights[]"/>';
        html+='<span class="removerate" id="removethis"><a class="button large_button delete"><span></span>Air</a></span>';
        html+='</div>';
        
        $("#air-div").append(html);
    });

	$('#removethis').live('click',function(e){
        $(this).parents('.remove').remove();

    });

</script>





<script>
    $('body').on('focus',".city", function(){
        $(this).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "<?php echo Yii::app()->request->baseUrl; ?>/autocom/city/",
                    dataType: "json",
                    data: {
                        featureClass: "P",
                        style: "full",
                        maxRows: 12,
                        term: request.term
                    },
                    success: function( data ) {
                        response( $.map( data, function( item ) {
                            return {
                                label: item.item,
                                value: item.id
                            }
                        }));
                    }
                });
            },
            minLength: 2,
            select: function (event, ui) {
                //alert(ui.item.value);
                $(this).val(ui.item.label);
                // update what is displayed in the textbox
                $(this).next('.cityID').val(ui.item.value); 
                return false;
            }
        });
    });

</script>
<script>
    $('body').on('focus',".transfer", function(){
        $(this).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "<?php echo Yii::app()->request->baseUrl; ?>/autocom/city/",
                    dataType: "json",
                    data: {
                        featureClass: "P",
                        style: "full",
                        maxRows: 12,
                        term: request.term
                    },
                    success: function( data ) {
                        response( $.map( data, function( item ) {
                            return {
                                label: item.item,
                                value: item.id
                            }
                        }));
                    }
                });
            },
            minLength: 2,
            select: function (event, ui) {
                //alert(ui.item.value);
                $(this).val(ui.item.label);
                // update what is displayed in the textbox
                $(this).next('.transferID').val(ui.item.value); 
                return false;
            }
        });
    });

</script>
<script>
    $('body').on('focus',".ferry", function(){
        $(this).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "<?php echo Yii::app()->request->baseUrl; ?>/autocom/city/",
                    dataType: "json",
                    data: {
                        featureClass: "P",
                        style: "full",
                        maxRows: 12,
                        term: request.term
                    },
                    success: function( data ) {
                        response( $.map( data, function( item ) {
                            return {
                                label: item.item,
                                value: item.id
                            }
                        }));
                    }
                });
            },
            minLength: 2,
            select: function (event, ui) {
                //alert(ui.item.value);
                $(this).val(ui.item.label);
                // update what is displayed in the textbox
                $(this).next('.ferryID').val(ui.item.value); 
                return false;
            }
        });
    });

</script>
<script>
    $('body').on('focus',".tour", function(){
        $(this).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "<?php echo Yii::app()->request->baseUrl; ?>/autocom/city/",
                    dataType: "json",
                    data: {
                        featureClass: "P",
                        style: "full",
                        maxRows: 12,
                        term: request.term
                    },
                    success: function( data ) {
                        response( $.map( data, function( item ) {
                            return {
                                label: item.item,
                                value: item.id
                            }
                        }));
                    }
                });
            },
            minLength: 2,
            select: function (event, ui) {
                //alert(ui.item.value);
                $(this).val(ui.item.label);
                // update what is displayed in the textbox
                $(this).next('.tourID').val(ui.item.value); 
                return false;
            }
        });
    });

</script>
<script>
    $('body').on('focus',".cruise", function(){
        $(this).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "<?php echo Yii::app()->request->baseUrl; ?>/autocom/city/",
                    dataType: "json",
                    data: {
                        featureClass: "P",
                        style: "full",
                        maxRows: 12,
                        term: request.term
                    },
                    success: function( data ) {
                        response( $.map( data, function( item ) {
                            return {
                                label: item.item,
                                value: item.id
                            }
                        }));
                    }
                });
            },
            minLength: 2,
            select: function (event, ui) {
                //alert(ui.item.value);
                $(this).val(ui.item.label);
                // update what is displayed in the textbox
                $(this).next('.cruiseID').val(ui.item.value); 
                return false;
            }
        });
    });

</script>
<script>
    $('body').on('focus',".air", function(){
        $(this).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "<?php echo Yii::app()->request->baseUrl; ?>/autocom/city/",
                    dataType: "json",
                    data: {
                        featureClass: "P",
                        style: "full",
                        maxRows: 12,
                        term: request.term
                    },
                    success: function( data ) {
                        response( $.map( data, function( item ) {
                            return {
                                label: item.item,
                                value: item.id
                            }
                        }));
                    }
                });
            },
            minLength: 2,
            select: function (event, ui) {
                //alert(ui.item.value);
                $(this).val(ui.item.label);
                // update what is displayed in the textbox
                $(this).next('.airID').val(ui.item.value); 
                return false;
            }
        });
    });

</script>
<script>
    $('body').on('focus',"#package", function(){
        $(this).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "<?php echo Yii::app()->request->baseUrl; ?>/autocom/packageclass/",
                    dataType: "json",
                    data: {
                        featureClass: "P",
                        style: "full",
                        maxRows: 12,
                        term: request.term
                    },
                    success: function( data ) {
                        response( $.map( data, function( item ) {
                            return {
                                label: item.item,
                                value: item.id
                            }
                        }));
                    }
                });
            },
            minLength: 2,
            select: function (event, ui) {
                //alert(ui.item.value);
                $(this).val(ui.item.label);
                // update what is displayed in the textbox
                $("#packageID").val(ui.item.value); 
                return false;
            }
        });
    });

</script>