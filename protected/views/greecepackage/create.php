<?php
/* @var $this GreecepackageController */
/* @var $model Greecepackage */

$this->breadcrumbs=array(
	'Greecepackages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Greecepackage', 'url'=>array('index')),
	array('label'=>'Manage Greecepackage', 'url'=>array('admin')),
);
?>

<h1>Create Greece package</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>