<?php
/* @var $this GreecepackageController */
/* @var $model Greecepackage */

$this->breadcrumbs=array(
	'Greecepackages'=>array('index'),
	$model->name=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Greecepackage', 'url'=>array('index')),
	array('label'=>'Create Greecepackage', 'url'=>array('create')),
	array('label'=>'View Greecepackage', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Greecepackage', 'url'=>array('admin')),
);
?>

<h1>Update Greecepackage <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>