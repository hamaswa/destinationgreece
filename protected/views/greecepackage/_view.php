<?php
/* @var $this GreecepackageController */
/* @var $data Greecepackage */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('departsonDate')); ?>:</b>
	<?php echo CHtml::encode($data->departsonDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('visiting')); ?>:</b>
	<?php echo CHtml::encode($data->visiting); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('packageClass')); ?>:</b>
	<?php echo CHtml::encode($data->packageClass); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numnights')); ?>:</b>
	<?php echo CHtml::encode($data->numnights); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('numdays')); ?>:</b>
	<?php echo CHtml::encode($data->numdays); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('summary')); ?>:</b>
	<?php echo CHtml::encode($data->summary); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dayToDayItinerary')); ?>:</b>
	<?php echo CHtml::encode($data->dayToDayItinerary); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('servicesIncluded')); ?>:</b>
	<?php echo CHtml::encode($data->servicesIncluded); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('map')); ?>:</b>
	<?php echo CHtml::encode($data->map); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('terms')); ?>:</b>
	<?php echo CHtml::encode($data->terms); ?>
	<br />

	*/ ?>

</div>