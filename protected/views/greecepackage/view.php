<?php
/* @var $this GreecepackageController */
/* @var $model Greecepackage */

$this->breadcrumbs=array(
	'Greecepackages'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Greecepackage', 'url'=>array('index')),
	array('label'=>'Create Greecepackage', 'url'=>array('create')),
	array('label'=>'Update Greecepackage', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Greecepackage', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Greecepackage', 'url'=>array('admin')),
);
?>

<h1>View Greecepackage #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'name',
		'departsonDate',
		'visiting',
		'status',
		'packageClass',
		'numnights',
		'numdays',
		'summary',
		'dayToDayItinerary',
		'servicesIncluded',
		'map',
		'terms',
	),
)); ?>
