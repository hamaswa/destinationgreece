<?php
/* @var $this FacilityController */
/* @var $model Facility */

$this->breadcrumbs=array(
	'Facilities'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Facility', 'url'=>array('index')),
	array('label'=>'Create Facility', 'url'=>array('create')),
	array('label'=>'Update Facility', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Facility', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Facility', 'url'=>array('admin')),
);
?>

<h1>View Facility #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'name',
	),
)); ?>
