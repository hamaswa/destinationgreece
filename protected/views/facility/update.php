<?php
/* @var $this FacilityController */
/* @var $model Facility */

$this->breadcrumbs=array(
	'Facilities'=>array('index'),
	$model->name=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Facility', 'url'=>array('index')),
	array('label'=>'Create Facility', 'url'=>array('create')),
	array('label'=>'View Facility', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Facility', 'url'=>array('admin')),
);
?>

<h1>Update Facility <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>