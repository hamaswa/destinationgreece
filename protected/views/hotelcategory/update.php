<?php
/* @var $this HotelcategoryController */
/* @var $model Hotelcategory */

$this->breadcrumbs=array(
	'Hotelcategories'=>array('index'),
	$model->name=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Hotelcategory', 'url'=>array('index')),
	array('label'=>'Create Hotelcategory', 'url'=>array('create')),
	array('label'=>'View Hotelcategory', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Hotelcategory', 'url'=>array('admin')),
);
?>

<h1>Update Hotelcategory <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>