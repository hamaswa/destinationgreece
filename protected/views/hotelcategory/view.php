<?php
/* @var $this HotelcategoryController */
/* @var $model Hotelcategory */

$this->breadcrumbs=array(
	'Hotelcategories'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Hotelcategory', 'url'=>array('index')),
	array('label'=>'Create Hotelcategory', 'url'=>array('create')),
	array('label'=>'Update Hotelcategory', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Hotelcategory', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Hotelcategory', 'url'=>array('admin')),
);
?>

<h1>View Hotelcategory #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'name',
	),
)); ?>
