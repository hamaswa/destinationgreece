<?php
/* @var $this HotelcategoryController */
/* @var $model Hotelcategory */

$this->breadcrumbs=array(
	'Hotelcategories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Hotelcategory', 'url'=>array('index')),
	array('label'=>'Manage Hotelcategory', 'url'=>array('admin')),
);
?>

<h1>Create Hotelcategory</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>