<?php
/* @var $this HotelcategoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Hotelcategories',
);

$this->menu=array(
	array('label'=>'Create Hotelcategory', 'url'=>array('create')),
	array('label'=>'Manage Hotelcategory', 'url'=>array('admin')),
);
?>

<h1>Hotelcategories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
