<?php
/* @var $this CeremonyController */
/* @var $model Ceremony */

$this->breadcrumbs=array(
	'Ceremonies'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Ceremony', 'url'=>array('index')),
	array('label'=>'Manage Ceremony', 'url'=>array('admin')),
);
?>

<h1>Create Ceremony</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>