<?php
/* @var $this CeremonyController */
/* @var $model Ceremony */

$this->breadcrumbs=array(
	'Ceremonies'=>array('index'),
	$model->name=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Ceremony', 'url'=>array('index')),
	array('label'=>'Create Ceremony', 'url'=>array('create')),
	array('label'=>'View Ceremony', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Ceremony', 'url'=>array('admin')),
);
?>

<h1>Update Ceremony <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>