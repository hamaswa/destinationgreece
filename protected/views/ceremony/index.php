<?php
/* @var $this CeremonyController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ceremonies',
);

$this->menu=array(
	array('label'=>'Create Ceremony', 'url'=>array('create')),
	array('label'=>'Manage Ceremony', 'url'=>array('admin')),
);
?>

<h1>Ceremonies</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
