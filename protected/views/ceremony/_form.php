<?php
/* @var $this CeremonyController */
/* @var $model Ceremony */
/* @var $form CActiveForm */
?>

   <div class="eleven columns" style="margin-left:20% !important;margin-right:20% !important;">
      <h3 class="form-heading">Add Agent</h3>
      <div id="response"></div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ceremony-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="five columns">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'city'); ?>
		<?php echo $form->textField($model,'city'); ?>
		<?php echo $form->error($model,'city'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'providerID'); ?>
		<?php echo $form->textField($model,'providerID'); ?>
		<?php echo $form->error($model,'providerID'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'providerEmail'); ?>
		<?php echo $form->textField($model,'providerEmail',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'providerEmail'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'providerPhone'); ?>
		<?php echo $form->textField($model,'providerPhone',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'providerPhone'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'summary'); ?>
		<?php echo $form->textArea($model,'summary',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'summary'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'rates'); ?>
		<?php echo $form->textArea($model,'rates',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'rates'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'terms'); ?>
		<?php echo $form->textArea($model,'terms',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'terms'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->