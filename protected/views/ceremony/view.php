<?php
/* @var $this CeremonyController */
/* @var $model Ceremony */

$this->breadcrumbs=array(
	'Ceremonies'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Ceremony', 'url'=>array('index')),
	array('label'=>'Create Ceremony', 'url'=>array('create')),
	array('label'=>'Update Ceremony', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Ceremony', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Ceremony', 'url'=>array('admin')),
);
?>

<h1>View Ceremony #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'name',
		'city',
		'providerID',
		'providerEmail',
		'providerPhone',
		'status',
		'summary',
		'description',
		'rates',
		'terms',
	),
)); ?>
