<?php
/* @var $this CruisecabintypeController */
/* @var $model Cruisecabintype */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID'); ?>
		<?php echo $form->textField($model,'ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cruiseID'); ?>
		<?php echo $form->textField($model,'cruiseID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cabintypeID'); ?>
		<?php echo $form->textField($model,'cabintypeID',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->