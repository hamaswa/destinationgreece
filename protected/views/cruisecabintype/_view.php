<?php
/* @var $this CruisecabintypeController */
/* @var $data Cruisecabintype */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cruiseID')); ?>:</b>
	<?php echo CHtml::encode($data->cruiseID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cabintypeID')); ?>:</b>
	<?php echo CHtml::encode($data->cabintypeID); ?>
	<br />


</div>