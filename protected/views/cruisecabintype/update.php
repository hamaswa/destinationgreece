<?php
/* @var $this CruisecabintypeController */
/* @var $model Cruisecabintype */

$this->breadcrumbs=array(
	'Cruisecabintypes'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Cruisecabintype', 'url'=>array('index')),
	array('label'=>'Create Cruisecabintype', 'url'=>array('create')),
	array('label'=>'View Cruisecabintype', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Cruisecabintype', 'url'=>array('admin')),
);
?>

<h1>Update Cruisecabintype <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>