<?php
/* @var $this CruisecabintypeController */
/* @var $model Cruisecabintype */
/* @var $form CActiveForm */
?>

   <div class="eleven columns" style="margin-left:20% !important;margin-right:20% !important;">
      <h3 class="form-heading">Add Agent</h3>
      <div id="response"></div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cruisecabintype-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="five columns">
		<?php echo $form->labelEx($model,'cruiseID'); ?>
		<?php echo $form->textField($model,'cruiseID'); ?>
		<?php echo $form->error($model,'cruiseID'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'cabintypeID'); ?>
		<?php echo $form->textField($model,'cabintypeID',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'cabintypeID'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->