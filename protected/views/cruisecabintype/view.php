<?php
/* @var $this CruisecabintypeController */
/* @var $model Cruisecabintype */

$this->breadcrumbs=array(
	'Cruisecabintypes'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Cruisecabintype', 'url'=>array('index')),
	array('label'=>'Create Cruisecabintype', 'url'=>array('create')),
	array('label'=>'Update Cruisecabintype', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Cruisecabintype', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Cruisecabintype', 'url'=>array('admin')),
);
?>

<h1>View Cruisecabintype #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'cruiseID',
		'cabintypeID',
	),
)); ?>
