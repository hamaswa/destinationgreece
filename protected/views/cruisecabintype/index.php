<?php
/* @var $this CruisecabintypeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cruisecabintypes',
);

$this->menu=array(
	array('label'=>'Create Cruisecabintype', 'url'=>array('create')),
	array('label'=>'Manage Cruisecabintype', 'url'=>array('admin')),
);
?>

<h1>Cruisecabintypes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
