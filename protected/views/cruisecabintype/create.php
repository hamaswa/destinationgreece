<?php
/* @var $this CruisecabintypeController */
/* @var $model Cruisecabintype */

$this->breadcrumbs=array(
	'Cruisecabintypes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Cruisecabintype', 'url'=>array('index')),
	array('label'=>'Manage Cruisecabintype', 'url'=>array('admin')),
);
?>

<h1>Create Cruisecabintype</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>