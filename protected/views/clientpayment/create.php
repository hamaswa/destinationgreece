<?php
/* @var $this ClientpaymentController */
/* @var $model Clientpayment */

$this->breadcrumbs=array(
	'Clientpayments'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Clientpayment', 'url'=>array('index')),
	array('label'=>'Manage Clientpayment', 'url'=>array('admin')),
);
?>

<h1>Create Clientpayment</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>