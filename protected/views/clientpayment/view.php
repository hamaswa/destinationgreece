<?php
/* @var $this ClientpaymentController */
/* @var $model Clientpayment */

$this->breadcrumbs=array(
	'Clientpayments'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Clientpayment', 'url'=>array('index')),
	array('label'=>'Create Clientpayment', 'url'=>array('create')),
	array('label'=>'Update Clientpayment', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Clientpayment', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Clientpayment', 'url'=>array('admin')),
);
?>

<h1>View Clientpayment #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'clientid',
		'amount',
		'date',
		'paymentMethod',
	),
)); ?>
