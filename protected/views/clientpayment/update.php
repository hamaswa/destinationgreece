<?php
/* @var $this ClientpaymentController */
/* @var $model Clientpayment */

$this->breadcrumbs=array(
	'Clientpayments'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Clientpayment', 'url'=>array('index')),
	array('label'=>'Create Clientpayment', 'url'=>array('create')),
	array('label'=>'View Clientpayment', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Clientpayment', 'url'=>array('admin')),
);
?>

<h1>Update Clientpayment <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>