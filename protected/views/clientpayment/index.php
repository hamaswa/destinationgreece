<?php
/* @var $this ClientpaymentController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Clientpayments',
);

$this->menu=array(
	array('label'=>'Create Clientpayment', 'url'=>array('create')),
	array('label'=>'Manage Clientpayment', 'url'=>array('admin')),
);
?>

<h1>Clientpayments</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
