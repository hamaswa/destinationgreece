<?php
/* @var $this GreecepackageclassController */
/* @var $model Greecepackageclass */

$this->breadcrumbs=array(
	'Greecepackageclasses'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Greecepackageclass', 'url'=>array('index')),
	array('label'=>'Create Greecepackageclass', 'url'=>array('create')),
	array('label'=>'View Greecepackageclass', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Greecepackageclass', 'url'=>array('admin')),
);
?>

<h1>Update Greecepackageclass <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>