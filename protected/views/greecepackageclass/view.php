<?php
/* @var $this GreecepackageclassController */
/* @var $model Greecepackageclass */

$this->breadcrumbs=array(
	'Greecepackageclasses'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Greecepackageclass', 'url'=>array('index')),
	array('label'=>'Create Greecepackageclass', 'url'=>array('create')),
	array('label'=>'Update Greecepackageclass', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Greecepackageclass', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Greecepackageclass', 'url'=>array('admin')),
);
?>

<h1>View Greecepackageclass #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'class',
	),
)); ?>
