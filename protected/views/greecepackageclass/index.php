<?php
/* @var $this GreecepackageclassController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Greecepackageclasses',
);

$this->menu=array(
	array('label'=>'Create Greecepackageclass', 'url'=>array('create')),
	array('label'=>'Manage Greecepackageclass', 'url'=>array('admin')),
);
?>

<h1>Greecepackageclasses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
