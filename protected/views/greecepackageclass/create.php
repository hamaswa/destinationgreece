<?php
/* @var $this GreecepackageclassController */
/* @var $model Greecepackageclass */

$this->breadcrumbs=array(
	'Greecepackageclasses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Greecepackageclass', 'url'=>array('index')),
	array('label'=>'Manage Greecepackageclass', 'url'=>array('admin')),
);
?>

<h1>Create Greecepackageclass</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>