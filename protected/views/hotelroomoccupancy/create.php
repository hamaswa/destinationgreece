<?php
/* @var $this HotelroomoccupancyController */
/* @var $model Hotelroomoccupancy */

$this->breadcrumbs=array(
	'Hotelroomoccupancies'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Hotelroomoccupancy', 'url'=>array('index')),
	array('label'=>'Manage Hotelroomoccupancy', 'url'=>array('admin')),
);
?>

<h1>Create Hotelroomoccupancy</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>