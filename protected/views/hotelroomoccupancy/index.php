<?php
/* @var $this HotelroomoccupancyController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Hotelroomoccupancies',
);

$this->menu=array(
	array('label'=>'Create Hotelroomoccupancy', 'url'=>array('create')),
	array('label'=>'Manage Hotelroomoccupancy', 'url'=>array('admin')),
);
?>

<h1>Hotelroomoccupancies</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
