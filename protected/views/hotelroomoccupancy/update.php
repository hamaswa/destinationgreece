<?php
/* @var $this HotelroomoccupancyController */
/* @var $model Hotelroomoccupancy */

$this->breadcrumbs=array(
	'Hotelroomoccupancies'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Hotelroomoccupancy', 'url'=>array('index')),
	array('label'=>'Create Hotelroomoccupancy', 'url'=>array('create')),
	array('label'=>'View Hotelroomoccupancy', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Hotelroomoccupancy', 'url'=>array('admin')),
);
?>

<h1>Update Hotelroomoccupancy <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>