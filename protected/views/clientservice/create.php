<?php
/* @var $this ClientserviceController */
/* @var $model Clientservice */

$this->breadcrumbs=array(
	'Clientservices'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Clientservice', 'url'=>array('index')),
	array('label'=>'Manage Clientservice', 'url'=>array('admin')),
);
?>

<h1>Create Clientservice</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>