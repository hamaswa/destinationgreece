<?php
/* @var $this ClientserviceController */
/* @var $data Clientservice */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('quoteID')); ?>:</b>
	<?php echo CHtml::encode($data->quoteID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('service')); ?>:</b>
	<?php echo CHtml::encode($data->service); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('city')); ?>:</b>
	<?php echo CHtml::encode($data->city); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numOfNights')); ?>:</b>
	<?php echo CHtml::encode($data->numOfNights); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hotel')); ?>:</b>
	<?php echo CHtml::encode($data->hotel); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('roomOccupency')); ?>:</b>
	<?php echo CHtml::encode($data->roomOccupency); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('roomtype')); ?>:</b>
	<?php echo CHtml::encode($data->roomtype); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('accomodationBasis')); ?>:</b>
	<?php echo CHtml::encode($data->accomodationBasis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rate')); ?>:</b>
	<?php echo CHtml::encode($data->rate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('transferFromTo')); ?>:</b>
	<?php echo CHtml::encode($data->transferFromTo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('transferPassengersNum')); ?>:</b>
	<?php echo CHtml::encode($data->transferPassengersNum); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('transferRate')); ?>:</b>
	<?php echo CHtml::encode($data->transferRate); ?>
	<br />

	*/ ?>

</div>