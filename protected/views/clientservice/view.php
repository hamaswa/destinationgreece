<?php
/* @var $this ClientserviceController */
/* @var $model Clientservice */

$this->breadcrumbs=array(
	'Clientservices'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Clientservice', 'url'=>array('index')),
	array('label'=>'Create Clientservice', 'url'=>array('create')),
	array('label'=>'Update Clientservice', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Clientservice', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Clientservice', 'url'=>array('admin')),
);
?>

<h1>View Clientservice #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'quoteID',
		'date',
		'service',
		'city',
		'numOfNights',
		'hotel',
		'roomOccupency',
		'roomtype',
		'accomodationBasis',
		'rate',
		'transferFromTo',
		'transferPassengersNum',
		'transferRate',
	),
)); ?>
