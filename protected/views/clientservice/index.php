<?php
/* @var $this ClientserviceController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Clientservices',
);

$this->menu=array(
	array('label'=>'Create Clientservice', 'url'=>array('create')),
	array('label'=>'Manage Clientservice', 'url'=>array('admin')),
);
?>

<h1>Clientservices</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
