<?php
/* @var $this ClientserviceController */
/* @var $model Clientservice */

$this->breadcrumbs=array(
	'Clientservices'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Clientservice', 'url'=>array('index')),
	array('label'=>'Create Clientservice', 'url'=>array('create')),
	array('label'=>'View Clientservice', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Clientservice', 'url'=>array('admin')),
);
?>

<h1>Update Clientservice <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>