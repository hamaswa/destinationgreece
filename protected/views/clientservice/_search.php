<?php
/* @var $this ClientserviceController */
/* @var $model Clientservice */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID'); ?>
		<?php echo $form->textField($model,'ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'quoteID'); ?>
		<?php echo $form->textField($model,'quoteID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'service'); ?>
		<?php echo $form->textField($model,'service'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'city'); ?>
		<?php echo $form->textField($model,'city'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'numOfNights'); ?>
		<?php echo $form->textField($model,'numOfNights'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hotel'); ?>
		<?php echo $form->textField($model,'hotel'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'roomOccupency'); ?>
		<?php echo $form->textField($model,'roomOccupency'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'roomtype'); ?>
		<?php echo $form->textField($model,'roomtype'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'accomodationBasis'); ?>
		<?php echo $form->textField($model,'accomodationBasis'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rate'); ?>
		<?php echo $form->textField($model,'rate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'transferFromTo'); ?>
		<?php echo $form->textField($model,'transferFromTo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'transferPassengersNum'); ?>
		<?php echo $form->textField($model,'transferPassengersNum'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'transferRate'); ?>
		<?php echo $form->textField($model,'transferRate',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->