<?php
/* @var $this ClientserviceController */
/* @var $model Clientservice */
/* @var $form CActiveForm */
?>

   <div class="eleven columns" style="margin-left:20% !important;margin-right:20% !important;">
      <h3 class="form-heading">Add Agent</h3>
      <div id="response"></div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'clientservice-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="five columns">
		<?php echo $form->labelEx($model,'quoteID'); ?>
		<?php echo $form->textField($model,'quoteID'); ?>
		<?php echo $form->error($model,'quoteID'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
		<?php echo $form->error($model,'date'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'service'); ?>
		<?php echo $form->textField($model,'service'); ?>
		<?php echo $form->error($model,'service'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'city'); ?>
		<?php echo $form->textField($model,'city'); ?>
		<?php echo $form->error($model,'city'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'numOfNights'); ?>
		<?php echo $form->textField($model,'numOfNights'); ?>
		<?php echo $form->error($model,'numOfNights'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'hotel'); ?>
		<?php echo $form->textField($model,'hotel'); ?>
		<?php echo $form->error($model,'hotel'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'roomOccupency'); ?>
		<?php echo $form->textField($model,'roomOccupency'); ?>
		<?php echo $form->error($model,'roomOccupency'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'roomtype'); ?>
		<?php echo $form->textField($model,'roomtype'); ?>
		<?php echo $form->error($model,'roomtype'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'accomodationBasis'); ?>
		<?php echo $form->textField($model,'accomodationBasis'); ?>
		<?php echo $form->error($model,'accomodationBasis'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'rate'); ?>
		<?php echo $form->textField($model,'rate'); ?>
		<?php echo $form->error($model,'rate'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'transferFromTo'); ?>
		<?php echo $form->textField($model,'transferFromTo'); ?>
		<?php echo $form->error($model,'transferFromTo'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'transferPassengersNum'); ?>
		<?php echo $form->textField($model,'transferPassengersNum'); ?>
		<?php echo $form->error($model,'transferPassengersNum'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'transferRate'); ?>
		<?php echo $form->textField($model,'transferRate',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'transferRate'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->