<?php
/* @var $this OperatorController */
/* @var $model Operator */
/* @var $form CActiveForm */
?>

   <div class="eleven columns" style="margin-left:20% !important;margin-right:20% !important;">
      <h3 class="form-heading">Add Agent</h3>
      <div id="response"></div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'operator-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="five columns">
		<?php echo $form->labelEx($model,'userID'); ?>
		<?php echo $form->textField($model,'userID'); ?>
		<?php echo $form->error($model,'userID'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'dateCreated'); ?>
		<?php echo $form->textField($model,'dateCreated'); ?>
		<?php echo $form->error($model,'dateCreated'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->