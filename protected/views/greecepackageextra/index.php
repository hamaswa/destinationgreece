<?php
/* @var $this GreecepackageextraController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Greecepackageextras',
);

$this->menu=array(
	array('label'=>'Create Greecepackageextra', 'url'=>array('create')),
	array('label'=>'Manage Greecepackageextra', 'url'=>array('admin')),
);
?>

<h1>Greecepackageextras</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
