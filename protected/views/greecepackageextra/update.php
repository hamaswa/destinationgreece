<?php
/* @var $this GreecepackageextraController */
/* @var $model Greecepackageextra */

$this->breadcrumbs=array(
	'Greecepackageextras'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Greecepackageextra', 'url'=>array('index')),
	array('label'=>'Create Greecepackageextra', 'url'=>array('create')),
	array('label'=>'View Greecepackageextra', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Greecepackageextra', 'url'=>array('admin')),
);
?>

<h1>Update Greecepackageextra <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>