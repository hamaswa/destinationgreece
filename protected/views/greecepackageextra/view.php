<?php
/* @var $this GreecepackageextraController */
/* @var $model Greecepackageextra */

$this->breadcrumbs=array(
	'Greecepackageextras'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Greecepackageextra', 'url'=>array('index')),
	array('label'=>'Create Greecepackageextra', 'url'=>array('create')),
	array('label'=>'Update Greecepackageextra', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Greecepackageextra', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Greecepackageextra', 'url'=>array('admin')),
);
?>

<h1>View Greecepackageextra #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'gpid',
		'extra',
		'numOverNights',
	),
)); ?>
