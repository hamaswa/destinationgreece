<?php
/* @var $this GreecepackageextraController */
/* @var $model Greecepackageextra */

$this->breadcrumbs=array(
	'Greecepackageextras'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Greecepackageextra', 'url'=>array('index')),
	array('label'=>'Manage Greecepackageextra', 'url'=>array('admin')),
);
?>

<h1>Create Greecepackageextra</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>