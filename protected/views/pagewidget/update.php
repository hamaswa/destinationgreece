<?php
/* @var $this PagewidgetController */
/* @var $model Pagewidget */

$this->breadcrumbs=array(
	'Pagewidgets'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Pagewidget', 'url'=>array('index')),
	array('label'=>'Create Pagewidget', 'url'=>array('create')),
	array('label'=>'View Pagewidget', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Pagewidget', 'url'=>array('admin')),
);
?>

<h1>Update Pagewidget <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>