<?php
/* @var $this PagewidgetController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pagewidgets',
);

$this->menu=array(
	array('label'=>'Create Pagewidget', 'url'=>array('create')),
	array('label'=>'Manage Pagewidget', 'url'=>array('admin')),
);
?>

<h1>Pagewidgets</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
