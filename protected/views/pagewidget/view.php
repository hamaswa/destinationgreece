<?php
/* @var $this PagewidgetController */
/* @var $model Pagewidget */

$this->breadcrumbs=array(
	'Pagewidgets'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Pagewidget', 'url'=>array('index')),
	array('label'=>'Create Pagewidget', 'url'=>array('create')),
	array('label'=>'Update Pagewidget', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Pagewidget', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Pagewidget', 'url'=>array('admin')),
);
?>

<h1>View Pagewidget #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
	),
)); ?>
