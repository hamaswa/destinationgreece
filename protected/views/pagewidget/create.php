<?php
/* @var $this PagewidgetController */
/* @var $model Pagewidget */

$this->breadcrumbs=array(
	'Pagewidgets'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Pagewidget', 'url'=>array('index')),
	array('label'=>'Manage Pagewidget', 'url'=>array('admin')),
);
?>

<h1>Create Pagewidget</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>