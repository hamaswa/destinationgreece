<?php
/* @var $this TourtypeController */
/* @var $model Tourtype */

$this->breadcrumbs=array(
	'Tourtypes'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Tourtype', 'url'=>array('index')),
	array('label'=>'Create Tourtype', 'url'=>array('create')),
	array('label'=>'Update Tourtype', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Tourtype', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Tourtype', 'url'=>array('admin')),
);
?>

<h1>View Tourtype #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'name',
	),
)); ?>
