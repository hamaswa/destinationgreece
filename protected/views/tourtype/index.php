<?php
/* @var $this TourtypeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tourtypes',
);

$this->menu=array(
	array('label'=>'Create Tourtype', 'url'=>array('create')),
	array('label'=>'Manage Tourtype', 'url'=>array('admin')),
);
?>

<h1>Tourtypes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
