<?php
/* @var $this TourtypeController */
/* @var $model Tourtype */

$this->breadcrumbs=array(
	'Tourtypes'=>array('index'),
	$model->name=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Tourtype', 'url'=>array('index')),
	array('label'=>'Create Tourtype', 'url'=>array('create')),
	array('label'=>'View Tourtype', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Tourtype', 'url'=>array('admin')),
);
?>

<h1>Update Tourtype <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>