<?php
/* @var $this TourtypeController */
/* @var $model Tourtype */

$this->breadcrumbs=array(
	'Tourtypes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Tourtype', 'url'=>array('index')),
	array('label'=>'Manage Tourtype', 'url'=>array('admin')),
);
?>

<h1>Create Tourtype</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>