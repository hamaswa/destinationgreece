<?php
/* @var $this LinkedsiteController */
/* @var $model Linkedsite */

$this->breadcrumbs=array(
	'Linkedsites'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Linkedsite', 'url'=>array('index')),
	array('label'=>'Manage Linkedsite', 'url'=>array('admin')),
);
?>

<h1>Create Linkedsite</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>