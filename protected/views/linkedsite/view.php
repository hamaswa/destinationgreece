<?php
/* @var $this LinkedsiteController */
/* @var $model Linkedsite */

$this->breadcrumbs=array(
	'Linkedsites'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Linkedsite', 'url'=>array('index')),
	array('label'=>'Create Linkedsite', 'url'=>array('create')),
	array('label'=>'Update Linkedsite', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Linkedsite', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Linkedsite', 'url'=>array('admin')),
);
?>

<h1>View Linkedsite #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'title',
		'description',
		'url',
		'date',
	),
)); ?>
