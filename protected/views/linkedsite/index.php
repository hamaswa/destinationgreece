<?php
/* @var $this LinkedsiteController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Linkedsites',
);

$this->menu=array(
	array('label'=>'Create Linkedsite', 'url'=>array('create')),
	array('label'=>'Manage Linkedsite', 'url'=>array('admin')),
);
?>

<h1>Linkedsites</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
