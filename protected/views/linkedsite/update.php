<?php
/* @var $this LinkedsiteController */
/* @var $model Linkedsite */

$this->breadcrumbs=array(
	'Linkedsites'=>array('index'),
	$model->title=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Linkedsite', 'url'=>array('index')),
	array('label'=>'Create Linkedsite', 'url'=>array('create')),
	array('label'=>'View Linkedsite', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Linkedsite', 'url'=>array('admin')),
);
?>

<h1>Update Linkedsite <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>