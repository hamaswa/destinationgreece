<?php
/* @var $this GrouprequestController */
/* @var $model Grouprequest */

$this->breadcrumbs=array(
	'Grouprequests'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Grouprequest', 'url'=>array('index')),
	array('label'=>'Create Grouprequest', 'url'=>array('create')),
	array('label'=>'View Grouprequest', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Grouprequest', 'url'=>array('admin')),
);
?>

<h1>Update Grouprequest <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>