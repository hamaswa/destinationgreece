<?php
/* @var $this GrouprequestController */
/* @var $model Grouprequest */

$this->breadcrumbs=array(
	'Grouprequests'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Grouprequest', 'url'=>array('index')),
	array('label'=>'Create Grouprequest', 'url'=>array('create')),
	array('label'=>'Update Grouprequest', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Grouprequest', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Grouprequest', 'url'=>array('admin')),
);
?>

<h1>View Grouprequest #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'firstName',
		'middleName',
		'lastName',
		'email',
		'phone',
		'cellPhone',
		'address',
		'zip',
		'city',
		'state',
		'country',
		'groupName',
		'placestoVisit',
		'departingUSon',
		'returningtoUSon',
		'numOfAdultsTravelling',
		'numberOfChildren',
		'numOfSingleRooms',
		'numOfDoubleRooms',
		'numOfTripleRooms',
		'numOfQuadrupleRooms',
		'hotelCategory',
		'budget',
		'additionalInfo',
	),
)); ?>
