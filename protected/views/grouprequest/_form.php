<?php
/* @var $this GrouprequestController */
/* @var $model Grouprequest */
/* @var $form CActiveForm */
?>

   <div class="eleven columns" style="margin-left:20% !important;margin-right:20% !important;">
      <h3 class="form-heading">Add Agent</h3>
      <div id="response"></div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'grouprequest-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="five columns">
		<?php echo $form->labelEx($model,'firstName'); ?>
		<?php echo $form->textField($model,'firstName',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'firstName'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'middleName'); ?>
		<?php echo $form->textField($model,'middleName',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'middleName'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'lastName'); ?>
		<?php echo $form->textField($model,'lastName',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'lastName'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model,'phone',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'cellPhone'); ?>
		<?php echo $form->textField($model,'cellPhone',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'cellPhone'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'address'); ?>
		<?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'address'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'zip'); ?>
		<?php echo $form->textField($model,'zip',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'zip'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'city'); ?>
		<?php echo $form->textField($model,'city'); ?>
		<?php echo $form->error($model,'city'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'state'); ?>
		<?php echo $form->textField($model,'state'); ?>
		<?php echo $form->error($model,'state'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'country'); ?>
		<?php echo $form->textField($model,'country'); ?>
		<?php echo $form->error($model,'country'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'groupName'); ?>
		<?php echo $form->textField($model,'groupName',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'groupName'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'placestoVisit'); ?>
		<?php echo $form->textField($model,'placestoVisit',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'placestoVisit'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'departingUSon'); ?>
		<?php echo $form->textField($model,'departingUSon'); ?>
		<?php echo $form->error($model,'departingUSon'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'returningtoUSon'); ?>
		<?php echo $form->textField($model,'returningtoUSon'); ?>
		<?php echo $form->error($model,'returningtoUSon'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'numOfAdultsTravelling'); ?>
		<?php echo $form->textField($model,'numOfAdultsTravelling'); ?>
		<?php echo $form->error($model,'numOfAdultsTravelling'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'numberOfChildren'); ?>
		<?php echo $form->textField($model,'numberOfChildren'); ?>
		<?php echo $form->error($model,'numberOfChildren'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'numOfSingleRooms'); ?>
		<?php echo $form->textField($model,'numOfSingleRooms'); ?>
		<?php echo $form->error($model,'numOfSingleRooms'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'numOfDoubleRooms'); ?>
		<?php echo $form->textField($model,'numOfDoubleRooms'); ?>
		<?php echo $form->error($model,'numOfDoubleRooms'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'numOfTripleRooms'); ?>
		<?php echo $form->textField($model,'numOfTripleRooms'); ?>
		<?php echo $form->error($model,'numOfTripleRooms'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'numOfQuadrupleRooms'); ?>
		<?php echo $form->textField($model,'numOfQuadrupleRooms'); ?>
		<?php echo $form->error($model,'numOfQuadrupleRooms'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'hotelCategory'); ?>
		<?php echo $form->textField($model,'hotelCategory'); ?>
		<?php echo $form->error($model,'hotelCategory'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'budget'); ?>
		<?php echo $form->textField($model,'budget'); ?>
		<?php echo $form->error($model,'budget'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'additionalInfo'); ?>
		<?php echo $form->textField($model,'additionalInfo',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'additionalInfo'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->