<?php
/* @var $this GrouprequestController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Grouprequests',
);

$this->menu=array(
	array('label'=>'Create Grouprequest', 'url'=>array('create')),
	array('label'=>'Manage Grouprequest', 'url'=>array('admin')),
);
?>

<h1>Grouprequests</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
