<?php
/* @var $this GrouprequestController */
/* @var $model Grouprequest */

$this->breadcrumbs=array(
	'Grouprequests'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Grouprequest', 'url'=>array('index')),
	array('label'=>'Create Grouprequest', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#grouprequest-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Grouprequests</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'grouprequest-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'ID',
		'firstName',
		'middleName',
		'lastName',
		'email',
		'phone',
		/*
		'cellPhone',
		'address',
		'zip',
		'city',
		'state',
		'country',
		'groupName',
		'placestoVisit',
		'departingUSon',
		'returningtoUSon',
		'numOfAdultsTravelling',
		'numberOfChildren',
		'numOfSingleRooms',
		'numOfDoubleRooms',
		'numOfTripleRooms',
		'numOfQuadrupleRooms',
		'hotelCategory',
		'budget',
		'additionalInfo',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
