<?php
/* @var $this GrouprequestController */
/* @var $model Grouprequest */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID'); ?>
		<?php echo $form->textField($model,'ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'firstName'); ?>
		<?php echo $form->textField($model,'firstName',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'middleName'); ?>
		<?php echo $form->textField($model,'middleName',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lastName'); ?>
		<?php echo $form->textField($model,'lastName',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'phone'); ?>
		<?php echo $form->textField($model,'phone',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cellPhone'); ?>
		<?php echo $form->textField($model,'cellPhone',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'address'); ?>
		<?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'zip'); ?>
		<?php echo $form->textField($model,'zip',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'city'); ?>
		<?php echo $form->textField($model,'city'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'state'); ?>
		<?php echo $form->textField($model,'state'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'country'); ?>
		<?php echo $form->textField($model,'country'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'groupName'); ?>
		<?php echo $form->textField($model,'groupName',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'placestoVisit'); ?>
		<?php echo $form->textField($model,'placestoVisit',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'departingUSon'); ?>
		<?php echo $form->textField($model,'departingUSon'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'returningtoUSon'); ?>
		<?php echo $form->textField($model,'returningtoUSon'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'numOfAdultsTravelling'); ?>
		<?php echo $form->textField($model,'numOfAdultsTravelling'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'numberOfChildren'); ?>
		<?php echo $form->textField($model,'numberOfChildren'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'numOfSingleRooms'); ?>
		<?php echo $form->textField($model,'numOfSingleRooms'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'numOfDoubleRooms'); ?>
		<?php echo $form->textField($model,'numOfDoubleRooms'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'numOfTripleRooms'); ?>
		<?php echo $form->textField($model,'numOfTripleRooms'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'numOfQuadrupleRooms'); ?>
		<?php echo $form->textField($model,'numOfQuadrupleRooms'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hotelCategory'); ?>
		<?php echo $form->textField($model,'hotelCategory'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'budget'); ?>
		<?php echo $form->textField($model,'budget'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'additionalInfo'); ?>
		<?php echo $form->textField($model,'additionalInfo',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->