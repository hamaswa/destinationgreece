<?php
/* @var $this GrouprequestController */
/* @var $model Grouprequest */

$this->breadcrumbs=array(
	'Grouprequests'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Grouprequest', 'url'=>array('index')),
	array('label'=>'Manage Grouprequest', 'url'=>array('admin')),
);
?>

<h1>Create Grouprequest</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>