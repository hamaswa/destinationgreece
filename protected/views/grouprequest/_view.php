<?php
/* @var $this GrouprequestController */
/* @var $data Grouprequest */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('firstName')); ?>:</b>
	<?php echo CHtml::encode($data->firstName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('middleName')); ?>:</b>
	<?php echo CHtml::encode($data->middleName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastName')); ?>:</b>
	<?php echo CHtml::encode($data->lastName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
	<?php echo CHtml::encode($data->phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cellPhone')); ?>:</b>
	<?php echo CHtml::encode($data->cellPhone); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
	<?php echo CHtml::encode($data->address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('zip')); ?>:</b>
	<?php echo CHtml::encode($data->zip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('city')); ?>:</b>
	<?php echo CHtml::encode($data->city); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('state')); ?>:</b>
	<?php echo CHtml::encode($data->state); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('country')); ?>:</b>
	<?php echo CHtml::encode($data->country); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('groupName')); ?>:</b>
	<?php echo CHtml::encode($data->groupName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('placestoVisit')); ?>:</b>
	<?php echo CHtml::encode($data->placestoVisit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('departingUSon')); ?>:</b>
	<?php echo CHtml::encode($data->departingUSon); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('returningtoUSon')); ?>:</b>
	<?php echo CHtml::encode($data->returningtoUSon); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numOfAdultsTravelling')); ?>:</b>
	<?php echo CHtml::encode($data->numOfAdultsTravelling); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numberOfChildren')); ?>:</b>
	<?php echo CHtml::encode($data->numberOfChildren); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numOfSingleRooms')); ?>:</b>
	<?php echo CHtml::encode($data->numOfSingleRooms); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numOfDoubleRooms')); ?>:</b>
	<?php echo CHtml::encode($data->numOfDoubleRooms); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numOfTripleRooms')); ?>:</b>
	<?php echo CHtml::encode($data->numOfTripleRooms); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numOfQuadrupleRooms')); ?>:</b>
	<?php echo CHtml::encode($data->numOfQuadrupleRooms); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hotelCategory')); ?>:</b>
	<?php echo CHtml::encode($data->hotelCategory); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('budget')); ?>:</b>
	<?php echo CHtml::encode($data->budget); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('additionalInfo')); ?>:</b>
	<?php echo CHtml::encode($data->additionalInfo); ?>
	<br />

	*/ ?>

</div>