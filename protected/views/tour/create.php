<?php
/* @var $this TourController */
/* @var $model Tour */

$this->breadcrumbs=array(
	'Tours'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Tour', 'url'=>array('index')),
	array('label'=>'Manage Tour', 'url'=>array('admin')),
);
?>

<div style="float:left;margin-left:15px;"><h3>Add a Tour</h3></div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>