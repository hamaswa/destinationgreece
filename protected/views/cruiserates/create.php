<?php
/* @var $this CruiseratesController */
/* @var $model Cruiserates */

$this->breadcrumbs=array(
	'Cruiserates'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Cruiserates', 'url'=>array('index')),
	array('label'=>'Manage Cruiserates', 'url'=>array('admin')),
);
?>

<h1>Create Cruiserates</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>