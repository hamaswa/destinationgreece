<?php
/* @var $this CruiseratesController */
/* @var $model Cruiserates */

$this->breadcrumbs=array(
	'Cruiserates'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Cruiserates', 'url'=>array('index')),
	array('label'=>'Create Cruiserates', 'url'=>array('create')),
	array('label'=>'Update Cruiserates', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Cruiserates', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Cruiserates', 'url'=>array('admin')),
);
?>

<h1>View Cruiserates #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'cruiseID',
		'fromDate',
		'toDate',
		'cabinType',
		'rate',
	),
)); ?>
