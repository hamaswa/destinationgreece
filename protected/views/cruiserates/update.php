<?php
/* @var $this CruiseratesController */
/* @var $model Cruiserates */

$this->breadcrumbs=array(
	'Cruiserates'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Cruiserates', 'url'=>array('index')),
	array('label'=>'Create Cruiserates', 'url'=>array('create')),
	array('label'=>'View Cruiserates', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Cruiserates', 'url'=>array('admin')),
);
?>

<h1>Update Cruiserates <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>