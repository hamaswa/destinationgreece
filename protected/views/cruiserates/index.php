<?php
/* @var $this CruiseratesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cruiserates',
);

$this->menu=array(
	array('label'=>'Create Cruiserates', 'url'=>array('create')),
	array('label'=>'Manage Cruiserates', 'url'=>array('admin')),
);
?>

<h1>Cruiserates</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
