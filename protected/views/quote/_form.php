<?php
/* @var $this QuoteController */
/* @var $model Quote */
/* @var $form CActiveForm */
?>

   <div class="eleven columns" style="margin-left:20% !important;margin-right:20% !important;">
      <h3 class="form-heading">Add Agent</h3>
      <div id="response"></div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'quote-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="five columns">
		<?php echo $form->labelEx($model,'clientid'); ?>
		<?php echo $form->textField($model,'clientid'); ?>
		<?php echo $form->error($model,'clientid'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'num'); ?>
		<?php echo $form->textField($model,'num'); ?>
		<?php echo $form->error($model,'num'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'pax'); ?>
		<?php echo $form->textField($model,'pax',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'pax'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'currency'); ?>
		<?php echo $form->textField($model,'currency'); ?>
		<?php echo $form->error($model,'currency'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'validityDate'); ?>
		<?php echo $form->textField($model,'validityDate'); ?>
		<?php echo $form->error($model,'validityDate'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'travelAgent'); ?>
		<?php echo $form->textField($model,'travelAgent'); ?>
		<?php echo $form->error($model,'travelAgent'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'agentCommissionPc'); ?>
		<?php echo $form->textField($model,'agentCommissionPc',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'agentCommissionPc'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'dgmarkupPc'); ?>
		<?php echo $form->textField($model,'dgmarkupPc',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'dgmarkupPc'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'ccHandlingPc'); ?>
		<?php echo $form->textField($model,'ccHandlingPc',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'ccHandlingPc'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'cost'); ?>
		<?php echo $form->textField($model,'cost',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'cost'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'dgMarkup'); ?>
		<?php echo $form->textField($model,'dgMarkup',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'dgMarkup'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'dgNetPrice'); ?>
		<?php echo $form->textField($model,'dgNetPrice',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'dgNetPrice'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'agentCommission'); ?>
		<?php echo $form->textField($model,'agentCommission',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'agentCommission'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'grossRate'); ?>
		<?php echo $form->textField($model,'grossRate',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'grossRate'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'ccHandling'); ?>
		<?php echo $form->textField($model,'ccHandling',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'ccHandling'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'grandTotal'); ?>
		<?php echo $form->textField($model,'grandTotal',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'grandTotal'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'editedBy'); ?>
		<?php echo $form->textField($model,'editedBy'); ?>
		<?php echo $form->error($model,'editedBy'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->