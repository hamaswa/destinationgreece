<?php
/* @var $this QuoteController */
/* @var $model Quote */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID'); ?>
		<?php echo $form->textField($model,'ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'clientid'); ?>
		<?php echo $form->textField($model,'clientid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'num'); ?>
		<?php echo $form->textField($model,'num'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pax'); ?>
		<?php echo $form->textField($model,'pax',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'currency'); ?>
		<?php echo $form->textField($model,'currency'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'validityDate'); ?>
		<?php echo $form->textField($model,'validityDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'travelAgent'); ?>
		<?php echo $form->textField($model,'travelAgent'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'agentCommissionPc'); ?>
		<?php echo $form->textField($model,'agentCommissionPc',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dgmarkupPc'); ?>
		<?php echo $form->textField($model,'dgmarkupPc',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ccHandlingPc'); ?>
		<?php echo $form->textField($model,'ccHandlingPc',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cost'); ?>
		<?php echo $form->textField($model,'cost',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dgMarkup'); ?>
		<?php echo $form->textField($model,'dgMarkup',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dgNetPrice'); ?>
		<?php echo $form->textField($model,'dgNetPrice',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'agentCommission'); ?>
		<?php echo $form->textField($model,'agentCommission',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'grossRate'); ?>
		<?php echo $form->textField($model,'grossRate',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ccHandling'); ?>
		<?php echo $form->textField($model,'ccHandling',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'grandTotal'); ?>
		<?php echo $form->textField($model,'grandTotal',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'editedBy'); ?>
		<?php echo $form->textField($model,'editedBy'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->