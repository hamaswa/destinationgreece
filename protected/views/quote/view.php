<?php
/* @var $this QuoteController */
/* @var $model Quote */

$this->breadcrumbs=array(
	'Quotes'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Quote', 'url'=>array('index')),
	array('label'=>'Create Quote', 'url'=>array('create')),
	array('label'=>'Update Quote', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Quote', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Quote', 'url'=>array('admin')),
);
?>

<h1>View Quote #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'clientid',
		'num',
		'pax',
		'currency',
		'validityDate',
		'travelAgent',
		'agentCommissionPc',
		'dgmarkupPc',
		'ccHandlingPc',
		'cost',
		'dgMarkup',
		'dgNetPrice',
		'agentCommission',
		'grossRate',
		'ccHandling',
		'grandTotal',
		'editedBy',
	),
)); ?>
