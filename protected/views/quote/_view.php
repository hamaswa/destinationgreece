<?php
/* @var $this QuoteController */
/* @var $data Quote */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('clientid')); ?>:</b>
	<?php echo CHtml::encode($data->clientid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('num')); ?>:</b>
	<?php echo CHtml::encode($data->num); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pax')); ?>:</b>
	<?php echo CHtml::encode($data->pax); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('currency')); ?>:</b>
	<?php echo CHtml::encode($data->currency); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('validityDate')); ?>:</b>
	<?php echo CHtml::encode($data->validityDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('travelAgent')); ?>:</b>
	<?php echo CHtml::encode($data->travelAgent); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('agentCommissionPc')); ?>:</b>
	<?php echo CHtml::encode($data->agentCommissionPc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dgmarkupPc')); ?>:</b>
	<?php echo CHtml::encode($data->dgmarkupPc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ccHandlingPc')); ?>:</b>
	<?php echo CHtml::encode($data->ccHandlingPc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cost')); ?>:</b>
	<?php echo CHtml::encode($data->cost); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dgMarkup')); ?>:</b>
	<?php echo CHtml::encode($data->dgMarkup); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dgNetPrice')); ?>:</b>
	<?php echo CHtml::encode($data->dgNetPrice); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agentCommission')); ?>:</b>
	<?php echo CHtml::encode($data->agentCommission); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('grossRate')); ?>:</b>
	<?php echo CHtml::encode($data->grossRate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ccHandling')); ?>:</b>
	<?php echo CHtml::encode($data->ccHandling); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('grandTotal')); ?>:</b>
	<?php echo CHtml::encode($data->grandTotal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('editedBy')); ?>:</b>
	<?php echo CHtml::encode($data->editedBy); ?>
	<br />

	*/ ?>

</div>