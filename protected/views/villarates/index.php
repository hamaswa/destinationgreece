<?php
/* @var $this VillaratesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Villarates',
);

$this->menu=array(
	array('label'=>'Create Villarates', 'url'=>array('create')),
	array('label'=>'Manage Villarates', 'url'=>array('admin')),
);
?>

<h1>Villarates</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
