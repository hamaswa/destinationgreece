<?php
/* @var $this VillaratesController */
/* @var $model Villarates */
/* @var $form CActiveForm */
?>

   <div class="eleven columns" style="margin-left:20% !important;margin-right:20% !important;">
      <h3 class="form-heading">Add Agent</h3>
      <div id="response"></div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'villarates-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="five columns">
		<?php echo $form->labelEx($model,'villaid'); ?>
		<?php echo $form->textField($model,'villaid'); ?>
		<?php echo $form->error($model,'villaid'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'from'); ?>
		<?php echo $form->textField($model,'from'); ?>
		<?php echo $form->error($model,'from'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'to'); ?>
		<?php echo $form->textField($model,'to'); ?>
		<?php echo $form->error($model,'to'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'netRate'); ?>
		<?php echo $form->textField($model,'netRate'); ?>
		<?php echo $form->error($model,'netRate'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'dgCommission'); ?>
		<?php echo $form->textField($model,'dgCommission',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'dgCommission'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'ccHandlingpc'); ?>
		<?php echo $form->textField($model,'ccHandlingpc',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'ccHandlingpc'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->