<?php
/* @var $this VillaratesController */
/* @var $model Villarates */

$this->breadcrumbs=array(
	'Villarates'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Villarates', 'url'=>array('index')),
	array('label'=>'Create Villarates', 'url'=>array('create')),
	array('label'=>'View Villarates', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Villarates', 'url'=>array('admin')),
);
?>

<h1>Update Villarates <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>