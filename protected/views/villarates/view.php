<?php
/* @var $this VillaratesController */
/* @var $model Villarates */

$this->breadcrumbs=array(
	'Villarates'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Villarates', 'url'=>array('index')),
	array('label'=>'Create Villarates', 'url'=>array('create')),
	array('label'=>'Update Villarates', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Villarates', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Villarates', 'url'=>array('admin')),
);
?>

<h1>View Villarates #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'villaid',
		'from',
		'to',
		'netRate',
		'dgCommission',
		'ccHandlingpc',
		'status',
	),
)); ?>
