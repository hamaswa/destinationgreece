<?php
/* @var $this VillaratesController */
/* @var $data Villarates */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('villaid')); ?>:</b>
	<?php echo CHtml::encode($data->villaid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('from')); ?>:</b>
	<?php echo CHtml::encode($data->from); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('to')); ?>:</b>
	<?php echo CHtml::encode($data->to); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('netRate')); ?>:</b>
	<?php echo CHtml::encode($data->netRate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dgCommission')); ?>:</b>
	<?php echo CHtml::encode($data->dgCommission); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ccHandlingpc')); ?>:</b>
	<?php echo CHtml::encode($data->ccHandlingpc); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	*/ ?>

</div>