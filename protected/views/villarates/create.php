<?php
/* @var $this VillaratesController */
/* @var $model Villarates */

$this->breadcrumbs=array(
	'Villarates'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Villarates', 'url'=>array('index')),
	array('label'=>'Manage Villarates', 'url'=>array('admin')),
);
?>

<h1>Create Villarates</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>