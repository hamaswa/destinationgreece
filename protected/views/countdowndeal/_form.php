<?php
/* @var $this CountdowndealController */
/* @var $model Countdowndeal */
/* @var $form CActiveForm */
?>

   <div class="eleven columns" style="margin-left:20% !important;margin-right:20% !important;">
      <h3 class="form-heading">Add Agent</h3>
      <div id="response"></div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'countdowndeal-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="five columns">
		<?php echo $form->labelEx($model,'packageID'); ?>
		<?php echo $form->textField($model,'packageID'); ?>
		<?php echo $form->error($model,'packageID'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'currentSale'); ?>
		<?php echo $form->textField($model,'currentSale'); ?>
		<?php echo $form->error($model,'currentSale'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'fromDate'); ?>
		<?php echo $form->textField($model,'fromDate'); ?>
		<?php echo $form->error($model,'fromDate'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'toDate'); ?>
		<?php echo $form->textField($model,'toDate'); ?>
		<?php echo $form->error($model,'toDate'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'valueUnit'); ?>
		<?php echo $form->textField($model,'valueUnit',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'valueUnit'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'currency'); ?>
		<?php echo $form->textField($model,'currency'); ?>
		<?php echo $form->error($model,'currency'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->