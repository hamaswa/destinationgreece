<?php
/* @var $this CountdowndealController */
/* @var $model Countdowndeal */

$this->breadcrumbs=array(
	'Countdowndeals'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Countdowndeal', 'url'=>array('index')),
	array('label'=>'Create Countdowndeal', 'url'=>array('create')),
	array('label'=>'View Countdowndeal', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Countdowndeal', 'url'=>array('admin')),
);
?>

<h1>Update Countdowndeal <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>