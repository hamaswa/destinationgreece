<?php
/* @var $this CountdowndealController */
/* @var $data Countdowndeal */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('packageID')); ?>:</b>
	<?php echo CHtml::encode($data->packageID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('currentSale')); ?>:</b>
	<?php echo CHtml::encode($data->currentSale); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fromDate')); ?>:</b>
	<?php echo CHtml::encode($data->fromDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('toDate')); ?>:</b>
	<?php echo CHtml::encode($data->toDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('valueUnit')); ?>:</b>
	<?php echo CHtml::encode($data->valueUnit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('currency')); ?>:</b>
	<?php echo CHtml::encode($data->currency); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	*/ ?>

</div>