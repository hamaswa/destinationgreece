<?php
/* @var $this CountdowndealController */
/* @var $model Countdowndeal */

$this->breadcrumbs=array(
	'Countdowndeals'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Countdowndeal', 'url'=>array('index')),
	array('label'=>'Create Countdowndeal', 'url'=>array('create')),
	array('label'=>'Update Countdowndeal', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Countdowndeal', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Countdowndeal', 'url'=>array('admin')),
);
?>

<h1>View Countdowndeal #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'packageID',
		'currentSale',
		'fromDate',
		'toDate',
		'valueUnit',
		'currency',
		'status',
	),
)); ?>
