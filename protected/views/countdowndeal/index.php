<?php
/* @var $this CountdowndealController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Countdowndeals',
);

$this->menu=array(
	array('label'=>'Create Countdowndeal', 'url'=>array('create')),
	array('label'=>'Manage Countdowndeal', 'url'=>array('admin')),
);
?>

<h1>Countdowndeals</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
