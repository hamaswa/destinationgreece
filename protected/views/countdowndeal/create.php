<?php
/* @var $this CountdowndealController */
/* @var $model Countdowndeal */

$this->breadcrumbs=array(
	'Countdowndeals'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Countdowndeal', 'url'=>array('index')),
	array('label'=>'Manage Countdowndeal', 'url'=>array('admin')),
);
?>

<h1>Create Countdowndeal</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>