<?php
/* @var $this JobopportunityController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Jobopportunities',
);

$this->menu=array(
	array('label'=>'Create Jobopportunity', 'url'=>array('create')),
	array('label'=>'Manage Jobopportunity', 'url'=>array('admin')),
);
?>

<h1>Jobopportunities</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
