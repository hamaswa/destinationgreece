<?php
/* @var $this JobopportunityController */
/* @var $model Jobopportunity */

$this->breadcrumbs=array(
	'Jobopportunities'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Jobopportunity', 'url'=>array('index')),
	array('label'=>'Create Jobopportunity', 'url'=>array('create')),
	array('label'=>'Update Jobopportunity', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Jobopportunity', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Jobopportunity', 'url'=>array('admin')),
);
?>

<h1>View Jobopportunity #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'title',
		'description',
		'date',
		'applyBy',
		'language',
	),
)); ?>
