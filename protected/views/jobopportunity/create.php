<?php
/* @var $this JobopportunityController */
/* @var $model Jobopportunity */

$this->breadcrumbs=array(
	'Jobopportunities'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Jobopportunity', 'url'=>array('index')),
	array('label'=>'Manage Jobopportunity', 'url'=>array('admin')),
);
?>

<h1>Create Jobopportunity</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>