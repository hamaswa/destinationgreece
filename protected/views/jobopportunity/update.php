<?php
/* @var $this JobopportunityController */
/* @var $model Jobopportunity */

$this->breadcrumbs=array(
	'Jobopportunities'=>array('index'),
	$model->title=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Jobopportunity', 'url'=>array('index')),
	array('label'=>'Create Jobopportunity', 'url'=>array('create')),
	array('label'=>'View Jobopportunity', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Jobopportunity', 'url'=>array('admin')),
);
?>

<h1>Update Jobopportunity <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>