<?php
/* @var $this VillaController */
/* @var $model Villa */

$this->breadcrumbs=array(
	'Villas'=>array('index'),
	$model->name=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Villa', 'url'=>array('index')),
	array('label'=>'Create Villa', 'url'=>array('create')),
	array('label'=>'View Villa', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Villa', 'url'=>array('admin')),
);
?>

	<div style="float:left;margin-left:15px;"><h3>Update Villa <?php echo $model->ID; ?></h3></div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>