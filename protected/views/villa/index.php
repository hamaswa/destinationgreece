<?php
/* @var $this VillaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Villas',
);

$this->menu=array(
	array('label'=>'Create Villa', 'url'=>array('create')),
	array('label'=>'Manage Villa', 'url'=>array('admin')),
);
?>

<h1>Villas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
