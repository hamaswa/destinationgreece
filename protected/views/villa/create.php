<?php
/* @var $this VillaController */
/* @var $villa Villa */

$this->breadcrumbs = array(
    'Villas' => array('index'),
    'Create',
);

$this->menu = array(
    array('label' => 'List Villa', 'url' => array('index')),
    array('label' => 'Manage Villa', 'url' => array('admin')),
);
?>

<div style="float:left;margin-left:15px;"><h3>Create Villa</h3></div>

<?php
echo $this->renderPartial('_form', array(
    'villa' => $villa,
    'villaFacility' => $villaFacility,
    'villaPhoto' => $villaPhoto,
    'hotelRoomType' => $hotelRoomType,
    'roomType' => $roomType,
    'villaRates' => $villaRates));
?>