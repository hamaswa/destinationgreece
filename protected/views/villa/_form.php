<?php
/* @var $this VillaController */
/* @var $villa Villa */
/* @var $form CActiveForm */
?>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'villa-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>
<div class="fifteen columns hoteladd form"> 
    <?php echo $form->errorSummary($villa); ?>
    <div class="four columns"> 
        <?php echo $form->textField($villa, 'name', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Villa Name','class'=>'four columns','style'=>'max-width:210px;')); ?> 
    </div>
    <div class="four columns">
        <?php
        $sql = 'SELECT ID,  name FROM city order by name';
        $cmd = Yii::app()->db->createCommand($sql);
        $res = $cmd->queryAll();
        $arr = array();
        $arr[0] = 'Select City';
        foreach ($res as $cat) {
            $arr[$cat['ID']] = $cat['name'];
        }
        echo $form->dropDownList($villa, 'city', $arr, array('id' => 'city', 'class' => 'three columns', 'style' => 'max-width:218px;float:none;margin-left:0'));
        ?>
        <?php echo $form->error($villa, 'city'); ?> 
    </div>
    <div class="four columns">
        <?php
        $arr = array('' => 'Show the Villa', 1 => 'Yes', 0 => 'No');
        echo $form->dropDownList($villa, 'status', $arr, array('id' => 'status', 'class' => 'three columns', 'style' => 'max-width:218px;float:none;margin-left:0'));
        ?>
        <?php echo $form->error($villa, 'status'); ?> 
    </div>
    <div class="four columns" > 
        <?php echo $form->textField($villa, 'bathrooms', array('size' => 60, 'class'=>'two columns', 'maxlength' => 255, 'placeholder' => '# of Bathroooms')); ?> 
        <?php echo $form->error($villa, 'bathrooms'); ?> 
    </div>
    

    <div class="four columns" >
        <?php
        $sql = 'SELECT ID,  name FROM currency order by name';
        $cmd = Yii::app()->db->createCommand($sql);
        $res = $cmd->queryAll();
        $arr = array();
        $arr[0] = 'Currency';
        foreach ($res as $cat) {
            $arr[$cat['ID']] = $cat['name'];
        }
        echo $form->dropDownList($villa, 'currency', $arr, array('id' => 'currency',  'class' => 'three columns', 'style' => 'max-width:218px;float:none;margin-left:0'));
        ?>
        <?php echo $form->error($villa, 'currency'); ?> </div>

    <div class="four columns" > 
        <?php echo $form->textField($villa, 'alias', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Alias', 'style' => 'width:80px')); ?> 
        <?php echo $form->error($villa, 'alias'); ?> 
    </div>
    <div class="four columns" > 
        <?php echo $form->textField($villa, 'sleeps', array('size' => 60, 'maxlength' => 255, 'style' => 'width:80px', 'placeholder' => 'Sleeps')); ?> 
        <?php echo $form->error($villa, 'sleeps'); ?> 
    </div>
    <div class="four columns"  > 
        <?php echo $form->textField($villa, 'ensuite', array('size' => 60, 'maxlength' => 255, 'style' => 'width:80px', 'placeholder' => 'Ensuite')); ?> 
        <?php echo $form->error($villa, 'ensuite'); ?> 
    </div>
    <div class="four columns"  > 
        <input type="text" name="cardCom0[]" placeholder="Card %" class="two columns" />
    </div>
    <div class="clear"></div>
    <div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
        <ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header">
            <li class="ui-state-default ui-tabs-selected ui-state-active"><a  class="two columns" href="#rates">Rates</a></li>
            <li class="ui-state-default"><a class="two columns"  href="#description">Description</a></li>
            <li class="ui-state-default"><a class="two columns"  href="#facilities">Facilities</a></li>
            <li class="ui-state-default"><a class="two columns"  href="#photos">Photos</a></li>
            <li class="ui-state-default"><a  class="two columns" href="#terms">Terms</a></li>
            <li class="ui-state-default"><a class="two columns"  href="#contact">Contact</a></li>
        </ul>
        <div id="rates" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
            <div id="rate-div0">
                <div class="remove1">
                    <input type="text" name="fromDate0[]"  placeholder="From Date" class="fromDate two columns" />
                    <input type="text" name="toDate0[]"  placeholder="To Date" class="toDate two columns"/>
                    <input type="text" name="rate0[]" placeholder="Rate" class="one columns" />
                    <select name="status0[]" class="two columns status">
                        <option value="">Show Rate</option>
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>
                    <input type="text" name="dgCom0[]" placeholder="DG %" class="one columns" />
                    
                    <span class="buttonfix">&nbsp;</span>
                </div>
            </div>
            <span class="buttonaddrate"  onclick="addVillaRate('0');">
                <a class="button large_button add"><span></span>rate</a></span>
<!--            <hr />
            <div id="roomtype-div"></div>
            <span style="cursor:pointer;float:right;margin-top:15px;" id="add-roomtype"><a class="button large_button add"><span></span>room</a></span> 
-->
        </div>
        <div id="description" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
            <div class="row"> <?php echo $form->textArea($villa, 'description', array('id' => 'hdescription', 'class' => 'ckeditor')); ?>
                <?php echo $form->error($villa, 'description'); ?> 
            </div>
        </div>
        <div id="facilities" class="ui-tabs-panel ui-widget-content ui-corner-bottom"> 
            <div class="ten columns">
                <?php echo $form->textArea($villa, 'facility', array('id' => 'facility', 'placeholder' => 'Facility', 'class' => 'ckeditor')); ?> 
                <?php echo $form->error($villa, 'facility'); ?>
            </div><div class="four columns">
                <?php
                $sql = 'SELECT ID,  name FROM facility order by name';
                $cmd = Yii::app()->db->createCommand($sql);
                $res = $cmd->queryAll();
                $arr = array();
                $arr[0] = 'Select Facility';
                foreach ($res as $cat) {
                    $arr[$cat['ID']] = $cat['name'];
                }
                echo $form->dropDownList($villaFacility, 'villaFacility', $arr, array('id' => 'name', 'multiple' => 'multiple'));
                ?>
                <?php echo $form->error($villaFacility, 'villaFacility'); ?> 
            </div>
        </div>

        <div id="photos"  class="ui-tabs-panel ui-widget-content ui-corner-bottom">
            <div id="photo-div"> </div>
            <div id="diplay-photo">
                <ul id="photo-list">


                </ul>
            </div>
            <span class="clear"></span>


            <?php
            $this->widget('ext.coco.CocoWidget'
                    , array(
                'id' => 'cocowidget1',
                'onCompleted' => "function(id,filename,jsoninfo){photoUpdate(jsoninfo,'static/uploads/villas/')}",
                'onCancelled' => 'function(id,filename){ alert("cancelled"); }',
                'onMessage' => 'function(m){ alert(m); }',
                'allowedExtensions' => array('jpeg', 'jpg', 'gif', 'png'), // server-side mime-type validated
                'sizeLimit' => 2000000, // limit in server-side and in client-side
                'uploadDir' => 'static/uploads/villas', // coco will @mkdir it
                // this arguments are used to send a notification
                // on a specific class when a new file is uploaded,
                'receptorClassName' => 'application.models.Photo',
                'methodName' => 'photoUploader',
                //'userdata' => $model->primaryKey,
                // controls how many files must be uploaded
                // 'defaultControllerName' => 'photo',
                // 'defaultActionName' => 'upload',
                'maxUploads' => -1, // defaults to -1 (unlimited)
                'maxUploadsReachMessage' => 'No more files allowed', // if empty, no message is shown
                // controls how many files the can select (not upload, for uploads see also: maxUploads)
                'multipleFileSelection' => true, // true or false, defaults: true
            ));
            ?>
        </div>           
        <div id="terms"  class="ui-tabs-panel ui-widget-content ui-corner-bottom"> 
            <?php echo $form->textArea($villa, 'terms', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Terms', 'id' => 'hterms', 'class' => 'ckeditor')); ?> 
            <?php echo $form->error($villa, 'terms'); ?> 
        </div>
        <div id="contact"  class="ui-tabs-panel ui-widget-content ui-corner-bottom">
            <div class="eleven columns">
                <div class="four columns"  style="width: 220px; padding-top: 0px;" >
                    <?php
                    echo $form->textField($villa, 'address1', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Address 1'));
                    ?>
                    <?php echo $form->error($villa, 'address1'); ?> </div>
                <div class="four columns"  style="width: 220px; padding-top: 0px;" >
                    <?php
                    echo $form->textField($villa, 'address2', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Address 2'));
                    echo $form->error($villa, 'address2');
                    ?>
                </div>
                <div class="four columns"  style="width: 220px; padding-top: 0px;" >
                    <?php
                    echo $form->textField($villa, 'zip', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Zip'));
                    echo $form->error($villa, 'zip');
                    ?>
                </div>
                <div class="four columns"  style="width: 220px; padding-top: 0px;" >
                    <?php
                    echo $form->textField($villa, 'fax', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Fax'));
                    echo $form->error($villa, 'fax');
                    ?>
                </div>
                <div class="four columns"  style="width: 220px; padding-top: 0px;" >
                    <?php
                    echo $form->textField($villa, 'contactName', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Contact Name'));
                    echo $form->error($villa, 'contactName');
                    ?>
                </div>
                <div class="four columns"  style="width: 220px; padding-top: 0px;" >
                    <?php
                    echo $form->textField($villa, 'cell', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Cell Phone'));
                    echo $form->error($villa, 'cell');
                    ?>
                </div>
                <div class="four columns"  style="width: 220px; padding-top: 0px;" >
                    <?php
                    echo $form->textField($villa, 'email1', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Email 1'));
                    echo $form->error($villa, 'email1');
                    ?>
                </div>
                <div class="four columns"  style="width: 220px; padding-top: 0px;" >
                    <?php
                    echo $form->textField($villa, 'email2', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Email 2'));
                    echo $form->error($villa, 'email2');
                    ?>
                </div>
                <div class="four columns" style="width:220px;" > 
                <?php echo $form->textField($villa, 'reservationEmail', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Reservation Email')); ?> 
                <?php echo $form->error($villa, 'reservationEmail'); ?> 
                </div>
                <div class="four columns" style="width:220px;" > 
                <?php echo $form->textField($villa, 'reservationPhone', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Reservation Phone')); ?> 
                <?php echo $form->error($villa, 'reservationPhone'); ?> 
                </div>
                <div class="four columns" style="width:220px;" > 
                <?php echo $form->textField($villa, 'location', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Location')); ?> 
                <?php echo $form->error($villa, 'location'); ?> 
                </div>
            </div>
        </div>
    </div><div class="clear"></div><hr />
    <div style="float: right;margin-right: 20px;" >
        <?php echo CHtml::submitButton($villa->isNewRecord ? 'Add Villa' : 'Edit Villa'); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
<script>

<?php /*?>    $("#add-roomtype").live('click', function(){
        var numRoomTypes = $('.roomtype').length
        var str="";
        var clear="";
        //alert(numRoomTypes);
        $('input:text').each(function() {
            $(this).attr('value', $(this).val());
        });

        $('#rate-div0 :input').not(':button,:hidden').each(function(){

            if($(this).attr("placeholder")=="From Date"){
                str+='<div class="remove">'+ clear +'<input type="text" value="'+ $(this).val() +'" name="fromDate'+numRoomTypes+'[]"  placeholder="From Date" class="fromDate two columns" />';
            }
            if($(this).attr("placeholder")=="To Date"){
                str+='<input type="text"  value="'+ $(this).val() +'" name="toDate'+numRoomTypes+'[]"  placeholder="To Date" class="toDate two columns" />';
            }
            if($(this).attr("placeholder")=="Rate"){
                str+='<input type="text" name="rate'+numRoomTypes+'[]"  placeholder="Rate" class="one columns" />';
            }
            if($(this).attr("placeholder")=="DG %"){
                str+='<input type="text" name="rate'+numRoomTypes+'[]"  placeholder="DG %" class="one columns" />';
            }
            if($(this).attr("class")=="status"){
            str+='<select name="status'+numRoomTypes+'[]" class="two columns status">';
            str+='<option value="">Show Rate</option>';
            str+='<option value="0">No</option>';
            str+='<option value="1">Yes</option>';
            str+='</select>';
            }
            if($(this).attr("placeholder")=="Card %"){
                str+='<input type="text" name="rate'+numRoomTypes+'[]"  placeholder="Card %" class="one columns" /><span class="buttonclear"></span><span class="removerate" id="removethis"><a class="button large_button delete"><span></span>rate</a></span></div>';
            }
            clear='<div class="clear"></div>';
        });
                
        var html='<div class="removeroomtype"><div class="clear"></div>';
        html+='<input class="roomtype two columns" type="text" placeholder="Room type" name="roomtype'+numRoomTypes+'">';
        html+='<input id="roomtype'+numRoomTypes+'" type="hidden" name="roomtype[]">';
        html+='<input type="text" placeholder="# of adults" name="numberofadults[]" class="one columns" />';

        html+='<div id="rate-div'+numRoomTypes+'">' + str + '</div>';
        html+='<span class="buttonaddrate"  onclick="addVillaRate(\''+numRoomTypes+'\');"><a class="button large_button add"><span></span>rate</a></span><span style="cursor:pointer;float: left;position: absolute;right: 10px;" id="removeroomtype"><a class="button large_button delete"><span></span>room</a></span><hr></div>';

        $("#roomtype-div").append(html);
    });
<?php */?>
         var rangeDate=0;
    $('body').on('focus',".fromDate", function(){
        if($(this).parent('.remove1').prev('.remove1').find(".toDate").val()){

            var prvDate=$(this).parent('.remove1').prev('.remove1').find(".toDate").val();
            rangeDate = new Date(prvDate);
            rangeDate.setDate(rangeDate.getDate() + 1);
        }
        $(this).datepicker({
            dateFormat: 'yy-mm-dd',
            minDate:rangeDate,
            onSelect: function(dateStr) {
                var nextDayDate = new Date(dateStr);
                nextDayDate.setDate(nextDayDate.getDate() + 1);
                $(this).next('.toDate').datepicker({
                    dateFormat: 'yy-mm-dd',
                    minDate: nextDayDate
                }).datepicker('setDate', nextDayDate);
            }
        });
    });
        
        
    
    $('#removethis').live('click',function(e){
        $(this).parents('.remove1').remove();

    });
    $('#removeroomtype').live('click',function(e){
        $(this).parents('.removeroomtype').remove();

    });


</script>

<script>
    $('body').on('focus',".roomtype", function(){
        $(this).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "<?php echo Yii::app()->request->baseUrl; ?>/autocom/roomtype/",
                    dataType: "json",
                    data: {
                        featureClass: "P",
                        style: "full",
                        maxRows: 12,
                        name_startsWith: request.term
                    },
                    success: function( data ) {
                        response( $.map( data, function( item ) {
                            return {
                                label: item.name,
                                value: item.id
                            }
                        }));
                    }
                });
            },
            minLength: 2,
            select: function (event, ui) {
                $('#'+$(this).attr('name')).val(ui.item.value);
                // update what is displayed in the textbox
                this.value = ui.item.label; 
                return false;
            }
        });
    });
</script> 
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/ajaxupload.3.5.js" ></script> 