<?php
/* @var $this ShorextypeController */
/* @var $model Shorextype */

$this->breadcrumbs=array(
	'Shorextypes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Shorextype', 'url'=>array('index')),
	array('label'=>'Manage Shorextype', 'url'=>array('admin')),
);
?>

<h1>Create Shorextype</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>