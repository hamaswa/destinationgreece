<?php
/* @var $this ShorextypeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Shorextypes',
);

$this->menu=array(
	array('label'=>'Create Shorextype', 'url'=>array('create')),
	array('label'=>'Manage Shorextype', 'url'=>array('admin')),
);
?>

<h1>Shorextypes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
