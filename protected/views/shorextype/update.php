<?php
/* @var $this ShorextypeController */
/* @var $model Shorextype */

$this->breadcrumbs=array(
	'Shorextypes'=>array('index'),
	$model->name=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Shorextype', 'url'=>array('index')),
	array('label'=>'Create Shorextype', 'url'=>array('create')),
	array('label'=>'View Shorextype', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Shorextype', 'url'=>array('admin')),
);
?>

<h1>Update Shorextype <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>