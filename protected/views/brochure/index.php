<?php
/* @var $this BrochureController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Brochures',
);

$this->menu=array(
	array('label'=>'Create Brochure', 'url'=>array('create')),
	array('label'=>'Manage Brochure', 'url'=>array('admin')),
);
?>

<h1>Brochures</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
