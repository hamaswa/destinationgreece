<?php
/* @var $this BrochureController */
/* @var $model Brochure */

$this->breadcrumbs=array(
	'Brochures'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Brochure', 'url'=>array('index')),
	array('label'=>'Create Brochure', 'url'=>array('create')),
	array('label'=>'Update Brochure', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Brochure', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Brochure', 'url'=>array('admin')),
);
?>

<h1>View Brochure #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'file',
		'date',
		'language',
	),
)); ?>
