<?php
/* @var $this BrochureController */
/* @var $model Brochure */

$this->breadcrumbs=array(
	'Brochures'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Brochure', 'url'=>array('index')),
	array('label'=>'Manage Brochure', 'url'=>array('admin')),
);
?>

<h1>Create Brochure</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>