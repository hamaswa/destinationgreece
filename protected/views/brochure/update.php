<?php
/* @var $this BrochureController */
/* @var $model Brochure */

$this->breadcrumbs=array(
	'Brochures'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Brochure', 'url'=>array('index')),
	array('label'=>'Create Brochure', 'url'=>array('create')),
	array('label'=>'View Brochure', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Brochure', 'url'=>array('admin')),
);
?>

<h1>Update Brochure <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>