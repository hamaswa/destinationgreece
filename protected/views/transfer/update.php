<?php
/* @var $this TransferController */
/* @var $model Transfer */

$this->breadcrumbs=array(
	'Transfers'=>array('index'),
	$model->name=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Transfer', 'url'=>array('index')),
	array('label'=>'Create Transfer', 'url'=>array('create')),
	array('label'=>'View Transfer', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Transfer', 'url'=>array('admin')),
);
?>

<h1>Update Transfer <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>