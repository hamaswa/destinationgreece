<?php
/* @var $this TransferController */
/* @var $model Transfer */

$this->breadcrumbs=array(
	'Transfers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Transfer', 'url'=>array('index')),
	array('label'=>'Manage Transfer', 'url'=>array('admin')),
);
?>
<div style="float:left;margin-left:15px;">

<h3>Add Transfer</h3>
</div>
<?php echo $this->renderPartial('_form', array('transfer'=>$transfer)); ?>