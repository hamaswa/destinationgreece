<?php
/* @var $this TransferController */
/* @var $transfer Transfer */
/* @var $form CActiveForm */
?>

<div class="fifteen columns form transfer"> 


    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'transfer-form',
        'enableAjaxValidation' => false,
            ));
    ?>
    <?php echo $form->errorSummary($transfer); ?>

    <div class="four columns" style="width:220px;">

        <?php
        $sql = 'SELECT ID,  name FROM currency order by name';
        $cmd = Yii::app()->db->createCommand($sql);
        $res = $cmd->queryAll();
        $arr = array();
        $arr[0] = 'Rates in';
        foreach ($res as $cat) {
            $arr[$cat['ID']] = $cat['name'];
        }
        echo $form->dropDownList($transfer, 'currency', $arr, array('id' => 'currency'));
        ?>
        <?php echo $form->error($transfer, 'currency'); ?> 

    </div>
    <div class="four columns" style="width:220px;">

        <input type="text" placeholder="Provider" class="three columns" />

    </div>
<div class="clear"></div>

        <div class="four columns"> 
        <?php echo $form->textField($transfer, 'name', array('size' => 60, 'class' => 'transfer', 'maxlength' => 255, 'placeholder' => 'Transfer Detail')); ?>
        <?php echo $form->error($transfer, 'name'); ?>
        </div>
        <div id="rate-div0" class="transferrates">
            <div class="removevillrate">
              <input type="text" name="people[]" placeholder="People" class="one columns" />
              <input type="text" name="rate[]" placeholder="Rate" class="one columns  rate" />
			</div>
        </div>
        <div id="rate-div" class="transferrates">
            
        </div>
        
        <span class="buttonaddrate">
        <a class="button large_button add" id="add-rate"><span></span>rate</a>
     </span>

    
    <div class="clear"></div>


    <div class="clear"></div><hr />
    <div style="float: right;margin-right: 20px;" >
        <?php echo CHtml::submitButton($transfer->isNewRecord ? 'Add Transfer' : 'Change Transfer'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $("#add-rate").click(function(){
        html='<div class="removevillrate"><div class="clear"></div>';
        html+='<input type="text" name="people[]" placeholder="People" class="one columns" />';
        html+='<input type="text" name="rate[]" placeholder="Rate" class="one columns rate" />';
        html+='<span class="removerate" id="removethis"><a class="button large_button delete"><span></span>rate</a></span>';
        //html+='<a class="button large_button add" href="#" onclick="removElem(\'rate-div'+numRates+'\')">Remove Rate</a>';
        html+='</div>';
        
        $("#rate-div").append(html);
    });

	$('#removethis').live('click',function(e){
        $(this).parents('.removevillrate').remove();

    });

</script>