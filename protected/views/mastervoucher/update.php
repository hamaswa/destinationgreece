<?php
/* @var $this MastervoucherController */
/* @var $model Mastervoucher */

$this->breadcrumbs=array(
	'Mastervouchers'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Mastervoucher', 'url'=>array('index')),
	array('label'=>'Create Mastervoucher', 'url'=>array('create')),
	array('label'=>'View Mastervoucher', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Mastervoucher', 'url'=>array('admin')),
);
?>

<h1>Update Mastervoucher <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>