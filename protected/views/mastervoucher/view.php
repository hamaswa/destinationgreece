<?php
/* @var $this MastervoucherController */
/* @var $model Mastervoucher */

$this->breadcrumbs=array(
	'Mastervouchers'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Mastervoucher', 'url'=>array('index')),
	array('label'=>'Create Mastervoucher', 'url'=>array('create')),
	array('label'=>'Update Mastervoucher', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Mastervoucher', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Mastervoucher', 'url'=>array('admin')),
);
?>

<h1>View Mastervoucher #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'voucherID',
		'date',
		'quoteid',
		'editedby',
	),
)); ?>
