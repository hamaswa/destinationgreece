<?php
/* @var $this MastervoucherController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Mastervouchers',
);

$this->menu=array(
	array('label'=>'Create Mastervoucher', 'url'=>array('create')),
	array('label'=>'Manage Mastervoucher', 'url'=>array('admin')),
);
?>

<h1>Mastervouchers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
