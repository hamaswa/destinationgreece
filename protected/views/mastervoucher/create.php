<?php
/* @var $this MastervoucherController */
/* @var $model Mastervoucher */

$this->breadcrumbs=array(
	'Mastervouchers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Mastervoucher', 'url'=>array('index')),
	array('label'=>'Manage Mastervoucher', 'url'=>array('admin')),
);
?>

<h1>Create Mastervoucher</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>