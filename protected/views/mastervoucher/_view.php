<?php
/* @var $this MastervoucherController */
/* @var $data Mastervoucher */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('voucherID')); ?>:</b>
	<?php echo CHtml::encode($data->voucherID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('quoteid')); ?>:</b>
	<?php echo CHtml::encode($data->quoteid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('editedby')); ?>:</b>
	<?php echo CHtml::encode($data->editedby); ?>
	<br />


</div>