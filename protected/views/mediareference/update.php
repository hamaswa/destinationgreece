<?php
/* @var $this MediareferenceController */
/* @var $model Mediareference */

$this->breadcrumbs=array(
	'Mediareferences'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Mediareference', 'url'=>array('index')),
	array('label'=>'Create Mediareference', 'url'=>array('create')),
	array('label'=>'View Mediareference', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Mediareference', 'url'=>array('admin')),
);
?>

<h1>Update Mediareference <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>