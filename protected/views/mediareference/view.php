<?php
/* @var $this MediareferenceController */
/* @var $model Mediareference */

$this->breadcrumbs=array(
	'Mediareferences'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Mediareference', 'url'=>array('index')),
	array('label'=>'Create Mediareference', 'url'=>array('create')),
	array('label'=>'Update Mediareference', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Mediareference', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Mediareference', 'url'=>array('admin')),
);
?>

<h1>View Mediareference #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'date',
		'content',
		'source',
		'firstName',
		'lastName',
		'status',
	),
)); ?>
