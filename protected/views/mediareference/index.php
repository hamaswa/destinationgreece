<?php
/* @var $this MediareferenceController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Mediareferences',
);

$this->menu=array(
	array('label'=>'Create Mediareference', 'url'=>array('create')),
	array('label'=>'Manage Mediareference', 'url'=>array('admin')),
);
?>

<h1>Mediareferences</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
