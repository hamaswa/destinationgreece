<?php
/* @var $this MediareferenceController */
/* @var $model Mediareference */

$this->breadcrumbs=array(
	'Mediareferences'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Mediareference', 'url'=>array('index')),
	array('label'=>'Manage Mediareference', 'url'=>array('admin')),
);
?>

<h1>Create Mediareference</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>