<?php
/* @var $this NongreecepackagedateandrateController */
/* @var $data Nongreecepackagedateandrate */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nonGreecePackageID')); ?>:</b>
	<?php echo CHtml::encode($data->nonGreecePackageID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fromDate')); ?>:</b>
	<?php echo CHtml::encode($data->fromDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hotelCategory')); ?>:</b>
	<?php echo CHtml::encode($data->hotelCategory); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('roomOccupancy')); ?>:</b>
	<?php echo CHtml::encode($data->roomOccupancy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rate')); ?>:</b>
	<?php echo CHtml::encode($data->rate); ?>
	<br />


</div>