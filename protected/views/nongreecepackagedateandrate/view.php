<?php
/* @var $this NongreecepackagedateandrateController */
/* @var $model Nongreecepackagedateandrate */

$this->breadcrumbs=array(
	'Nongreecepackagedateandrates'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Nongreecepackagedateandrate', 'url'=>array('index')),
	array('label'=>'Create Nongreecepackagedateandrate', 'url'=>array('create')),
	array('label'=>'Update Nongreecepackagedateandrate', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Nongreecepackagedateandrate', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Nongreecepackagedateandrate', 'url'=>array('admin')),
);
?>

<h1>View Nongreecepackagedateandrate #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'nonGreecePackageID',
		'fromDate',
		'hotelCategory',
		'roomOccupancy',
		'rate',
	),
)); ?>
