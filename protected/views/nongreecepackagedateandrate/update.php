<?php
/* @var $this NongreecepackagedateandrateController */
/* @var $model Nongreecepackagedateandrate */

$this->breadcrumbs=array(
	'Nongreecepackagedateandrates'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Nongreecepackagedateandrate', 'url'=>array('index')),
	array('label'=>'Create Nongreecepackagedateandrate', 'url'=>array('create')),
	array('label'=>'View Nongreecepackagedateandrate', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Nongreecepackagedateandrate', 'url'=>array('admin')),
);
?>

<h1>Update Nongreecepackagedateandrate <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>