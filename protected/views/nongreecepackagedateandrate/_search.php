<?php
/* @var $this NongreecepackagedateandrateController */
/* @var $model Nongreecepackagedateandrate */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID'); ?>
		<?php echo $form->textField($model,'ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nonGreecePackageID'); ?>
		<?php echo $form->textField($model,'nonGreecePackageID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fromDate'); ?>
		<?php echo $form->textField($model,'fromDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hotelCategory'); ?>
		<?php echo $form->textField($model,'hotelCategory'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'roomOccupancy'); ?>
		<?php echo $form->textField($model,'roomOccupancy'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rate'); ?>
		<?php echo $form->textField($model,'rate',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->