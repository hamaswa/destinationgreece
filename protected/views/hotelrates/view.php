<?php
/* @var $this HotelratesController */
/* @var $model Hotelrates */

$this->breadcrumbs=array(
	'Hotelrates'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Hotelrates', 'url'=>array('index')),
	array('label'=>'Create Hotelrates', 'url'=>array('create')),
	array('label'=>'Update Hotelrates', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Hotelrates', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Hotelrates', 'url'=>array('admin')),
);
?>

<h1>View Hotelrates #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'fromDate',
		'toDate',
		'hotelID',
		'rate',
	),
)); ?>
