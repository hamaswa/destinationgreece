<?php
/* @var $this HotelratesController */
/* @var $model Hotelrates */

$this->breadcrumbs=array(
	'Hotelrates'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Hotelrates', 'url'=>array('index')),
	array('label'=>'Create Hotelrates', 'url'=>array('create')),
	array('label'=>'View Hotelrates', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Hotelrates', 'url'=>array('admin')),
);
?>

<h1>Update Hotelrates <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>