<?php
/* @var $this HotelratesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Hotelrates',
);

$this->menu=array(
	array('label'=>'Create Hotelrates', 'url'=>array('create')),
	array('label'=>'Manage Hotelrates', 'url'=>array('admin')),
);
?>

<h1>Hotelrates</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
