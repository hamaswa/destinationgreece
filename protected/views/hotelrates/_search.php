<?php
/* @var $this HotelratesController */
/* @var $model Hotelrates */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID'); ?>
		<?php echo $form->textField($model,'ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fromDate'); ?>
		<?php echo $form->textField($model,'fromDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'toDate'); ?>
		<?php echo $form->textField($model,'toDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hotelID'); ?>
		<?php echo $form->textField($model,'hotelID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rate'); ?>
		<?php echo $form->textField($model,'rate',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->