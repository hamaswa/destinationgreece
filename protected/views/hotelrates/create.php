<?php
/* @var $this HotelratesController */
/* @var $model Hotelrates */

$this->breadcrumbs=array(
	'Hotelrates'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Hotelrates', 'url'=>array('index')),
	array('label'=>'Manage Hotelrates', 'url'=>array('admin')),
);
?>

<h1>Create Hotelrates</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>