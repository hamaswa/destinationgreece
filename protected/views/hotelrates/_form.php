<?php
/* @var $this HotelratesController */
/* @var $model Hotelrates */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'hotelrates-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'fromDate'); ?>
		<?php echo $form->textField($model,'fromDate'); ?>
		<?php echo $form->error($model,'fromDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'toDate'); ?>
		<?php echo $form->textField($model,'toDate'); ?>
		<?php echo $form->error($model,'toDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hotelID'); ?>
		<?php echo $form->textField($model,'hotelID'); ?>
		<?php echo $form->error($model,'hotelID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rate'); ?>
		<?php echo $form->textField($model,'rate',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'rate'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->