<?php
/* @var $this TourrateController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tourrates',
);

$this->menu=array(
	array('label'=>'Create Tourrate', 'url'=>array('create')),
	array('label'=>'Manage Tourrate', 'url'=>array('admin')),
);
?>

<h1>Tourrates</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
