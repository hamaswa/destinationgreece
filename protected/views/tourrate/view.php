<?php
/* @var $this TourrateController */
/* @var $model Tourrate */

$this->breadcrumbs=array(
	'Tourrates'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Tourrate', 'url'=>array('index')),
	array('label'=>'Create Tourrate', 'url'=>array('create')),
	array('label'=>'Update Tourrate', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Tourrate', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Tourrate', 'url'=>array('admin')),
);
?>

<h1>View Tourrate #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'tourID',
		'fromDate',
		'toDate',
		'rate',
	),
)); ?>
