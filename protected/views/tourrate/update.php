<?php
/* @var $this TourrateController */
/* @var $model Tourrate */

$this->breadcrumbs=array(
	'Tourrates'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Tourrate', 'url'=>array('index')),
	array('label'=>'Create Tourrate', 'url'=>array('create')),
	array('label'=>'View Tourrate', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Tourrate', 'url'=>array('admin')),
);
?>

<h1>Update Tourrate <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>