<?php
/* @var $this TourrateController */
/* @var $model Tourrate */

$this->breadcrumbs=array(
	'Tourrates'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Tourrate', 'url'=>array('index')),
	array('label'=>'Manage Tourrate', 'url'=>array('admin')),
);
?>

<h1>Create Tourrate</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>