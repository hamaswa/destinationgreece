<?php
/* @var $this TourrateController */
/* @var $data Tourrate */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tourID')); ?>:</b>
	<?php echo CHtml::encode($data->tourID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fromDate')); ?>:</b>
	<?php echo CHtml::encode($data->fromDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('toDate')); ?>:</b>
	<?php echo CHtml::encode($data->toDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rate')); ?>:</b>
	<?php echo CHtml::encode($data->rate); ?>
	<br />


</div>