<?php
/* @var $this TourrateController */
/* @var $model Tourrate */
/* @var $form CActiveForm */
?>

   <div class="eleven columns" style="margin-left:20% !important;margin-right:20% !important;">
      <h3 class="form-heading">Add Agent</h3>
      <div id="response"></div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tourrate-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="five columns">
		<?php echo $form->labelEx($model,'tourID'); ?>
		<?php echo $form->textField($model,'tourID'); ?>
		<?php echo $form->error($model,'tourID'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'fromDate'); ?>
		<?php echo $form->textField($model,'fromDate'); ?>
		<?php echo $form->error($model,'fromDate'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'toDate'); ?>
		<?php echo $form->textField($model,'toDate'); ?>
		<?php echo $form->error($model,'toDate'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'rate'); ?>
		<?php echo $form->textField($model,'rate',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'rate'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->