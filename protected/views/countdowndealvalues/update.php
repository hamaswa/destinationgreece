<?php
/* @var $this CountdowndealvaluesController */
/* @var $model Countdowndealvalues */

$this->breadcrumbs=array(
	'Countdowndealvalues'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Countdowndealvalues', 'url'=>array('index')),
	array('label'=>'Create Countdowndealvalues', 'url'=>array('create')),
	array('label'=>'View Countdowndealvalues', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Countdowndealvalues', 'url'=>array('admin')),
);
?>

<h1>Update Countdowndealvalues <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>