<?php
/* @var $this CountdowndealvaluesController */
/* @var $model Countdowndealvalues */
/* @var $form CActiveForm */
?>

   <div class="eleven columns" style="margin-left:20% !important;margin-right:20% !important;">
      <h3 class="form-heading">Add Agent</h3>
      <div id="response"></div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'countdowndealvalues-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="five columns">
		<?php echo $form->labelEx($model,'countdownDealID'); ?>
		<?php echo $form->textField($model,'countdownDealID'); ?>
		<?php echo $form->error($model,'countdownDealID'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'saleNum'); ?>
		<?php echo $form->textField($model,'saleNum'); ?>
		<?php echo $form->error($model,'saleNum'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'discountValue'); ?>
		<?php echo $form->textField($model,'discountValue'); ?>
		<?php echo $form->error($model,'discountValue'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->