<?php
/* @var $this CountdowndealvaluesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Countdowndealvalues',
);

$this->menu=array(
	array('label'=>'Create Countdowndealvalues', 'url'=>array('create')),
	array('label'=>'Manage Countdowndealvalues', 'url'=>array('admin')),
);
?>

<h1>Countdowndealvalues</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
