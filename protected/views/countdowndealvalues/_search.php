<?php
/* @var $this CountdowndealvaluesController */
/* @var $model Countdowndealvalues */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID'); ?>
		<?php echo $form->textField($model,'ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'countdownDealID'); ?>
		<?php echo $form->textField($model,'countdownDealID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'saleNum'); ?>
		<?php echo $form->textField($model,'saleNum'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'discountValue'); ?>
		<?php echo $form->textField($model,'discountValue'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->