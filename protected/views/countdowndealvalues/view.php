<?php
/* @var $this CountdowndealvaluesController */
/* @var $model Countdowndealvalues */

$this->breadcrumbs=array(
	'Countdowndealvalues'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Countdowndealvalues', 'url'=>array('index')),
	array('label'=>'Create Countdowndealvalues', 'url'=>array('create')),
	array('label'=>'Update Countdowndealvalues', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Countdowndealvalues', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Countdowndealvalues', 'url'=>array('admin')),
);
?>

<h1>View Countdowndealvalues #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'countdownDealID',
		'saleNum',
		'discountValue',
	),
)); ?>
