<?php
/* @var $this CountdowndealvaluesController */
/* @var $model Countdowndealvalues */

$this->breadcrumbs=array(
	'Countdowndealvalues'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Countdowndealvalues', 'url'=>array('index')),
	array('label'=>'Manage Countdowndealvalues', 'url'=>array('admin')),
);
?>

<h1>Create Countdowndealvalues</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>