<?php
/* @var $this CountdowndealvaluesController */
/* @var $data Countdowndealvalues */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('countdownDealID')); ?>:</b>
	<?php echo CHtml::encode($data->countdownDealID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saleNum')); ?>:</b>
	<?php echo CHtml::encode($data->saleNum); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('discountValue')); ?>:</b>
	<?php echo CHtml::encode($data->discountValue); ?>
	<br />


</div>