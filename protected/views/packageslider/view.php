<?php
/* @var $this PackagesliderController */
/* @var $model Packageslider */

$this->breadcrumbs=array(
	'Packagesliders'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Packageslider', 'url'=>array('index')),
	array('label'=>'Create Packageslider', 'url'=>array('create')),
	array('label'=>'Update Packageslider', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Packageslider', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Packageslider', 'url'=>array('admin')),
);
?>

<h1>View Packageslider #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'packageID',
		'packageDescription',
		'photoFull',
		'photoDescription',
		'url',
		'order',
		'status',
		'text',
	),
)); ?>
