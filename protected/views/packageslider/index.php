<?php
/* @var $this PackagesliderController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Packagesliders',
);

$this->menu=array(
	array('label'=>'Create Packageslider', 'url'=>array('create')),
	array('label'=>'Manage Packageslider', 'url'=>array('admin')),
);
?>

<h1>Packagesliders</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
