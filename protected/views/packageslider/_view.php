<?php
/* @var $this PackagesliderController */
/* @var $data Packageslider */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('packageID')); ?>:</b>
	<?php echo CHtml::encode($data->packageID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('packageDescription')); ?>:</b>
	<?php echo CHtml::encode($data->packageDescription); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('photoFull')); ?>:</b>
	<?php echo CHtml::encode($data->photoFull); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('photoDescription')); ?>:</b>
	<?php echo CHtml::encode($data->photoDescription); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('url')); ?>:</b>
	<?php echo CHtml::encode($data->url); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('order')); ?>:</b>
	<?php echo CHtml::encode($data->order); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('text')); ?>:</b>
	<?php echo CHtml::encode($data->text); ?>
	<br />

	*/ ?>

</div>