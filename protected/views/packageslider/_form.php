<?php
/* @var $this PackagesliderController */
/* @var $model Packageslider */
/* @var $form CActiveForm */
?>

   <div class="eleven columns" style="margin-left:20% !important;margin-right:20% !important;">
      <h3 class="form-heading">Add Agent</h3>
      <div id="response"></div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'packageslider-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="five columns">
		<?php echo $form->labelEx($model,'packageID'); ?>
		<?php echo $form->textField($model,'packageID'); ?>
		<?php echo $form->error($model,'packageID'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'packageDescription'); ?>
		<?php echo $form->textField($model,'packageDescription'); ?>
		<?php echo $form->error($model,'packageDescription'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'photoFull'); ?>
		<?php echo $form->textField($model,'photoFull',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'photoFull'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'photoDescription'); ?>
		<?php echo $form->textField($model,'photoDescription',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'photoDescription'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'url'); ?>
		<?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'url'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'order'); ?>
		<?php echo $form->textField($model,'order'); ?>
		<?php echo $form->error($model,'order'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'text'); ?>
		<?php echo $form->textField($model,'text',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'text'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->