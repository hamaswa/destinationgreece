<?php
/* @var $this PackagesliderController */
/* @var $model Packageslider */

$this->breadcrumbs=array(
	'Packagesliders'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Packageslider', 'url'=>array('index')),
	array('label'=>'Create Packageslider', 'url'=>array('create')),
	array('label'=>'View Packageslider', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Packageslider', 'url'=>array('admin')),
);
?>

<h1>Update Packageslider <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>