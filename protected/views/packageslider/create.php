<?php
/* @var $this PackagesliderController */
/* @var $model Packageslider */

$this->breadcrumbs=array(
	'Packagesliders'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Packageslider', 'url'=>array('index')),
	array('label'=>'Manage Packageslider', 'url'=>array('admin')),
);
?>

<h1>Create Packageslider</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>