<?php
/* @var $this RoomtypeController */
/* @var $model Roomtype */

$this->breadcrumbs=array(
	'Roomtypes'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Roomtype', 'url'=>array('index')),
	array('label'=>'Create Roomtype', 'url'=>array('create')),
	array('label'=>'View Roomtype', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Roomtype', 'url'=>array('admin')),
);
?>

<h1>Update Roomtype <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>