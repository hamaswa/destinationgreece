<?php
/* @var $this GreecepackagecruiseController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Greecepackagecruises',
);

$this->menu=array(
	array('label'=>'Create Greecepackagecruise', 'url'=>array('create')),
	array('label'=>'Manage Greecepackagecruise', 'url'=>array('admin')),
);
?>

<h1>Greecepackagecruises</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
