<?php
/* @var $this GreecepackagecruiseController */
/* @var $model Greecepackagecruise */

$this->breadcrumbs=array(
	'Greecepackagecruises'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Greecepackagecruise', 'url'=>array('index')),
	array('label'=>'Create Greecepackagecruise', 'url'=>array('create')),
	array('label'=>'View Greecepackagecruise', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Greecepackagecruise', 'url'=>array('admin')),
);
?>

<h1>Update Greecepackagecruise <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>