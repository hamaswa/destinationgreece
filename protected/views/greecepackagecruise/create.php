<?php
/* @var $this GreecepackagecruiseController */
/* @var $model Greecepackagecruise */

$this->breadcrumbs=array(
	'Greecepackagecruises'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Greecepackagecruise', 'url'=>array('index')),
	array('label'=>'Manage Greecepackagecruise', 'url'=>array('admin')),
);
?>

<h1>Create Greecepackagecruise</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>