<?php
/* @var $this GreecepackagecruiseController */
/* @var $model Greecepackagecruise */

$this->breadcrumbs=array(
	'Greecepackagecruises'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Greecepackagecruise', 'url'=>array('index')),
	array('label'=>'Create Greecepackagecruise', 'url'=>array('create')),
	array('label'=>'Update Greecepackagecruise', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Greecepackagecruise', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Greecepackagecruise', 'url'=>array('admin')),
);
?>

<h1>View Greecepackagecruise #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'gpid',
		'cruise',
		'numOverNights',
	),
)); ?>
