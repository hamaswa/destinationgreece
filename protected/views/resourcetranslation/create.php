<?php
/* @var $this ResourcetranslationController */
/* @var $model Resourcetranslation */

$this->breadcrumbs=array(
	'Resourcetranslations'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Resourcetranslation', 'url'=>array('index')),
	array('label'=>'Manage Resourcetranslation', 'url'=>array('admin')),
);
?>

<h1>Create Resourcetranslation</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>