<?php
/* @var $this ResourcetranslationController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Resourcetranslations',
);

$this->menu=array(
	array('label'=>'Create Resourcetranslation', 'url'=>array('create')),
	array('label'=>'Manage Resourcetranslation', 'url'=>array('admin')),
);
?>

<h1>Resourcetranslations</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
