<?php
/* @var $this ResourcetranslationController */
/* @var $model Resourcetranslation */
/* @var $form CActiveForm */
?>

   <div class="eleven columns" style="margin-left:20% !important;margin-right:20% !important;">
      <h3 class="form-heading">Add Agent</h3>
      <div id="response"></div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'resourcetranslation-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="five columns">
		<?php echo $form->labelEx($model,'resourceID'); ?>
		<?php echo $form->textField($model,'resourceID',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'resourceID'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'language'); ?>
		<?php echo $form->textField($model,'language'); ?>
		<?php echo $form->error($model,'language'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'translation'); ?>
		<?php echo $form->textField($model,'translation',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'translation'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->