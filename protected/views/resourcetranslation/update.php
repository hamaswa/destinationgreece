<?php
/* @var $this ResourcetranslationController */
/* @var $model Resourcetranslation */

$this->breadcrumbs=array(
	'Resourcetranslations'=>array('index'),
	$model->resourceID=>array('view','id'=>$model->resourceID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Resourcetranslation', 'url'=>array('index')),
	array('label'=>'Create Resourcetranslation', 'url'=>array('create')),
	array('label'=>'View Resourcetranslation', 'url'=>array('view', 'id'=>$model->resourceID)),
	array('label'=>'Manage Resourcetranslation', 'url'=>array('admin')),
);
?>

<h1>Update Resourcetranslation <?php echo $model->resourceID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>