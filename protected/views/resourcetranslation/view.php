<?php
/* @var $this ResourcetranslationController */
/* @var $model Resourcetranslation */

$this->breadcrumbs=array(
	'Resourcetranslations'=>array('index'),
	$model->resourceID,
);

$this->menu=array(
	array('label'=>'List Resourcetranslation', 'url'=>array('index')),
	array('label'=>'Create Resourcetranslation', 'url'=>array('create')),
	array('label'=>'Update Resourcetranslation', 'url'=>array('update', 'id'=>$model->resourceID)),
	array('label'=>'Delete Resourcetranslation', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->resourceID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Resourcetranslation', 'url'=>array('admin')),
);
?>

<h1>View Resourcetranslation #<?php echo $model->resourceID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'resourceID',
		'language',
		'translation',
		'status',
	),
)); ?>
