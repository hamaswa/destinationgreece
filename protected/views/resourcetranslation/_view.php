<?php
/* @var $this ResourcetranslationController */
/* @var $data Resourcetranslation */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('resourceID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->resourceID), array('view', 'id'=>$data->resourceID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('language')); ?>:</b>
	<?php echo CHtml::encode($data->language); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('translation')); ?>:</b>
	<?php echo CHtml::encode($data->translation); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />


</div>