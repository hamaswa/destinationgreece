<?php
/* @var $this CabintypeController */
/* @var $model Cabintype */

$this->breadcrumbs=array(
	'Cabintypes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Cabintype', 'url'=>array('index')),
	array('label'=>'Manage Cabintype', 'url'=>array('admin')),
);
?>

<h1>Create Cabintype</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>