<?php
/* @var $this CabintypeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cabintypes',
);

$this->menu=array(
	array('label'=>'Create Cabintype', 'url'=>array('create')),
	array('label'=>'Manage Cabintype', 'url'=>array('admin')),
);
?>

<h1>Cabintypes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
