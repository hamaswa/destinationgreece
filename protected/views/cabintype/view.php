<?php
/* @var $this CabintypeController */
/* @var $model Cabintype */

$this->breadcrumbs=array(
	'Cabintypes'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Cabintype', 'url'=>array('index')),
	array('label'=>'Create Cabintype', 'url'=>array('create')),
	array('label'=>'Update Cabintype', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Cabintype', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Cabintype', 'url'=>array('admin')),
);
?>

<h1>View Cabintype #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'name',
		'maxAdults',
	),
)); ?>
