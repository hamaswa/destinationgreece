<?php
/* @var $this CabintypeController */
/* @var $model Cabintype */

$this->breadcrumbs=array(
	'Cabintypes'=>array('index'),
	$model->name=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Cabintype', 'url'=>array('index')),
	array('label'=>'Create Cabintype', 'url'=>array('create')),
	array('label'=>'View Cabintype', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Cabintype', 'url'=>array('admin')),
);
?>

<h1>Update Cabintype <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>