<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Create User', 'url'=>array('create')),
	array('label'=>'Update User', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage User', 'url'=>array('admin')),
);
?>

<h1>View User #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'firstName',
		'lastName',
		'secretQuestion',
		'secretAnswer',
		'email1',
		'email2',
		'phone1',
		'phone2',
		'cell1',
		'cell2',
		'fax',
		'address',
		'city',
		'zip',
		'state',
		'country',
		'notes',
		'areacode1',
		'areacode2',
		'username',
		'password',
		'changePass',
		'dateCreated',
	),
)); ?>
