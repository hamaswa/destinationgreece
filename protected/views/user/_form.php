<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

   <div class="eleven columns" style="margin-left:20% !important;margin-right:20% !important;">
      <h3 class="form-heading">Add Agent</h3>
      <div id="response"></div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="five columns">
		<?php echo $form->labelEx($model,'firstName'); ?>
		<?php echo $form->textField($model,'firstName',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'firstName'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'lastName'); ?>
		<?php echo $form->textField($model,'lastName',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'lastName'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'secretQuestion'); ?>
		<?php echo $form->textField($model,'secretQuestion',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'secretQuestion'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'secretAnswer'); ?>
		<?php echo $form->textField($model,'secretAnswer',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'secretAnswer'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'email1'); ?>
		<?php echo $form->textField($model,'email1',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'email1'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'email2'); ?>
		<?php echo $form->textField($model,'email2',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'email2'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'phone1'); ?>
		<?php echo $form->textField($model,'phone1',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'phone1'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'phone2'); ?>
		<?php echo $form->textField($model,'phone2',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'phone2'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'cell1'); ?>
		<?php echo $form->textField($model,'cell1',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'cell1'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'cell2'); ?>
		<?php echo $form->textField($model,'cell2',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'cell2'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'fax'); ?>
		<?php echo $form->textField($model,'fax',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'fax'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'address'); ?>
		<?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'address'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'city'); ?>
		<?php echo $form->textField($model,'city',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'city'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'zip'); ?>
		<?php echo $form->textField($model,'zip',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'zip'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'state'); ?>
		<?php echo $form->textField($model,'state',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'state'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'country'); ?>
		<?php echo $form->textField($model,'country',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'country'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'notes'); ?>
		<?php echo $form->textField($model,'notes',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'notes'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'areacode1'); ?>
		<?php echo $form->textField($model,'areacode1',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'areacode1'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'areacode2'); ?>
		<?php echo $form->textField($model,'areacode2',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'areacode2'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'changePass'); ?>
		<?php echo $form->textField($model,'changePass'); ?>
		<?php echo $form->error($model,'changePass'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'dateCreated'); ?>
		<?php echo $form->textField($model,'dateCreated'); ?>
		<?php echo $form->error($model,'dateCreated'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->