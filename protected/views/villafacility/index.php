<?php
/* @var $this VillafacilityController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Villafacilities',
);

$this->menu=array(
	array('label'=>'Create Villafacility', 'url'=>array('create')),
	array('label'=>'Manage Villafacility', 'url'=>array('admin')),
);
?>

<h1>Villafacilities</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
