<?php
/* @var $this VillafacilityController */
/* @var $model Villafacility */

$this->breadcrumbs=array(
	'Villafacilities'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Villafacility', 'url'=>array('index')),
	array('label'=>'Create Villafacility', 'url'=>array('create')),
	array('label'=>'View Villafacility', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Villafacility', 'url'=>array('admin')),
);
?>

<h1>Update Villafacility <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>