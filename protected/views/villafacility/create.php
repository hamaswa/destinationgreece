<?php
/* @var $this VillafacilityController */
/* @var $model Villafacility */

$this->breadcrumbs=array(
	'Villafacilities'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Villafacility', 'url'=>array('index')),
	array('label'=>'Manage Villafacility', 'url'=>array('admin')),
);
?>

<h1>Create Villafacility</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>