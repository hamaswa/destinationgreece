<?php
/* @var $this VillafacilityController */
/* @var $model Villafacility */

$this->breadcrumbs=array(
	'Villafacilities'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Villafacility', 'url'=>array('index')),
	array('label'=>'Create Villafacility', 'url'=>array('create')),
	array('label'=>'Update Villafacility', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Villafacility', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Villafacility', 'url'=>array('admin')),
);
?>

<h1>View Villafacility #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'villaID',
	),
)); ?>
