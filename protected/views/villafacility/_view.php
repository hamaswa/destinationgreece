<?php
/* @var $this VillafacilityController */
/* @var $data Villafacility */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('villaID')); ?>:</b>
	<?php echo CHtml::encode($data->villaID); ?>
	<br />


</div>