<?php
/* @var $this GreecepackageovernightController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Greecepackageovernights',
);

$this->menu=array(
	array('label'=>'Create Greecepackageovernight', 'url'=>array('create')),
	array('label'=>'Manage Greecepackageovernight', 'url'=>array('admin')),
);
?>

<h1>Greecepackageovernights</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
