<?php
/* @var $this GreecepackageovernightController */
/* @var $model Greecepackageovernight */
/* @var $form CActiveForm */
?>

   <div class="eleven columns" style="margin-left:20% !important;margin-right:20% !important;">
      <h3 class="form-heading">Add Agent</h3>
      <div id="response"></div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'greecepackageovernight-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="five columns">
		<?php echo $form->labelEx($model,'gpid'); ?>
		<?php echo $form->textField($model,'gpid'); ?>
		<?php echo $form->error($model,'gpid'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'city'); ?>
		<?php echo $form->textField($model,'city'); ?>
		<?php echo $form->error($model,'city'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'numOverNights'); ?>
		<?php echo $form->textField($model,'numOverNights'); ?>
		<?php echo $form->error($model,'numOverNights'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->