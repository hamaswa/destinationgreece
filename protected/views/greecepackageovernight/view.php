<?php
/* @var $this GreecepackageovernightController */
/* @var $model Greecepackageovernight */

$this->breadcrumbs=array(
	'Greecepackageovernights'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Greecepackageovernight', 'url'=>array('index')),
	array('label'=>'Create Greecepackageovernight', 'url'=>array('create')),
	array('label'=>'Update Greecepackageovernight', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Greecepackageovernight', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Greecepackageovernight', 'url'=>array('admin')),
);
?>

<h1>View Greecepackageovernight #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'gpid',
		'city',
		'numOverNights',
	),
)); ?>
