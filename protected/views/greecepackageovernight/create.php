<?php
/* @var $this GreecepackageovernightController */
/* @var $model Greecepackageovernight */

$this->breadcrumbs=array(
	'Greecepackageovernights'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Greecepackageovernight', 'url'=>array('index')),
	array('label'=>'Manage Greecepackageovernight', 'url'=>array('admin')),
);
?>

<h1>Create Greecepackageovernight</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>