<?php
/* @var $this GreecepackageovernightController */
/* @var $model Greecepackageovernight */

$this->breadcrumbs=array(
	'Greecepackageovernights'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Greecepackageovernight', 'url'=>array('index')),
	array('label'=>'Create Greecepackageovernight', 'url'=>array('create')),
	array('label'=>'View Greecepackageovernight', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Greecepackageovernight', 'url'=>array('admin')),
);
?>

<h1>Update Greecepackageovernight <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>