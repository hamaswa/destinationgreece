<?php
/* @var $this GreecepackageairController */
/* @var $data Greecepackageair */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gpid')); ?>:</b>
	<?php echo CHtml::encode($data->gpid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('air')); ?>:</b>
	<?php echo CHtml::encode($data->air); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numOverNights')); ?>:</b>
	<?php echo CHtml::encode($data->numOverNights); ?>
	<br />


</div>