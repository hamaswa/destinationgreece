<?php
/* @var $this GreecepackageairController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Greecepackageairs',
);

$this->menu=array(
	array('label'=>'Create Greecepackageair', 'url'=>array('create')),
	array('label'=>'Manage Greecepackageair', 'url'=>array('admin')),
);
?>

<h1>Greecepackageairs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
