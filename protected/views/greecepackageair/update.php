<?php
/* @var $this GreecepackageairController */
/* @var $model Greecepackageair */

$this->breadcrumbs=array(
	'Greecepackageairs'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Greecepackageair', 'url'=>array('index')),
	array('label'=>'Create Greecepackageair', 'url'=>array('create')),
	array('label'=>'View Greecepackageair', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Greecepackageair', 'url'=>array('admin')),
);
?>

<h1>Update Greecepackageair <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>