<?php
/* @var $this GreecepackageairController */
/* @var $model Greecepackageair */

$this->breadcrumbs=array(
	'Greecepackageairs'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Greecepackageair', 'url'=>array('index')),
	array('label'=>'Create Greecepackageair', 'url'=>array('create')),
	array('label'=>'Update Greecepackageair', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Greecepackageair', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Greecepackageair', 'url'=>array('admin')),
);
?>

<h1>View Greecepackageair #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'gpid',
		'air',
		'numOverNights',
	),
)); ?>
