<?php
/* @var $this GreecepackageairController */
/* @var $model Greecepackageair */

$this->breadcrumbs=array(
	'Greecepackageairs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Greecepackageair', 'url'=>array('index')),
	array('label'=>'Manage Greecepackageair', 'url'=>array('admin')),
);
?>

<h1>Create Greecepackageair</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>