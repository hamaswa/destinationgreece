<?php
/* @var $this ClientController */
/* @var $model Client */
/* @var $form CActiveForm */
?>
<script type="text/javascript">
    /* <![CDATA[ */
    jQuery(function(){				
        jQuery("#Client_firstName").validate({
            expression:  "if (VAL.match(/^[a-zA-Z]+([a-zA-Z]?[\-' ]?[a-zA-Z]+)*[a-zA-Z]+$/)) return true; else return false;",
            message: "Invalid characters"
        });
        jQuery("#Client_lastName").validate({
            expression:  "if (VAL.match(/^[a-zA-Z]+([a-zA-Z]?[\-' ]?[a-zA-Z]+)*[a-zA-Z]+$/)) return true; else return false;",
            message: "Invalid characters"
        });
        jQuery("#agent").validate({
            expression:  "if (VAL.match(/^[a-zA-Z]+([a-zA-Z]?(( \- )|([\-' ]?))?[a-zA-Z]+)*[a-zA-Z]+$/)) return true; else return false;",
            message: "Invalid characters"
        });
        jQuery("#Client_arrivalDate").validate({
            expression: "if (!isValidDate(parseInt(VAL.split('-')[0]), parseInt(VAL.split('-')[1]), parseInt(VAL.split('-')[2]))) return false; else return true;",
            message: "Please enter a valid Date"
        });
        jQuery("#Client_departureDate").validate({
            expression: "if (!isValidDate(parseInt(VAL.split('-')[0]), parseInt(VAL.split('-')[1]), parseInt(VAL.split('-')[2]))) return false; else return true;",
            message: "Please enter a valid Date"
        });
        jQuery("#Client_sellingPrice").validate({
            expression: "if (VAL.match(/^[0-9]*(,|.)?[0-9]+$/)) return true; else return false;",
            message: "Invalid Number"
        });			
        jQuery("#paxNum").validate({
            expression: "if (VAL.match(/^[1-9]+[0-9]*$/)) return true; else return false;",
            message: "Invalid Number"
        });			
    });
    /* ]]> */
  
</script>
<div class="ten columns">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'client-form',
        'enableAjaxValidation' => true,
            ));
    ?>
    <div style="width: 220px;">
          <!--div id="response"><?php echo $form->errorSummary($model); ?></div-->


        <div class="four columns" style="width: 220px; padding-top: 0px;">
            <?php echo $form->textField($model, 'firstName', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'First Name')); ?>
            <?php echo $form->error($model, 'firstName'); ?>
        </div>

        <div class="four columns" style="width: 220px; padding-top: 0px;">
            <?php echo $form->textField($model, 'lastName', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Last Name')); ?>
            <?php echo $form->error($model, 'lastName'); ?>
        </div>

        <div class="four columns" style="width: 220px; padding-top: 0px;">
            <?php echo $form->textField($model, 'paxNum', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Passengers')); ?>
            <?php echo $form->error($model, 'paxNum'); ?>
        </div>

        <div class="four columns" style="width: 220px; padding-top: 0px;">
            <?php echo $form->textField($model, 'arrivalDate', array('placeholder' => 'Arrival Date')); ?>
            <?php echo $form->error($model, 'arrivalDate'); ?>
        </div>

        <div class="four columns" style="width: 220px; padding-top: 0px;">
            <?php echo $form->textField($model, 'departureDate', array('placeholder' => 'Departure Date')); ?>
            <?php echo $form->error($model, 'departureDate'); ?>
        </div>

        <div class="four columns" style="width: 220px; padding-top: 0px;">
            <?php echo $form->textField($model, 'sellingPrice', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Selling Price')); ?>
            <?php echo $form->error($model, 'sellingPrice'); ?>
        </div>

        <div class="four columns" style="width: 220px; padding-top: 0px;">
            <?php
            $sql = 'SELECT agentID,  agentName FROM agent order by agentName';
            $cmd = Yii::app()->db->createCommand($sql);
            $res = $cmd->queryAll();
            $arr[0]='Agent Name';
            foreach ($res as $agent) {
                $arr[$agent['agentID']] = $agent['agentName'];
            }
            echo $form->dropDownList($model, 'agent',$arr, array('id' => 'aID'));
            ?>
            <?php echo $form->error($model, 'agent'); ?>
            <?php //echo $form->hiddenField($model, 'agent', array('id' => 'aID')); ?>
            <?php //echo $form->error($model, 'agent'); ?>
        </div>

        <div class="four columns" style="width: 220px; padding-top: 0px;">
            <?php echo $form->textArea($model, 'notes', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Notes')); ?>
            <div id="counter" style="min-width:200px;text-align:right;"></div>
        </div>


        <div style="float: right;margin-right: -10px;" >
            <input type="reset" value="Clear" style="margin-right:10px;" id="clear" /><?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update'); ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<!-- form -->

<script>
    $().ready(function(){		 		
        $( "#Client_arrivalDate" ).datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(dateText, inst){
                $("#Client_departureDate").datepicker("option","minDate",
                $("#Client_arrivalDate").datepicker("getDate"));
            }
        }).datepicker('setDate', '0');
        $( "#Client_departureDate" ).datepicker({
            dateFormat: 'yy-mm-dd'
        }).datepicker('setDate', '0');
        var characters = 2000;
        $("#counter").append("You have <strong>"+  characters+"</strong> characters remaining");
        $("#Client_notes").keyup(function(){
            if($(this).val().length > characters){
                $(this).val($(this).val().substr(0, characters));
            }
            var remaining = characters -  $(this).val().length;
            $("#counter").html("You have <strong>"+  remaining+"</strong> characters remaining");
            if(remaining <= 10)
            {
                $("#counter").css("color","red");
            }
            else
            {
                $("#counter").css("color","black");
            }
        });
        $("#clear").click(function(){
            $("input:text").next(".ValidationErrors").remove();
            $("input:text").removeClass("ErrorField");
        });
    });
</script>