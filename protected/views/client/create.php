<?php
/* @var $this ClientController */
/* @var $model Client */

$this->breadcrumbs=array(
	'Clients'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Client', 'url'=>array('index')),
	array('label'=>'Manage Client', 'url'=>array('admin')),
);
?>

<div style="margin-left:20px;margin-bottom:20px;">
<h3>Add Client</h3></div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>