<?php
/* @var $this ClientController */
/* @var $data Client */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />


	<b><?php echo CHtml::encode($data->getAttributeLabel('firstName')); ?>:</b>
	<?php echo CHtml::encode($data->firstName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastName')); ?>:</b>
	<?php echo CHtml::encode($data->lastName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paxNum')); ?>:</b>
	<?php echo CHtml::encode($data->paxNum); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('arrivalDate')); ?>:</b>
	<?php echo CHtml::encode($data->arrivalDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('departureDate')); ?>:</b>
	<?php echo CHtml::encode($data->departureDate); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('sellingPrice')); ?>:</b>
	<?php echo CHtml::encode($data->sellingPrice); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agent')); ?>:</b>
	<?php echo CHtml::encode($data->agent); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('notes')); ?>:</b>
	<?php echo CHtml::encode($data->notes); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('editedBy')); ?>:</b>
	<?php echo CHtml::encode($data->editedBy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deleted')); ?>:</b>
	<?php echo CHtml::encode($data->deleted); ?>
	<br />

	*/ ?>

</div>