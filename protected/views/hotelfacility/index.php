<?php
/* @var $this HotelfacilityController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Hotelfacilities',
);

$this->menu=array(
	array('label'=>'Create Hotelfacility', 'url'=>array('create')),
	array('label'=>'Manage Hotelfacility', 'url'=>array('admin')),
);
?>

<h1>Hotelfacilities</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
