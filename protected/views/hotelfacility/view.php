<?php
/* @var $this HotelfacilityController */
/* @var $model Hotelfacility */

$this->breadcrumbs=array(
	'Hotelfacilities'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Hotelfacility', 'url'=>array('index')),
	array('label'=>'Create Hotelfacility', 'url'=>array('create')),
	array('label'=>'Update Hotelfacility', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Hotelfacility', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Hotelfacility', 'url'=>array('admin')),
);
?>

<h1>View Hotelfacility #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'hotelID',
	),
)); ?>
