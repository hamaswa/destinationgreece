<?php
/* @var $this HotelfacilityController */
/* @var $model Hotelfacility */

$this->breadcrumbs=array(
	'Hotelfacilities'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Hotelfacility', 'url'=>array('index')),
	array('label'=>'Create Hotelfacility', 'url'=>array('create')),
	array('label'=>'View Hotelfacility', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Hotelfacility', 'url'=>array('admin')),
);
?>

<h1>Update Hotelfacility <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>