<?php
/* @var $this HotelfacilityController */
/* @var $model Hotelfacility */

$this->breadcrumbs=array(
	'Hotelfacilities'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Hotelfacility', 'url'=>array('index')),
	array('label'=>'Manage Hotelfacility', 'url'=>array('admin')),
);
?>

<h1>Create Hotelfacility</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>