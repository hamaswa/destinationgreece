<?php
/* @var $this HotelfacilityController */
/* @var $data Hotelfacility */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hotelID')); ?>:</b>
	<?php echo CHtml::encode($data->hotelID); ?>
	<br />


</div>