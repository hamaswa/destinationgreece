<h1> Welcome <?php echo Yii::app()->user->name; ?></h1>


<script>
    $().ready(function(){
        $("select").change(function(){
            
            URL=$(this).val();
            window.location=URL;
            return;
            var urlregex = new RegExp("^(http:\/\/www.|https:\/\/www.|ftp:\/\/www.|www.){1}([0-9A-Za-z]+\.)");
            if (urlregex.test(URL)) {
                window.location=URL;
            }
            return;
            
            
        });
        
    });
</script>


<div id="callout">
    <div class="container">
        <div class="three_columns columns right">
            <select class="navigate" name="">
                <option>Client Control</option>
                <option value="<?php echo $this->createUrl('/quote/create'); ?>">Create a new quote</option>
                <option value="<?php echo $this->createUrl('/quote/admin'); ?>">Edit/Delete existing quote</option>
                <option value="">Generate a master voucher</option>
                <option value="<?php echo $this->createUrl('/client/create'); ?>">Add a Client</option>
                <option value="<?php echo $this->createUrl('/client/admin'); ?>">Edit/Delete Client</option>
                <option value="">Check hotel rates</option>
            </select> 
        </div>
        <div class="three_columns columns right">
            <select name="">
                <option>Agent Control</option>
                <option value="<?php echo $this->createUrl('/agent/create'); ?>">Add a travel agent</option>
                <option value="<?php echo $this->createUrl('/agent/admin'); ?>">Edit/Delete a travel agent</option>
                <option value="">Pending registration</option>
                <option value="">Agent statistics</option>
            </select> 
        </div>
        <div class="three_columns columns right">
            <select name="">
                <option>Provider Control</option>
                <option value="<?php echo $this->createUrl('/provider/create'); ?>">Add a provider</option>
                <option value="<?php echo $this->createUrl('/provider/admin'); ?>">Edit/Delete a provider</option>
            </select> 
        </div>
        <div class="three_columns columns right">
            <select name="">
                <option>Reports Control</option>
                <option value="">Services/Costs/Provider/Client</option>
                <option value="">Financial figures/Agents</option>
                <option value="">Financial figures/Provider</option>
                <option value="">Clients/Agent</option>
                <option value="">Interactive query</option>
                <option value="">Data export</option>
            </select> 
        </div>
        <div class="clearfix"></div>
		<div class="three_columns columns right">
            <select name="">
                <option>Information Control</option>
                <option value="<?php echo $this->createUrl('/testimonials/create'); ?>">Add a testimonial</option>
                <option value="<?php echo $this->createUrl('/testimonials/admin'); ?>">Edit/Delete a testimonial</option>
                <option value="">Add a location</option>
                <option value="">Edit/Delete a location</option>
                <option value="">Add a RSS news article</option>
                <option value="">Edit/Delete a RSS article</option>
                <option value="">Add a RSS offer</option>
                <option value="">Edit/Delete a RSS offer</option>
                <option value="">Add a poll</option>
                <option value="">Edit/Delete a Poll</option>
            </select> 
        </div>
        <div class="three_columns columns right">
            <select name="">
                <option>Admin Control</option>
                <option value="">Edit page METAs</option>
                <option value="">Edit package slider</option>
                <option value="">Page Widgets</option>
                <option value="">Add a language version</option>
                <option value="">Edit/Delete a lang version</option>
                <option value="">Country secondary tables</option>
                <option value="">Add a user</option>
                <option value="">Edit/Delete a user</option>
                <option value="">Export data</option>
                <option value="">Control look and feel</option>
            </select> 
        </div>
        
        <div class="three_columns columns right">
            <select name="">
                <option>Accommodation  Control</option>
                <option value="<?php echo $this->createUrl('/hotel/create'); ?>">Add a hotel</option>
                <option value="<?php echo $this->createUrl('/hotel/admin'); ?>">Edit/Delete a hotel</option>
                <option value="<?php echo $this->createUrl('/villa/create'); ?>">Add a villa</option>
                <option value="<?php echo $this->createUrl('/villa/admin'); ?>">Edit/Delete a villa</option>
            </select> 
        </div>
        <div class="three_columns columns right">
            <select name="">
                <option>Transportation Control</option>
                <option value="<?php echo $this->createUrl('/transfer/create'); ?>">Add a transfer</option>
                <option value="<?php echo $this->createUrl('/transfer/admin'); ?>">Edit/Delete a transfer</option>
                <option value="<?php echo $this->createUrl('/ferry/create'); ?>">Add a ferry</option>
                <option value="<?php echo $this->createUrl('/ferry/admin'); ?>">Edit/Delete a ferry</option>
                <option value="<?php echo $this->createUrl('/hydro/create'); ?>">Add a hydrofoil</option>
                <option value="<?php echo $this->createUrl('/hydro/admin'); ?>">Edit/Delete a hydrofoil</option>
                <option value="<?php echo $this->createUrl('/air/create'); ?>">Add an air ticket</option>
                <option value="<?php echo $this->createUrl('/air/admin'); ?>">Edit/Delete a air ticket</option>
                <option value="<?php echo $this->createUrl('/car/create'); ?>">Add a car rental</option>
                <option value="<?php echo $this->createUrl('/car/admin'); ?>">Edit/Delete car rental</option>
            </select> 
        </div>
        <div class="three_columns columns right">
            <select name="">
                <option>Cruise/Yachts Control</option>
                <option value="<?php echo $this->createUrl('/cruise/create'); ?>">Add a cruise</option>
                <option value="<?php echo $this->createUrl('/cruise/admin'); ?>">Edit/Delete a cruise</option>
                <option value="">Cruise market XML ctrl</option>
                <option value="">Yacht market XML ctrl</option>
                <option value="">IFRAME</option>
            </select> 
        </div>
		<div class="clearfix"></div>
        <div class="three_columns columns right">
            <select name="">
                <option>Tours/Shorex Control</option>
                <option value="<?php echo $this->createUrl('/tour/create'); ?> ">Add a tour</option>
                <option value="<?php echo $this->createUrl('/tour/admin'); ?>">Edit/Delete a tour</option>
                <option value="<?php echo $this->createUrl('/shorex/create'); ?>">Add a shorex</option>
                <option value="<?php echo $this->createUrl('/shorex/admin'); ?>">Edit/Delete a shorex</option>
            </select> 
        </div>
        
        <div class="three_columns columns right">
            <select name="">
                <option>Packages Control</option>
                <option value="<?php echo $this->createUrl('/greecepackage/create'); ?>">Add a Greece package</option>
                <option value="<?php echo $this->createUrl('/greecepackage/admin'); ?>">Edit/Delete a Greece package</option>
                <option value="">Manage Special Offer</option>
                <option value="<?php echo $this->createUrl('/nongreecepackage/create'); ?>">Add a non-Greece package</option>
                <option value="<?php echo $this->createUrl('/nongreecepackage/admin'); ?>">Edit/Delete a non-Greece package</option>
                <option value="<?php echo $this->createUrl('/combopackage/create'); ?>">Add a combo package</option>
                <option value="<?php echo $this->createUrl('/combopackage/admin'); ?>">Edit/Delete a combo package</option>
                <option value="<?php echo $this->createUrl('/ceremony/create'); ?>">Add ceremony</option>
                <option value="<?php echo $this->createUrl('/ceremony/admin'); ?>">Edit/Delete a ceremony</option>
                <option value="">Ceremony XML control</option>
            </select> 
        </div>
        <div class="three_columns columns right">
            <select name="">
                <option>Marketing Control</option>
                <option value="">Travel angents email promos</option>
                <option value="<?php echo $this->createUrl('/brochure/admin'); ?>">Manage brochures</option>
                <option value="">Manage linked sites</option>
            </select> 
        </div>
    </div>
</div>



