<?php
/* @var $this TransferrateController */
/* @var $model Transferrate */

$this->breadcrumbs=array(
	'Transferrates'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Transferrate', 'url'=>array('index')),
	array('label'=>'Create Transferrate', 'url'=>array('create')),
	array('label'=>'View Transferrate', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Transferrate', 'url'=>array('admin')),
);
?>

<h1>Update Transferrate <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>