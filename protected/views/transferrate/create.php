<?php
/* @var $this TransferrateController */
/* @var $model Transferrate */

$this->breadcrumbs=array(
	'Transferrates'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Transferrate', 'url'=>array('index')),
	array('label'=>'Manage Transferrate', 'url'=>array('admin')),
);
?>

<h1>Create Transferrate</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>