<?php
/* @var $this TransferrateController */
/* @var $model Transferrate */
/* @var $form CActiveForm */
?>

   <div class="eleven columns" style="margin-left:20% !important;margin-right:20% !important;">
      <h3 class="form-heading">Add Agent</h3>
      <div id="response"></div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'transferrate-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="five columns">
		<?php echo $form->labelEx($model,'transferID'); ?>
		<?php echo $form->textField($model,'transferID'); ?>
		<?php echo $form->error($model,'transferID'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'peopleNum'); ?>
		<?php echo $form->textField($model,'peopleNum'); ?>
		<?php echo $form->error($model,'peopleNum'); ?>
	</div>

	<div class="five columns">
		<?php echo $form->labelEx($model,'rate'); ?>
		<?php echo $form->textField($model,'rate',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'rate'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->