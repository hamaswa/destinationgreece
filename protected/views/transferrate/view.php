<?php
/* @var $this TransferrateController */
/* @var $model Transferrate */

$this->breadcrumbs=array(
	'Transferrates'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Transferrate', 'url'=>array('index')),
	array('label'=>'Create Transferrate', 'url'=>array('create')),
	array('label'=>'Update Transferrate', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Transferrate', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Transferrate', 'url'=>array('admin')),
);
?>

<h1>View Transferrate #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'transferID',
		'peopleNum',
		'rate',
	),
)); ?>
