<?php
/* @var $this TransferrateController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Transferrates',
);

$this->menu=array(
	array('label'=>'Create Transferrate', 'url'=>array('create')),
	array('label'=>'Manage Transferrate', 'url'=>array('admin')),
);
?>

<h1>Transferrates</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
