<?php
/* @var $this TransferrateController */
/* @var $data Transferrate */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('transferID')); ?>:</b>
	<?php echo CHtml::encode($data->transferID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('peopleNum')); ?>:</b>
	<?php echo CHtml::encode($data->peopleNum); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rate')); ?>:</b>
	<?php echo CHtml::encode($data->rate); ?>
	<br />


</div>