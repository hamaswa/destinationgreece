<?php
/* @var $this HotelamenityController */
/* @var $model Hotelamenity */

$this->breadcrumbs=array(
	'Hotelamenities'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Hotelamenity', 'url'=>array('index')),
	array('label'=>'Manage Hotelamenity', 'url'=>array('admin')),
);
?>

<h1>Create Hotelamenity</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>