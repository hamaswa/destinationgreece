<?php
/* @var $this HotelamenityController */
/* @var $model Hotelamenity */

$this->breadcrumbs=array(
	'Hotelamenities'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Hotelamenity', 'url'=>array('index')),
	array('label'=>'Create Hotelamenity', 'url'=>array('create')),
	array('label'=>'View Hotelamenity', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Hotelamenity', 'url'=>array('admin')),
);
?>

<h1>Update Hotelamenity <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>