<?php
/* @var $this HotelamenityController */
/* @var $model Hotelamenity */

$this->breadcrumbs=array(
	'Hotelamenities'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Hotelamenity', 'url'=>array('index')),
	array('label'=>'Create Hotelamenity', 'url'=>array('create')),
	array('label'=>'Update Hotelamenity', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Hotelamenity', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Hotelamenity', 'url'=>array('admin')),
);
?>

<h1>View Hotelamenity #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'hotelID',
	),
)); ?>
