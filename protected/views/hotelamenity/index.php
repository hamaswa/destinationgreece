<?php
/* @var $this HotelamenityController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Hotelamenities',
);

$this->menu=array(
	array('label'=>'Create Hotelamenity', 'url'=>array('create')),
	array('label'=>'Manage Hotelamenity', 'url'=>array('admin')),
);
?>

<h1>Hotelamenities</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
