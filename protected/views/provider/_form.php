<?php
/* @var $this ProviderController */
/* @var $model Provider */
/* @var $form CActiveForm */
?>

<script>
    /* <![CDATA[ */
    jQuery(function(){				
        jQuery("#providerName").validate({
            expression:  "if (VAL.match(/^[a-zA-Z]+([a-zA-Z]?[\-' ]?[a-zA-Z]+)*[a-zA-Z]+$/)) return true; else return false;",
            message: "Invalid characters"
        });
        jQuery("#agencyName").validate({
            expression:  "if (VAL.match(/^[a-zA-Z]+([a-zA-Z]?[ ]?[a-zA-Z]+)*[a-zA-Z]+$/)) return true; else return false;",
            message: "Invalid characters"
        });
        jQuery("#email1").validate({
            expression: "if (checkEmail(VAL)) return true; else return false;",
            message: "Invalid Email"
        });
        jQuery("#email2").validate({
            expression: "if (VAL.match(/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/) || ! VAL) return true; else return false;",
            message: "Invalid Email"
        });
 
        jQuery("#phone1").validate({
            expression: "if (checkInternationalPhone(VAL)) return true; else return false;",
            message: "Invalid Number"
        });
        jQuery("#phone2").validate({
            expression: "if (checkInternationalPhone(VAL) || ! VAL) return true; else return false;",
            message: "Invalid Number"
        });
        jQuery("#username").validate({
            expression: "if (VAL.match(/^[a-zA-Z0-9]{4,15}$/)) return true; else return false;",
            message: "Invalid username"
        });
        jQuery("#password").validate({
            expression: "if (VAL.match(/^[a-zA-Z0-9#\-\+@]{5,15}$/)) return true; else return false;",
            message: "Invalid Password"
        });
				
        jQuery("#address1").validate({
            expression: "if (VAL.match(/^[a-zA-Z0-9# ]+$/)) return true; else return false;",
            message: "Invalid Address"
        });
        jQuery("#zip").validate({
            expression: "if (VAL.match(/^[0-9]{5}(-\[0-9]{2,5})?$/)) return true; else return false;",
            message: "Invalid Zip"
        });
        jQuery("#city").validate({
            expression: "if (VAL.match(/^[a-zA-Z ]{3,25}$/)) return true; else return false;",
            message: "Invalid City Name"
        });

        jQuery("#country").validate({
            expression: "if (VAL.match(/^[a-zA-Z]+)) return true; else return false;",
            message: "Please select country"
        });
				
    });
    /* ]]> */
</script>


<div class="ten columns">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'provider-form',
        'enableAjaxValidation' => false,
            ));
    ?>
    <div class="agentform">
        <div class="four columns" style="width:220px;">
            <?php echo $form->textField($provider, 'providerName', array('size' => 60, 'maxlength' => 255,'id'=>'providerName', 'placeholder' => 'Provider Name * ')); ?>
            <?php echo $form->error($provider, 'providerName'); ?>
            
            
        </div>
        <div class="four columns" style="width:220px;">
           <?php echo $form->textField($provider, 'title', array('size' => 60, 'maxlength' => 255,'id'=>'title', 'placeholder' => 'Title')); ?>
            <?php echo $form->error($provider, 'title'); ?>
        </div>
        <div class="four columns" style="width:220px;">
            <?php echo $form->textField($provider, 'agency', array('size' => 60, 'maxlength' => 255,'id'=>'agencyName', 'placeholder' => 'Agency Name * ')); ?>
            <?php echo $form->error($provider, 'agency'); ?>
        </div>
        <div class="four columns" style="width:220px;">
            <?php echo $form->textField($provider, 'website', array('size' => 60, 'maxlength' => 255,'id'=>'website', 'placeholder' => 'Agency URL')); ?>
            <?php echo $form->error($provider, 'website'); ?>
        </div>
        <div class="four columns" style="width:220px;">
            <?php echo $form->textField($user, 'email1', array('size' => 60, 'maxlength' => 255,'id'=>'email1', 'placeholder' => 'Email 1 * ')); ?>
            <?php echo $form->error($user, 'email1'); ?>
            
        </div>
        <div class="four columns" style="width:220px;">
            <?php echo $form->textField($user, 'email2', array('size' => 60, 'maxlength' => 255,'id'=>'email2', 'placeholder' => 'Email 2')); ?>
            <?php echo $form->error($user, 'email2'); ?>
        </div>
        <div class="four columns" style="width:220px;">
            <?php echo $form->textField($user, 'phone1', array('size' => 60, 'maxlength' => 20,'id'=>'phone1', 'placeholder' => 'Phone 1 * ')); ?>
            <?php echo $form->error($user, 'phone1'); ?>
        </div>
        <div class="four columns" style="width:220px;">
            <?php echo $form->textField($user, 'phone2', array('size' => 60, 'maxlength' => 20,'id'=>'phone2', 'placeholder' => 'Phone 2')); ?>
            <?php echo $form->error($user, 'phone2'); ?>
        </div>
        <div class="four columns" style="width:220px;">
            <?php echo $form->textField($user, 'cell1', array('size' => 60, 'maxlength' => 255,'id'=>'cellphone', 'placeholder' => 'Cell Phone')); ?>
            <?php echo $form->error($user, 'cell1'); ?>
        </div>
        <div class="four columns" style="width:220px;">
            <?php echo $form->textField($user, 'fax', array('size' => 60, 'maxlength' => 255,'id'=>'fax', 'placeholder' => 'Fax')); ?>
            <?php echo $form->error($user, 'fax'); ?>
        </div>
        <div class="four columns" style="width:220px;">
            <?php echo $form->textField($user, 'address1', array('size' => 60, 'maxlength' => 255,'id'=>'address1', 'placeholder' => 'Address 1 * ')); ?>
            <?php echo $form->error($user, 'address1'); ?>
        </div>
        <div class="four columns" style="width:220px;">
                <?php echo $form->textField($user, 'address2', array('size' => 60, 'maxlength' => 255,'id'=>'address2', 'placeholder' => 'Address 2')); ?>
            <?php echo $form->error($user, 'address2'); ?>
        </div>
        <div class="four columns"  style="width: 220px; padding-top: 0px;">
            <?php echo $form->textField($user, 'city', array('size' => 60, 'maxlength' => 255,'id'=>'city', 'placeholder' => 'City * ')); ?>
            <?php echo $form->error($user, 'city'); ?>
        </div>
        <div class="four columns"  style="width: 220px; padding-top: 0px;">
            <?php echo $form->textField($user, 'zip', array('size' => 60, 'maxlength' =>255 ,'id'=>'zip', 'placeholder' => 'Zip * ')); ?>
            <?php echo $form->error($user, 'zip'); ?>
        </div>
        <div class="four columns"  style="width: 220px; padding-top: 0px;">
            <?php echo $form->dropDownList($user, 'country',array(),  array('id'=>'countrySelect','style'=>"padding-top:4px !important;",'draggable'=>"true",'onchange'=>'populateState()')); ?>
            <?php echo $form->error($user, 'country'); ?>

        </div>
        <div class="four columns"  style="width: 220px; padding-top: 0px;">
            <?php echo $form->dropDownList($user, 'state',array(), array('id'=>'stateSelect')); ?>
            <?php echo $form->error($user, 'state'); ?>

        </div>
        <div class="four columns" style="width:220px;">
            <?php echo $form->textField($provider, 'astaNum', array('size' => 60, 'maxlength' => 255,'id'=>'astaNum' ,'placeholder' => 'ASTA No./TRUE No.')); ?>
            <?php echo $form->error($provider, 'astaNum'); ?>
            
        </div>
        <div class="four columns" style="width:220px;">
            <?php echo $form->textField($provider, 'iatanNum', array('size' => 60, 'maxlength' => 255,'id'=>'iatanNum' ,'placeholder' => 'IATA No.')); ?>
            <?php echo $form->error($provider, 'iatanNum'); ?>
            
        </div>
        <div class="four columns" style="width:220px;">
             <?php echo $form->textField($user, 'username', array('size' => 60, 'maxlength' => 255,'id'=>'username' ,'placeholder' => 'Username * ')); ?>
            <?php echo $form->error($user, 'username'); ?>
        </div>
        <div class="four columns" style="width:220px;">
            <?php echo $form->passwordField($user, 'password', array('size' => 60,'id'=>'password', 'maxlength' => 255, 'placeholder' => 'Password * ')); ?>
            <?php echo $form->error($user, 'password'); ?>
        </div>
         <div class="agent-notes">
            <?php echo $form->textArea($user, 'notes', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Notes', 'id' => 'notes', 'class' => 'notes')); ?>
            <?php echo $form->error($user, 'notes'); ?>

            <div id="counter" style="text-align:right;"></div>
        </div>
        <div class="agent-notes">
            <div style="float:right;"> 
                <input type="reset" value="Clear" style="margin-right:10px;" id="clear" /><?php echo CHtml::submitButton($provider->isNewRecord ? 'Save' : 'Update'); ?>
            </div>
        </div>
    </div>
<?php $this->endWidget(); ?>
</div>
<!-- form -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/countrystate.js" type="text/javascript"></script>
<script>
    $().ready(function(){
        var characters = 2000;
        $("#counter").append("You have <strong>"+  characters+"</strong> characters remaining");
        $("#notes").keyup(function(){
            if($(this).val().length > characters){
                $(this).val($(this).val().substr(0, characters));
            }
            var remaining = characters -  $(this).val().length;
            $("#counter").html("You have <strong>"+  remaining+"</strong> characters remaining");
            if(remaining <= 10)
            {
                $("#counter").css("color","red");
            }
            else
            {
                $("#counter").css("color","black");
            }
        });
	
        $('input:reset').click(function(){
            $("input:text").next(".ValidationErrors").remove();
            $("input:text").removeClass("ErrorField");
            $("input:password").next(".ValidationErrors").remove();
            $("input:password").removeClass("ErrorField");
            $("select").next(".ValidationErrors").remove();
            $("select").removeClass("ErrorField");
        });
    });
    initCountry('GR');
</script>