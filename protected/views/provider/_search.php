<?php
/* @var $this ProviderController */
/* @var $model Provider */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
            ));
    ?>

    <div class="row">
        <?php echo $form->textField($model, 'providerName', array('size' => 60, 'maxlength' => 255,'placeholder'=>'Provider Name')); ?>
    
        <?php echo $form->textField($model, 'agency', array('size' => 60, 'maxlength' => 255, 'id' => 'agency','placeholder'=>'Agency Name')); ?>
        
        <input type="text" name="country" value="" placeholder="Country" id="country"/>
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->
<script>
    $('body').on('focus',"#agency", function(){
        $(this).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "<?php echo Yii::app()->request->baseUrl; ?>/autocom/agencypro/",
                    dataType: "json",
                    data: {
                        featureClass: "P",
                        style: "full",
                        maxRows: 12,
                        term: request.term
                    },
                    success: function( data ) {
                        response( $.map( data, function( item ) {
                            return {
                                label: item.item,
                                value: item.item
                            }
                        }));
                    }
                });
            },
            minLength: 2,
            select: function (event, ui) {
                $('#'+$(this).attr('name')).val(ui.item.value);
                // update what is displayed in the textbox
                this.value = ui.item.label; 
                return false;
            }
        });
    });
</script> 
<script>
    $('body').on('focus',"#country", function(){
        $(this).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "<?php echo Yii::app()->request->baseUrl; ?>/autocom/country/",
                    dataType: "json",
                    data: {
                        featureClass: "P",
                        style: "full",
                        maxRows: 12,
                        term: request.term
                    },
                    success: function( data ) {
                        response( $.map( data, function( item ) {
                            return {
                                label: item.item,
                                value: item.id
                            }
                        }));
                    }
                });
            },
            minLength: 2,
            select: function (event, ui) {
                $('#'+$(this).attr('name')).val(ui.item.value);
                // update what is displayed in the textbox
                this.value = ui.item.label; 
                return false;
            }
        });
    });
</script> 