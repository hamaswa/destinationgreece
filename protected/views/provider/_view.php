<?php
/* @var $this ProviderController */
/* @var $data Provider */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('providerID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->providerID), array('view', 'id'=>$data->providerID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('providerName')); ?>:</b>
	<?php echo CHtml::encode($data->providerName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agency')); ?>:</b>
	<?php echo CHtml::encode($data->agency); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('website')); ?>:</b>
	<?php echo CHtml::encode($data->website); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('astaNum')); ?>:</b>
	<?php echo CHtml::encode($data->astaNum); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('iatanNum')); ?>:</b>
	<?php echo CHtml::encode($data->iatanNum); ?>
	<br />


</div>