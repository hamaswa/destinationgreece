<?php
/* @var $this ProviderController */
/* @var $model Provider */

$this->breadcrumbs=array(
	'Providers'=>array('index'),
	$provider->title=>array('view','id'=>$provider->providerID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Provider', 'url'=>array('index')),
	array('label'=>'Create Provider', 'url'=>array('create')),
	array('label'=>'View Provider', 'url'=>array('view', 'id'=>$provider->providerID)),
	array('label'=>'Manage Provider', 'url'=>array('admin')),
);
?>
<div style="float:left;margin-left:15px;"><h3>Update Provider <?php echo $provider->providerID; ?></h3></div>


<?php echo $this->renderPartial('_form', array('user'=>$user,'provider'=>$provider)); ?>