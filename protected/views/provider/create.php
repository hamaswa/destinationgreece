<?php
/* @var $this ProviderController */
/* @var $model Provider */

$this->breadcrumbs=array(
	'Providers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Provider', 'url'=>array('index')),
	array('label'=>'Manage Provider', 'url'=>array('admin')),
);
?>

<div style="margin-left:20px;margin-bottom:20px;"><h3>Add Provider</h3></div>
	

<?php echo $this->renderPartial('_form', array('user'=>$user,'provider'=>$provider)); ?>