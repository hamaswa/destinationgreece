<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
    <!--<![endif]--><head>

        <!-- Basic Page Needs
            ================================================== -->
        <meta charset="utf-8">
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <meta name="description" content="">
        <meta name="author" content="lidplussdesign">

        <!-- Mobile Specific Metas
            ================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- CSS
            ================================================== -->
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/styles.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/new/languageswitcher.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/dgtheme/color.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/dgtheme/color1.css">

        <!-- autumn skin -->
        <!-- <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/autumn/color.css"> -->
        <!-- <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/autumn/color1.css"> -->

        <!-- violet skin -->
        <!-- <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/violet/color.css"> -->
        <!-- <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/violet/color1.css"> -->

        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/button-colors.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/background.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/jquery.validate.css" />

        <!-- JS
            ================================================== -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/jquery-1.7.1.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/jquery.validate.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/jquery.validation.functions.js" type="text/javascript"></script>

        <!-- jquery UI -->
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/jqueryui.function.js"></script>
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/jquery-ui.css" /> 
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/ckeditor/ckeditor.js"></script>

        <!--
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/jquery-ui-1.9.2.custom.css" rel="stylesheet">
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/jquery-ui-1.9.2.custom.js"></script>
        
        <!-- jquery UI END-->

        <!-- layerslider 2.0
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/static/layerslider-2.0/layerslider/css/layerslider.css" type="text/css">
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/layerslider-2.0/layerslider/jQuery/jquery-easing-1.3.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/layerslider-2.0/layerslider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/layerslider-2.0/layerslider/js/layerslider.function.js"></script>
        <!-- layerslider 2.0 END-->

        <!-- nivoslider -->
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/static/nivo-slider/nivo-slider.css" type="text/css" media="screen" />
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/nivo-slider-theme.css"/>
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/static/nivo-slider/themes/456theme/456theme.css" type="text/css" media="screen" />
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/nivo-slider/jquery.nivo.slider.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/nivo-slider/nivo.slider.function.js"></script>
        <!-- nivoslider END-->

        <!-- superfish -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/static/superfish-1.4.8/css/superfish.css" media="screen">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/superfish-theme.css" media="screen">
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/superfish-1.4.8/js/hoverIntent.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/superfish-1.4.8/js/superfish.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/superfish.function.js"></script>
        <!-- superfish END-->

        <!-- HoverAlls -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo Yii::app()->request->baseUrl; ?>/static/hoveralls/css/hoveralls.css"/>
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/hoveralls-theme.css"/>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/hoveralls/js/jquery.easing.1.3.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/hoveralls/js/jquery.hoveralls.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/hoveralls/js/jquery.hoveralls-functions.js"></script>
        <!-- HoverAlls END-->

        <!-- isotope -->
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/isotope/jquery.isotope.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/isotope/isotope.function.js"></script>
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/static/isotope/css/style.css">
        <!-- isotope END-->

        <!-- prettyPhoto-->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo Yii::app()->request->baseUrl; ?>/static/prettyPhoto/prettyPhoto.css"/>
        <script type="text/javascript" language="javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/prettyPhoto/jquery.prettyPhoto.js"></script>
        <script type="text/javascript" language="javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/prettyPhoto/prettyPhoto.function.js"></script>
        <!-- prettyPhoto END-->

        <!-- Showbiz-->
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/showbiz/services-plugin/js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/showbiz/services-plugin/js/jquery.cssAnimate.mini.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/showbiz/services-plugin/js/jquery.touchwipe.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/showbiz/services-plugin/js/jquery.mousewheel.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/showbiz/services-plugin/js/jquery.themepunch.services.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/showbiz/services-plugin/js/showbiz.function.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/static/showbiz/services-plugin/css/settings.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/showbiz-theme.css" media="screen">
        <!-- Showbiz END-->

        <!-- widget js -->
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/widget.function.js"></script>
        <!-- widget js END-->

        <!-- blog js -->
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/blog.function.js"></script>
        <!-- blog js END-->

        <!-- animate shadow js -->
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/jquery.animate-shadow-min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/animate-shadow.function.js"></script>
        <!-- animate shadow END-->

        <!-- seaofclouds js -->
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/seaofclouds/jquery.tweet.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/seaofclouds/twitter.function.js"></script>
        <!-- seaofclouds js END-->

        <!-- responsive iframe js -->
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/iframe.function.js"></script>
        <!-- responsive iframe js END-->

        <!-- social icons js -->
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/social.function.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/jquery.animate-colors-min.js"></script>
        <!-- social icons END-->

        <!-- mobile navi js -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/navigation.function.js"></script>
        <!-- mobile navi END-->

        <!-- callout js -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/callout.function.js"></script>
        <!-- callout js END-->

        <!-- grayscale js -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/grayscale.function.js"></script>
        <!-- grayscale js END-->

        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->

        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/opera.css">

        <!-- Favicons
            ================================================== -->
        <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>images/favicon.ico">
        <link rel="apple-touch-icon" href="<?php echo Yii::app()->request->baseUrl ?>images/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo Yii::app()->request->baseUrl ?>images/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo Yii::app()->request->baseUrl ?>images/apple-touch-icon-114x114.png">
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/static/lpd-switcher/colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/static/lpd-switcher/colorpicker/css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/static/lpd-switcher/css/styles.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" id="skins-switcher" href="style.html" type="text/css" media="screen"/>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/lpd-switcher/fontfamily.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/lpd-switcher/colorpicker/js/colorpicker.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/lpd-switcher/jquery.cookie.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/static/lpd-switcher/switcher.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/functions.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/jquery.cookie.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/jquery.form.js"></script>
        <style>
            .form {
                /*width: 50%;*/
                margin: 0 auto;
            }
            .form-row {
                clear: both;
                width: 100%;
                min-height: 25px;
                margin-bottom: 10px;
            }
            input{
                padding-top:4px !important;
                padding-bottom:4px !important;
            }
            .form-row label {
                float: left;
                width: 30%;
                margin: 0;
            }
            .form-row input,.form-row textarea {
                float: left;
                margin: 0;
                padding: 3px 4px;
            }


            .form h1, .form h2, .form h3, .form h4, .form h5, .form h6 {
                width: 100%;
                text-align: center;
            }
            .form .currency {
                width: 70px !important;
            }
            .form input[type="submit"] {
                margin: 0 auto;
            }
            .form-heading {
                width: 95%;
                padding-left: 3%;
                text-align: left;
            }
            .halfwidth {
                width: 65%;
                margin: auto;
            }

        </style>
        <script>
            function removeElem(id){
                $("#"+id).remove();
            }
        </script>
    </head>
    <body>
        <div id="header-bg"></div>
        <div id="header" class="container clearfix" style="padding-left: 0px; padding-right: 0px;">
            <div class="one-third columns" style="margin-left: 0;">
                <div id="headerMetaLogo" class="clearfix display_style"> <a href="#"><img alt="alt" src="<?php echo Yii::app()->request->baseUrl ?>/static/images/logo3-trans-bev.png"> </a> </div>
            </div>
            <div class="one-third columns">
                <div id="headerMeta" class="clearfix"> <span style="color:#fff;font-size:20px;">Your Trusted Experts to Greece</span> </div>
            </div>
            <div class="one-third columns" style="margin-right: 0; float: right;">
                <div style="min-width:296px;">
                    <div id="headerMeta" class="display_style">
                        <div class="meta-data light_shadow"><strong>Toll Free: &nbsp; 888-473-3238 (888-GREECE-8)</strong></div>
                    </div>
                    <div style="height:5px;clear:both;"></div>
                    <div id="country-select">
                        <form action="server-side-script.php" style="display: none;">
                            <select id="country-options" name="country-options">
                                <option selected="selected" value="us">English (en)</option>
                                <option value="es">Español (es)</option>
                                <option value="fr">Français (fr)</option>
                                <option value="pt">Português (pt)</option>
                                <option value="ru">Русский (ru)</option>
                            </select>
                            <input value="Select" type="submit">
                        </form>

                    </div>
                    <a style="float:right; margin-right:6px;font-size: 11px; text-align:center;" href="javascript: w=window.open('https://support.telcan.net/CallMe/?ID=CR0150132528&No=6343774','CallMe','toolbar=0,scrollbars=0,width=350,height=330,left=0,top=0'); w.focus();" class="button callout-button">Contact Us</a> </div>
            </div>
        </div>
        <div id="main">
            <div id="navi" class="display_style">
                <div class="container">
                    <div class="thirteen columns">
                        <ul class="navi light_shadow sf-menu">
                            <li class="current"><a href="<?php echo $this->createUrl('/'); ?>"><strong>Home</strong></a>
                                <ul>
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Testimonials</a></li>
                                    <li><a href="#">Media References</a></li>
                                    <li><a href="#">Job Opportunities</a></li>
                                    <li><a href="#">Operations</a></li>
                                    <li><a href="#">Contact</a></li>
                                </ul>
                            </li>
                            <li><a href="full-width-template.html"><strong>Vacation Packages</strong></a>
                                <ul>
                                    <li><a href="about.html">Greek Islands</a></li>
                                    <li><a href="#">Greece HoneyMoon</a></li>
                                    <li><a href="#">Greek Island Cruise</a></li>
                                    <li><a href="#">Greece Land Tours</a></li>
                                    <li><a href="#">Egypt</a></li>
                                    <li><a href="#">Turkey</a></li>
                                    <li><a href="#">Combination Packages</a></li>
                                    <li><a href="#">Spiritual</a></li>
                                </ul>
                            </li>
                            <li><a href="portfolio-4.html"><strong>More Services</strong></a>
                                <ul>
                                    <li><a href="#">Special Offers</a></li>
                                    <li><a href="#">Short Excursions</a></li>
                                    <li><a href="#">Conferences</a></li>
                                    <li><a href="#">Hotel presentation</a></li>
                                    <li><a href="#">Luxury Villas</a></li>
                                    <li><a href="#">Yacht Chartering</a></li>
                                    <li><a href="#">Car Rental</a></li>
                                    <li><a href="#">Weddings A La Carte</a></li>
                                </ul>
                            </li>
                            <li><a href="blog.html"><strong>Community</strong></a>
                                <ul>
                                    <li><a href="#">Destination Info</a></li>
                                    <li><a href="#">Group Request Form</a></li>
                                    <li><a href="#">RSS News</a></li>
                                    <li><a href="#">RSS Community</a></li>
                                    <li><a href="#">Brochures</a></li>
                                    <li><a href="#">Linked Sites</a></li>
                                </ul>
                            </li>
                            <?php if (Yii::app()->user->isGuest): ?>
                            <li>
                              <a href="<?php echo $this->createUrl('/login'); ?>"><strong>Login</strong></a>
                            </li>
                            <?php else: ?>
                            <li>
                              <a href="<?php echo $this->createUrl('/dashboard'); ?>"><strong><?php echo Yii::app()->user->name; ?></strong></a>
                              <ul>
                                <li><a href="<?php echo $this->createUrl('/dashboard'); ?>">Dashboard</a></li>
                                <li><a href="<?php echo $this->createUrl('/logout'); ?>">Logout</a></li>
                              </ul>
                            </li>
                            <?php endif; ?>
                        </ul>
                        <div class="mobileNavi_wrap"></div>
                    </div>
                    <div class="three columns">
                        <form method="get" action="#">
                            <fieldset>
                                <input id="none" class="search_input" type="text" name="none" value="find a package" onfocus="if(this.value == 'search site') { this.value = ''; }" onblur="if(this.value==''){this.value = 'find a package';}"/>
                                <input class="submit" type="submit" value="+"/>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div id="callout">
                <div class="container">
                    <?php echo $content; ?>
                </div>
            </div>
            <div class="clear"></div>
            <div id="footer">
                <div class="footer">
                    <div class="container">
                        <div class="one-third column">
                            <div class="widget contact-widget">
                                <h4>Contact Us</h4>
                                <p>You may freely contact us by following ways:</p>
                                <ul>
                                    <li><span>phone :</span> +44 (0) 1234567890</li>
                                    <li><span>fax :</span> +44 (0) 1234567890</li>
                                    <li><span>email :</span> <a href="#" title="title">services@destinationgreece.com</a></li>
                                    <li><span>address :</span> Leathermarket court, SE1</li>
                                </ul>
                            </div>
                        </div>
                        <div class="one-third column">
                            <div class="widget category-widget">
                                <h4>Site Links</h4>
                                <ul>
                                    <li><a href="#" title="title">Terms &amp; Conditions</a></li>
                                    <li><a href="#" title="title">About Us</a></li>
                                    <li><a href="#" title="title">FAQs</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="one-third column">
                            <div class="widget post-widget">
                                <h4>Recent Blog <span>Posts</span></h4>
                                <ul>
                                    <li class="item clearfix">
                                        <div class="icon">
                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/static/images/48x48_photodune-202935-business-growth-hands-holding-green-plant-indicating-teamwork-m.jpg" alt="alt" height="48" width="48"/>
                                        </div>
                                        <div class="content">
                                            <a href="#" title="title">Lorem ipsum dolor sit amet, consectetur</a>
                                            <a class="date" href="#" title="2nd Dec 2011">2nd Dec 2011</a> <a class="comments" href="#" title="29 Comments">29</a>
                                        </div>
                                    </li>
                                    <li class="item clearfix">
                                        <div class="icon">
                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/static/images/post-widget-link.png" alt="alt" height="48" width="48"/>
                                        </div>
                                        <div class="content">
                                            <a href="#" title="title">Lorem ipsum dolor sit amet, consectetur</a>
                                            <a class="date" href="#" title="2nd Dec 2011">2nd Dec 2011</a> <a class="comments" href="#" title="29 Comments">29</a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="footer-sub-2">
                        <p>&copy;Copyright <?php echo date('Y'); ?> Destination Greece. All Rights Reserved. Disclaimer for use of this site.
                            Please direct Questions or Comments to: services@DestinationGreece.com<br>
                            CST: 2093898-40 Registration as a seller of travel does not constitute approval by the State of California
                            FLA Seller of Travel Ref. No. ST37432
                        </p>
                    </div>
                    <div class="footer-sub-2 sub">
                        <p>Testimonials | Media references | Job opportunities | Register | Affiliates | Advertising | Links
                            Hotels in Greece | Villas in Greece | Group request form | RSS news | RSS offers  <br> Newsletters | Brochures
                            Greece vacation packages | Greek travel packages | Greece honeymoon packages | Greece tours | Greece island cruises | Greece weddings </p>
                    </div>

                </div>
            </div>
        </div>
        <div style="height:20px;"></div>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/new/languageswitcher.js"></script>

    </body>
</html>

