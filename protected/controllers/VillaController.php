<?php

class VillaController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
public function accessRules() {
        return array(
            array('allow',
                'actions' => array('admin', 'delete', 'create', 'update', 'index', 'view'),
                'roles' => array('admin', 'operator'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
   }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $villa = new Villa;
        $roomType = new Roomtype();
        $hotelRoomType = new Hotelroomtype();
        $villaFacility = new Villafacility();
        $photo = new Photo();
        $villaPhoto = new Villaphoto();
        $villaRates = new Villarates();
// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

        if (isset($_POST['Villa'])) {
//            echo '<pre>';
//            print_r($_POST);
//            echo '</pre>'; //exit;
            $villa->attributes = $_POST['Villa'];
            if ($villa->save()) {
                $villaID = $villa->ID;
                foreach ($_POST['Villafacility']['villaFacility'] as $facility) {
                    $villaFac = new Villafacility();
                    $villaFac->attributes = array('villaFacility' => $facility, 'villaID' => $villaID);
                    $villaFac->save(false);
                }
                foreach ($_POST['photo']['ID'] as $id) {
                    $villaPh = new Villaphoto();
                    $villaPh->attributes = array('ID' => $id, 'villaID' => $villaID);
                    $villaPh->save();
                }
                $countTypes = 0;
               // foreach ($_POST['roomtype'] as $room) {
               //     $roomT = new Hotelroomtype();
               //     $roomT->attributes = array('villaID' => $villaID,
               //         'roomType' => $room,
               //         'numAdults' => $_POST['numberofadults'][$countTypes]);
                   // if ($roomT->save()) {
                        $j = 0;
                        foreach ($_POST['rate' . $countTypes] as $rate) {
                            $villaRate = new Villarates();
                            $villaRate->attributes = array(
                                'villaID' => $villaID,
                                'fromDate' => $_POST['fromDate' . $countTypes][$j],
                                'toDate' => $_POST['toDate' . $countTypes][$j],
                                'dgCommission' => $_POST['dgCom' . $countTypes][$j],
                                'ccHandlingpc' => $_POST['cardCom' . $countTypes][$j],
                                'status' => $_POST['status' . $countTypes][$j],
                                'netRate' => $rate,
                                
                            );
                            //'roomTypeID' => $roomT->ID,
                            $villaRate->save(false);
                            $j++;

                            //  echo $rate.' '.$_POST['fromDate'.$countTypes][$j].' '.$_POST['toDate'.$countTypes][$j];
                        }
                   // }
                   // $countTypes++;
               //}
                $this->redirect(array('view', 'id' => $villa->ID));
            }
        }

        $this->render('create', array(
            'villa' => $villa,
            'villaFacility' => $villaFacility,
            'villaPhoto' => $villaPhoto,
            'hotelRoomType' => $hotelRoomType,
            'roomType' => $roomType,
            'villaRates' => $villaRates
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

        if (isset($_POST['Villa'])) {
            $model->attributes = $_POST['Villa'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->ID));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Villa');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Villa('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Villa']))
            $model->attributes = $_GET['Villa'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Villa the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Villa::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Villa $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'villa-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}