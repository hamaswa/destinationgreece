<?php

/**
 * @author destinationgreece-dev@masoodware.com
 */
class LogoutController extends Controller {

  public function actionIndex() {
    Yii::app()->user->logout();
    $this->redirect(Yii::app()->homeUrl);
  }

}
