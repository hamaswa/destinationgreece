<?php

class AutocomController extends Controller {

    public function actionIndex() {
        
    }

    public function actionAgent() {
        $res = array();
        $term = $_GET['term'];
        if ($term) {
            $sql = 'SELECT agentID,  agentName FROM agent where agentName like :name ';
            $cmd = Yii::app()->db->createCommand($sql);
            $cmd->bindValue(":name", "%" . strtolower($term) . "%", PDO::PARAM_STR);
            $res = $cmd->queryAll();
        }

        foreach ($res as $model) {
            $arr[] = array(
                'id' => $model['agentID'],
                'nome' => $model['agentName'],
            );
        }

        echo CJSON::encode($arr);
        Yii::app()->end();
    }
     public function actionProvider() {
        $res = array();
        $term = $_GET['term'];
        if ($term) {
            $sql = 'SELECT providerName,providerID FROM provider where providerName like :name ';
            $cmd = Yii::app()->db->createCommand($sql);
            $cmd->bindValue(":name", "%" . strtolower($term) . "%", PDO::PARAM_STR);
            $res = $cmd->queryAll();
        }

        foreach ($res as $model) {
            $arr[] = array(
                'item' => $model['providerName'],
                'id'=>$model['providerID']
            );
        }

        echo CJSON::encode($arr);
        Yii::app()->end();
    }
 public function actionRoomtype() {
        $res = array();
        $term = $_GET['name_startsWith'];
        if ($term) {
            $sql = 'SELECT ID,  type FROM roomtype where type like :name ';
            $cmd = Yii::app()->db->createCommand($sql);
            $cmd->bindValue(":name", "%" . strtolower($term) . "%", PDO::PARAM_STR);
            $res = $cmd->queryAll();
        }

        foreach ($res as $model) {
            $arr[] = array(
                'id' => $model['ID'],
                'name' => $model['type'],
            );
        }

        echo CJSON::encode($arr);
        Yii::app()->end();
    }
    public function actionCabintype() {
        $res = array();
        $term = $_GET['name_startsWith'];
        if ($term) {
            $sql = 'SELECT ID,  name FROM cabintype where name like :name ';
            $cmd = Yii::app()->db->createCommand($sql);
            $cmd->bindValue(":name", "%" . strtolower($term) . "%", PDO::PARAM_STR);
            $res = $cmd->queryAll();
        }

        foreach ($res as $model) {
            $arr[] = array(
                'id' => $model['ID'],
                'name' => $model['name'],
            );
        }

        echo CJSON::encode($arr);
        Yii::app()->end();
    }
    public function actionTourtype() {
        $res = array();
        $term = $_GET['term'];
        if ($term) {
            $sql = 'SELECT ID,  name FROM tourtype where name like :name ';
            $cmd = Yii::app()->db->createCommand($sql);
            $cmd->bindValue(":name", "%" . strtolower($term) . "%", PDO::PARAM_STR);
            $res = $cmd->queryAll();
        }

        foreach ($res as $model) {
            $arr[] = array(
                'id' => $model['ID'],
                'name' => $model['name'],
            );
        }

        echo CJSON::encode($arr);
        Yii::app()->end();
    }
    public function actionAgencypro() {
        $res = array();
        $term = $_GET['term'];
        if ($term) {
            $sql = 'SELECT agency FROM provider where agency like :name ';
            $cmd = Yii::app()->db->createCommand($sql);
            $cmd->bindValue(":name", "%" . strtolower($term) . "%", PDO::PARAM_STR);
            $res = $cmd->queryAll();
        }

        foreach ($res as $model) {
            $arr[] = array(
                'item' => $model['agency'],
            );
        }

        echo CJSON::encode($arr);
        Yii::app()->end();
    }
    public function actionCountry() {
        $res = array();
        $term = $_GET['term'];
        if ($term) {
            $sql = 'SELECT name,ID FROM country where name like :name ';
            $cmd = Yii::app()->db->createCommand($sql);
            $cmd->bindValue(":name", "%" . strtolower($term) . "%", PDO::PARAM_STR);
            $res = $cmd->queryAll();
        }

        foreach ($res as $model) {
            $arr[] = array(
                'item' => $model['name'],
                'id' => $model['ID']
            );
        }

        echo CJSON::encode($arr);
        Yii::app()->end();
    }
    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }

      public function actions()
      {
      // return external action classes, e.g.:
      return array(
      'action1'=>'path.to.ActionClass',
      'action2'=>array(
      'class'=>'path.to.AnotherActionClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */
}