<?php

/**
 * @author Destination Greece Developers <destinationgreece-dev@masoodware.com>
 */
class LoginController extends Controller {

  public function actionIndex() {
    if (!Yii::app()->user->isGuest) {
      $this->redirect($this->createUrl('/dashboard'));
    }

    $model = new LoginForm;

    // check for ajax request first
    if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
      echo CActiveForm::validate($model);
      Yii::app()->end();
    }

    if (isset($_POST['LoginForm'])) {
      $model->attributes = $_POST['LoginForm'];
      if ($model->validate() && $model->login()) {
        $this->redirect(Yii::app()->user->returnUrl);
      }
    }

    $this->render('index', array('model' => $model));
  }

}
