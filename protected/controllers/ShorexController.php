<?php

class ShorexController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
//				'users'=>array('*'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete'),
//				'users'=>array('admin'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Shorex;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Shorex'])) {
//            echo '<pre>';
//            print_r($_POST);
//            echo '</pre>';
//            exit;
            $model->attributes = $_POST['Shorex'];
            if ($model->save(false))
                $shorexID = $model->ID;
            foreach ($_POST['Photo']['ID'] as $id) {
                $tourPh = new Shorexphoto();
                $tourPh->attributes = array('ID' => $id, 'shorexID' => $shorexID);
                $tourPh->save();
            }
           // exit;
            $countTypes = 0;
            foreach ($_POST['shorextype'] as $shorextype) {
                $shorexT = new Shorextype();
                $shorexT->attributes = array('name' => $shorextype, 'shorexID' => $shorexID);
                if ($shorexT->save(false)) {
                    $j = 0;
                    
                    foreach ($_POST['standard' . $countTypes] as $standard) {
                        $shorexRate = new Shorexrate();
                        $shorexRate->attributes = array(
                            'shorexID' => $shorexID,
                            'fromDate' => $_POST['fromDate' . $countTypes][$j],
                            'toDate' => $_POST['toDate' . $countTypes][$j],
                            'standard' => $standard,
                            'sgl3' => $_POST['sgl3' . $countTypes][$j],
                            'sgl4' => $_POST['sgl4' . $countTypes][$j],
                            'dbl3' => $_POST['dbl3' . $countTypes][$j],
                            'dbl4' => $_POST['dbl4' . $countTypes][$j],
                            'sgl5' => $_POST['sgl5' . $countTypes][$j],
                            'dbl5' => $_POST['dbl5' . $countTypes][$j],
                            'shorexType' => $shorexT->ID,
                        );
                        $shorexRate->save();
                        $j++;
                    }
                }
                $countTypes++;
            }
            $this->redirect(array('view','id'=>$model->ID));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Shorex'])) {
            $model->attributes = $_POST['Shorex'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->ID));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Shorex');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Shorex('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Shorex']))
            $model->attributes = $_GET['Shorex'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Shorex the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Shorex::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Shorex $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'shorex-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
