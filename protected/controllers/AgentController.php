<?php

class AgentController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
public function accessRules() {
        return array(
            array('allow',
                'actions' => array('admin', 'delete', 'create', 'update', 'index', 'view'),
                'roles' => array('admin', 'operator'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
   }
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$agent=new Agent;
		$user= new User;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		if(isset($_POST['User'])&&isset($_POST['Agent']))
		{		
			$user->attributes=$_POST['User'];
			$user->save(false);
			$_POST['Agent']['agentID']=$user->ID;
			$agent->attributes=$_POST['Agent'];
			if($agent->save(false))
				$this->redirect(array('view','id'=>$agent->agentID));
		}
		$this->render('create',array(
			'agent'=>$agent,'user'=>$user
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$user=$this->loadModel2($id);		
		$agent=$this->loadModel($id);
		//echo '<pre>';print_r($user);echo '</pre>';exit;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($user);

		if(isset($_POST['Agent'])&&isset($_POST['User']))
		{
			$user->attributes=$_POST['User'];
			$agent->attributes=$_POST['Agent'];
			$user->save(false);
			if($agent->save(false))
				$this->redirect(array('view','id'=>$user->ID));
		}

		$this->render('update',array(
			'user'=>$user,'agent'=>$agent
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Agent');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Agent('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Agent']))
			$model->attributes=$_GET['Agent'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Agent the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Agent::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	public function loadModel2($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Agent $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='agent-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
