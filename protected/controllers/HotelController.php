<?php

class HotelController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
public function accessRules() {
        return array(
            array('allow',
                'actions' => array('admin', 'delete', 'create', 'update', 'index', 'view'),
                'roles' => array('admin', 'operator'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
   }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $hotel = new Hotel;
        $roomType = new Roomtype();
        $hotelRoomType = new Hotelroomtype();
        $hotelFacility = new Hotelfacility();
        $hotelAmenity = new Hotelamenity();
        $photo = new Photo();
        $hotelPhoto = new Hotelphoto();
        $hotelRates = new Hotelrates();
        if (isset($_POST['Hotel'])) {
//            echo '<pre>';
//            print_r($_POST);
//             print_r($_POST['roomtype']);
//            echo '</pre>';
//            exit;
            $hotel->attributes = $_POST['Hotel']; //saving hotel attributes
            $hotel->save(false);
            $hotelID = $hotel->ID;
            foreach ($_POST['Hotelfacility']['hotelFacility'] as $facility) {
                $hotelFac = new Hotelfacility();
                $hotelFac->attributes = array('hotelFacility' => $facility, 'hotelID' => $hotelID);
                $hotelFac->save(false);
            }
            foreach ($_POST['Hotelamenity']['hotelAmenity'] as $amenity) {
                $hotelAm = new Hotelamenity();
                $hotelAm->attributes = array('hotelAmenity' => $amenity, 'hotelID' => $hotelID);
                $hotelAm->save(false);
            }
            foreach ($_POST['Photo']['ID'] as $id){
                $hotelPh=new Hotelphoto();
                $hotelPh->attributes=array('ID'=>$id,'hotelID'=>$hotelID);
                $hotelPh->save();
            }
            
            $countTypes=0;
            foreach($_POST['roomtype'] as $room){
                $roomT=new Hotelroomtype();
                $roomT->attributes=array('hotelID'=>$hotelID,'roomType'=>$room,'numAdults'=>$_POST['numberofadults'][$countTypes]);
                if($roomT->save()){
                $j=0;
                foreach($_POST['rate'.$countTypes] as $rate){
                    $hotelRate=new Hotelrates();
                    $hotelRate->attributes=array(
                        'hotelID'=>$hotelID,
                        'fromDate'=>$_POST['fromDate'.$countTypes][$j],
                        'toDate'=>$_POST['toDate'.$countTypes][$j],
                        'rate'=>$rate,
                        'roomTypeID'=>$roomT->ID,
                        );
                    $hotelRate->save(false);
                    $j++;
                    
                  //  echo $rate.' '.$_POST['fromDate'.$countTypes][$j].' '.$_POST['toDate'.$countTypes][$j];
                }
                }
                $countTypes++;
            }           
           $this->redirect(array('view', 'id' => $hotel->ID));
        }

        $this->render('create', array(
            'hotel' => $hotel,
            'hotelAmenity' => $hotelAmenity,
            'hotelFacility' => $hotelFacility,
            'hotelPhoto' => $hotelPhoto,
            'hotelRoomType' => $hotelRoomType,
            'roomType' => $roomType,
            'hotelRates' => $hotelRates,
            'hotelPhoto' => $hotelPhoto
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Hotel'])) {
            $model->attributes = $_POST['Hotel'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->ID));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Hotel');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Hotel('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Hotel']))
            $model->attributes = $_GET['Hotel'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Hotel the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Hotel::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Hotel $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'hotel-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
