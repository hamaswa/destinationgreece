<?php

class DropdownController extends Controller {

    public function actionAgent() {
        $sql = 'SELECT agentID,  agentName FROM agent order by agentName';
        $cmd = Yii::app()->db->createCommand($sql);
        $res = $cmd->queryAll();
        foreach($res as $agent){
            $arr[$agent['agentID']]=$agent['agentName'];
        }
        return $arr;
    }

    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }

      public function actions()
      {
      // return external action classes, e.g.:
      return array(
      'action1'=>'path.to.ActionClass',
      'action2'=>array(
      'class'=>'path.to.AnotherActionClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */
}