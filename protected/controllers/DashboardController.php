<?php

/**
 * Dashboard
 *
 * @author Destination Greece Developers <destinationgreece-dev@masoodware.com>
 */
class DashboardController extends CController {

  public function actionIndex() {
    return $this->render('index');
  }

  public function filters() {
    return array(
        'accessControl',
    );
  }

  public function accessRules() {
    return array(
        array(
            'allow',
            'users' => array('@'),
        ),
        array(
            'deny',
        ),
    );
  }

}
