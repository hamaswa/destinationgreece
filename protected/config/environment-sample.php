<?php

/**
 * Environment configuration
 * 
 * Put your environment specific settings here.
 */

// The following example demonstrates database configuration
return array(
    'components' => array(
        'db' => array(
            'connectionString' => 'mysql:host=;dbname=',
            'username' => '',
            'password' => '',
        ),
    ),
);
