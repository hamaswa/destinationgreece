<?php

if (!file_exists(dirname(__FILE__).'/protected/initialize.php')) {
  die('Please read protected/initialize-sample.php');
}

require_once(dirname(__FILE__).'/protected/initialize.php');

$config = array();
$config_files = array(
  'base',
  'environment',
);

foreach ($config_files as $f) {
  $c = require_once(dirname( __FILE__ )."/protected/config/$f.php");
  $config = CMap::mergeArray($config, $c);
}

Yii::createWebApplication($config)->run();
