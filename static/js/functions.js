function getClientDates(){
    $( "#arrivalDate" ).datepicker({
        showButtonPanel: true, 
        dateFormat: 'yy-mm-dd',
        minDate:'2012-09-01', 
        maxDate:getMaxDate('departureDate') , 
        beforeShow: function( input ) {
            AddClearButton(input);
        },
        onChangeMonthYear:function( year, month, inst ) {
            AddClearButton(inst.input);
        }
	
    });
    $( "#departureDate" ).datepicker({
        showButtonPanel: true, 
        dateFormat: 'yy-mm-dd',
        minDate:getMinDate('arrivalDate'), 
        maxDate:getCurrentDate(), 
        beforeShow: function( input ) {
            AddClearButton(input);
        },
        onChangeMonthYear:function( year, month, inst ) {
            AddClearButton(inst.input);
        }
	
    });
}
function getMinDate(id){
    date = $("#"+id).val();
    if(date.length){
        return date;
    }
    return '2012-09-01'
}
function getMaxDate(id){
    date = $("#"+id).val();
    if(date.length){
        return date;
    }
    return getCurrentDate();
}
function getCurrentDate(){
    var currentTime = new Date()
    var month = currentTime.getMonth() + 1
    var day = currentTime.getDate()
    var year = currentTime.getFullYear()
    return year + "-" + month + "-" + day;
}
function AddClearButton(input){
    setTimeout(function() {  
        var buttonPane = $( input )  
        .datepicker( "widget" )  
        .find( ".ui-datepicker-buttonpane" );  
  
        var btn = $('<button class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" type="button">Clear</button>');  
        btn  
        .unbind("click")  
        .bind("click", function () {  
            $.datepicker._clearDate( input );  
        });  
  
        btn.appendTo( buttonPane );  
  
    }, 1 );  
}
function addRate(id){
			
    html='<div class="remove hotelremove"><div class="clear"></div>';
    html+='<input type="text" name="fromDate'+id+'[]" placeholder="From Date" class="fromDate two columns" />';
    html+='<input type="text" name="toDate'+id+'[]" placeholder="To Date" class="toDate two columns"/>';
    html+='<input type="text" name="rate'+id+'[]" placeholder="Rate" class="one columns" />';
    html+='<span class="removerate" id="removethis"><a class="button large_button delete"><span></span>rate</a></span></div>';
    $('#rate-div'+id).append(html);
}
function photoUpdate(response,dir){
    uploadID =response.uploadID;
    if(response.success){
        $("#photo-div").append('<input type="hidden" id="photo-'+response.uploadID+'" value="'+response.uploadID+'" name="Photo[ID][]"/>');
        $("#photo-list").append('<li style="float:left;width:180px" id="photo-li-'+response.uploadID+'"><img src="<?php echo Yii::app()->request->baseUrl; ?>/'+dir+response.filename+'" width="100" /> <a href="#"  onclick="deletePhoto(\''+response.uploadID+'\',\''+dir+'\'  ); return false;">Delete</a><li>');
    }
        
}
function deletePhoto(id,dir){
    $.ajax({
        url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/photo/delete/"+id
    });
    $("#photo-li-"+id).remove();
    $("#photo-"+id).remove();
}
function addVillaRate(id){
			
        html='<div class="remove villaremove"><div class="clear"></div>';
        html+='<input type="text" name="fromDate'+id+'[]" placeholder="From Date" class="fromDate two columns" />';
        html+='<input type="text" name="toDate'+id+'[]" placeholder="To Date" class="toDate two columns"/>';
        html+='<input type="text" name="rate'+id+'[]" placeholder="Rate" class="one columns" />';
        html+='<select name="status'+id+'[]" class="two columns status">';
        html+='<option value="">Show Rate</option>';
        html+='<option value="0">No</option>';
        html+='<option value="1">Yes</option>';
        html+='</select>';
        html+='<input type="text" name="dgCom'+id+'[]" placeholder="DG %" class="toDate one columns"/>';
        html+='<input type="text" name="cardCom'+id+'[]" placeholder="Card %" class="toDate one columns"/>';
        html+='<span class="removerate" id="removethis"><a class="button large_button delete"><span></span>rate</a></span></div>';
        $('#rate-div'+id).append(html);
    }
function addTourRate(id){
     html='<div class="remove tourremove"><div class="clear"></div>';
        html+='<input type="text" name="fromDate'+id+'[]" placeholder="From Date" class="fromDate two columns" />';
        html+='<input type="text" name="toDate'+id+'[]" placeholder="To Date" class="toDate two columns"/>';
        html+='<input type="text" name="standrd'+id+'[]" placeholder="Standard" class="one columns" />';
        html+='<input type="text" name="sgl3'+id+'[]" placeholder="3 * sgl" class="toDate one columns"/>';
        html+='<input type="text" name="dbl3'+id+'[]" placeholder="3 * dbl" class="toDate one columns"/>';
        html+='<input type="text" name="sgl4'+id+'[]" placeholder="4 * sgl" class="toDate one columns"/>';
        html+='<input type="text" name="dbl4'+id+'[]" placeholder="4 * dbl" class="toDate one columns"/>';
        html+='<span class="removerate" id="removethis"><a class="button large_button delete"><span></span>rate</a></span></div>';
        $('#rate-div'+id).append(html);
}
function addShorexRate(id){
	     html='<div class="remove shorexremove"><div class="clear"></div>';
//        html+='<input type="text" name="fromDate'+id+'[]" placeholder="From Date" class="fromDate two columns" />';
//        html+='<input type="text" name="toDate'+id+'[]" placeholder="To Date" class="toDate two columns"/>';
        html+='<input type="text" name="standrd'+id+'[]" placeholder="Standard" class="one columns" />';
        html+='<input type="text" name="sgl3'+id+'[]" placeholder="3 * sgl" class="toDate one columns"/>';
        html+='<input type="text" name="dbl3'+id+'[]" placeholder="3 * dbl" class="toDate one columns"/>';
        html+='<input type="text" name="sgl4'+id+'[]" placeholder="4 * sgl" class="toDate one columns"/>';
        html+='<input type="text" name="dbl4'+id+'[]" placeholder="4 * dbl" class="toDate one columns"/>';
        html+='<input type="text" name="sgl4'+id+'[]" placeholder="5 * sgl" class="toDate one columns"/>';
        html+='<input type="text" name="dbl5'+id+'[]" placeholder="5 * dbl" class="toDate one columns"/>';
        html+='<span class="removerate" id="removethis"><a class="button large_button delete"><span></span>rate</a></span></div>';
        $('#rate-div'+id).append(html);
}