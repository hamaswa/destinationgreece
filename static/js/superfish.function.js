		// initialise plugins
		jQuery(function(){
			jQuery('ul.sf-menu').superfish({
				delay:       300,                // one second delay on mouseout
				speed:       0,                  // faster animation speed
				speedOut:	 100,                // speed of the closing animation. Equivalent to second parameter of jQuery’s .animate() method
				disableHI:   false,              // set to true to disable hoverIntent detection
			});
		});