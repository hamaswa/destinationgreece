$(window).load(function () {

    $('.tooltip').HoverAlls({
        tooltip:true,
        starts:"0px,18px",
        ends:"0,23px",
        returns:"0px,18px",
        bg_class:"tooltipBg",
        text_class:"tooltipTx",
        speed_in:1000,
        speed_out:380,
        effect_in:"easeOutBack",
        effect_out:"easeInSine",
        bg_width:"199px",
        bg_height:"auto",
        container_class:"tooltip_container",
        link_target:"_parent"
    });
    
    $('.img-hoveralls img').HoverAlls({
        starts:"50%,100%",
        ends:"50%,50%",
        returns:"50%,100%",
        bg_class:"overlay_backgrond",
        text_class: false,
        speed_in:400,
        speed_out:400,
        bg_width:"54px",
        bg_height:"54px",
        big_link:true,
        container_class:"overlay_container",
        link_target:"_parent"
    });
  
});