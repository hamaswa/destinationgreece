$(document).ready(function(){
    $('#body-font').bind('change', function() {
        var font = $(this).val();
         $('body').css('fontFamily', font);
    });
    $('#heading-font').bind('change', function() {
        var font = $(this).val();
         $('h1').css('fontFamily', font);
         $('h2').css('fontFamily', font);
         $('h3').css('fontFamily', font);
         $('h4').css('fontFamily', font);
         $('h5').css('fontFamily', font);
         $('h6').css('fontFamily', font);
    });
});