$(document).ready(function() {
    $("#switcher > #handle").toggle(function(){
    	$('#switcher').animate({left:'0px'}, {queue:false,duration:200});
    	$('#switcher > #handle').addClass('active');
    }
    ,function(){
    	$('#switcher').animate({left:'-192px'}, {queue:false,duration:200});
    	$('#switcher > #handle').removeClass('active');
    });
});

var cssText              = '',
    forValue             = '';

var cssStuff = new Array();


cssStuff['ellipce']   = $.cookie("cookie_color_ellipce");
cssStuff['background']      = $.cookie("cookie_color_background");
cssStuff['color']      = $.cookie("cookie_color_color");

    
function createCSSColor1() {    
    cssText              = "#header-bg{";
    cssText             += "     background: " + cssStuff['background'] + ";";
    cssText             += "     background: -webkit-gradient(linear, left top, left bottom, color-stop(0%," + cssStuff['ellipce'] + "), color-stop(100%," + cssStuff['background'] + "));";
    cssText             += "     background: -moz-radial-gradient(center, ellipse cover,  " + cssStuff['ellipce'] + " 0%, " + cssStuff['background'] + " 100%);";
    cssText             += "     background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%," + cssStuff['ellipce'] + "), color-stop(100%," + cssStuff['background'] + "));";
    cssText             += "     background: -webkit-radial-gradient(center, ellipse cover,  " + cssStuff['ellipce'] + " 0%," + cssStuff['background'] + " 100%);";
    cssText             += "     background: -o-radial-gradient(center, ellipse cover,  " + cssStuff['ellipce'] + " 0%," + cssStuff['background'] + " 100%);";
    cssText             += "     background: -ms-radial-gradient(center, ellipse cover,  " + cssStuff['ellipce'] + " 0%," + cssStuff['background'] + " 100%);";
    cssText             += "     background: radial-gradient(center, ellipse cover,  " + cssStuff['ellipce'] + " 0%," + cssStuff['background'] + " 100%);";
    cssText             += "     filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='" + cssStuff['background'] + "', endColorstr='" + cssStuff['ellipce'] + "',GradientType=0 );";
    cssText             += "}";
    cssText             += "#copyright a{";
    cssText             += "     color: " + cssStuff['background'] + ";";
    cssText             += "}";
    cssText             += ".dropcap2{";
    cssText             += "     background-color: " + cssStuff['background'] + ";";
    cssText             += "}";
    cssText             += ".theme_table tr.description td.selected,";
    cssText             += ".theme_table tr.price td.selected{";
    cssText             += "     background-color: " + cssStuff['background'] + ";";
    cssText             += "}";
    cssText             += ".theme_table .selected .button{";
    cssText             += "     background-color: " + cssStuff['background'] + ";";
    cssText             += "     border-color: " + cssStuff['background'] + ";";
    cssText             += "}";

            
    $("#the-style").replaceWith("<style id='the-style' type='text/css'>" + cssText + "</style>");
    cssText = cssText
        .replace(/;/g,";<br />&nbsp;&nbsp;").replace(/{/g,"{<br />&nbsp;&nbsp;").replace(/}/g,"&nbsp;}<br />");
        

}
function createCSSColor() {    
    cssText              = "a{";
    cssText             += "     color: " + cssStuff['color'] + ";";
    cssText             += "}";
    cssText             += "h1 span,";
    cssText             += "h2 span,";
    cssText             += "h3 span,";
    cssText             += "h4 span,";
    cssText             += "h5 span,";
    cssText             += "h6 span{";
    cssText             += "     color: " + cssStuff['color'] + ";";
    cssText             += "}";
    cssText             += "ul.navi li:hover a strong{";
    cssText             += "     color: " + cssStuff['color'] + ";";
    cssText             += "}";
    cssText             += "ul.navi li.current a strong{";
    cssText             += "     background-color: " + cssStuff['color'] + ";";
    cssText             += "}";
    cssText             += "ul.navi ul li a:hover{";
    cssText             += "     background-color: " + cssStuff['color'] + ";";
    cssText             += "}";
    cssText             += ".button,";
    cssText             += "button,";
    cssText             += "input[type='submit'],";
    cssText             += "input[type='reset'],";
    cssText             += "input[type='button']{";
    cssText             += "     background-color: " + cssStuff['color'] + ";";
    cssText             += "     border: 1px solid " + cssStuff['color'] + ";";
    cssText             += "}";
    cssText             += ".meta ul li a:hover{";
    cssText             += "     color: " + cssStuff['color'] + ";";
    cssText             += "}";
    cssText             += ".pagination .current{";
    cssText             += "     background-color: " + cssStuff['color'] + ";";
    cssText             += "}";
    cssText             += ".tags a{";
    cssText             += "     background-color: " + cssStuff['color'] + ";";
    cssText             += "     border-color: " + cssStuff['color'] + ";";
    cssText             += "}";
    cssText             += ".dropcap1{";
    cssText             += "     background-color: " + cssStuff['color'] + ";";
    cssText             += "}";
    cssText             += ".ui-widget-content a{";
    cssText             += "     color: " + cssStuff['color'] + ";";
    cssText             += "}";
    cssText             += ".widget ul li a:hover{";
    cssText             += "     color: " + cssStuff['color'] + ";";
    cssText             += "}";
    cssText             += ".post-widget .item a.comments{";
    cssText             += "     background-color: " + cssStuff['color'] + ";";
    cssText             += "}";
    cssText             += "#options a:hover,";
    cssText             += ".portfolio-pagination a:hover{";
    cssText             += "     color: " + cssStuff['color'] + ";";
    cssText             += "}";
    cssText             += "#container.portfolio .element h4.hover{";
    cssText             += "     color: " + cssStuff['color'] + ";";
    cssText             += "}";
    cssText             += "blockquote span{";
    cssText             += "     color: " + cssStuff['color'] + "; ";
    cssText             += "}";
    cssText             += ".showbiz-responsive .column h6.hover,";
    cssText             += ".showbiz .item h6.hover{";
    cssText             += "     color: " + cssStuff['color'] + ";";
    cssText             += "}";
    cssText             += ".sitemap ul li a:hover{";
    cssText             += "     color: " + cssStuff['color'] + ";";
    cssText             += "}";
    cssText             += ".sitemap-isotope .element h4.hover{";
    cssText             += "     color: " + cssStuff['color'] + ";";
    cssText             += "}";
    
            
    $("#the-style1").replaceWith("<style id='the-style1' type='text/css'>" + cssText + "</style>");
    cssText = cssText
        .replace(/;/g,";<br />&nbsp;&nbsp;").replace(/{/g,"{<br />&nbsp;&nbsp;").replace(/}/g,"&nbsp;}<br />");
        

}


$(function() {

    $("head").append("<style id='the-style' type='text/css'></style>");
    $("head").append("<style id='the-style1' type='text/css'></style>");

$('.background').ColorPicker({
    color: '315e7b',
	onShow: function (colpkr) {
		$(colpkr).fadeIn("fast");
		return false;
	},
	onHide: function (colpkr) {
		$(colpkr).fadeOut("fast");
		return false;
	},
	onChange: function (hsb, hex, rgb) {
        $($(this).data('colorpicker').el).val(hex).children().css("backgroundColor", "#" + hex);
        
        forValue = $($(this).data('colorpicker').el).attr("rel");
            		
        cssStuff[forValue] = "#" + hex;
        createCSSColor1();
        
        var color_background = cssStuff['background'];
        var color_ellipce = cssStuff['ellipce'];
        
        $.cookie("cookie_color_background", color_background);
        $.cookie("cookie_color_ellipce", color_ellipce);  
	},
});
$('.ellipce').ColorPicker({
    color: '#73c9ff',
	onShow: function (colpkr) {
		$(colpkr).fadeIn("fast");
		return false;
	},
	onHide: function (colpkr) {
		$(colpkr).fadeOut("fast");
		return false;
	},
	onChange: function (hsb, hex, rgb) {
        $($(this).data('colorpicker').el).val(hex).children().css("backgroundColor", "#" + hex);
        
        forValue = $($(this).data('colorpicker').el).attr("rel");
            		
        cssStuff[forValue] = "#" + hex;
        createCSSColor1();
        
        var color_background = cssStuff['background'];
        var color_ellipce = cssStuff['ellipce'];
        
        $.cookie("cookie_color_background", color_background);
        $.cookie("cookie_color_ellipce", color_ellipce);  
	},
});
$('.pickable-link').ColorPicker({
    color: '#9ebf26',
    onShow: function (colpkr) {
    	$(colpkr).fadeIn("fast");
    	return false;
    },
    onHide: function (colpkr) {
    	$(colpkr).fadeOut("fast");
    	return false;
    },
    onChange: function (hsb, hex, rgb) {
        $($(this).data('colorpicker').el).val(hex).children().css("backgroundColor", "#" + hex);
        
        forValue = $($(this).data('colorpicker').el).attr("rel");
            		
        cssStuff[forValue] = "#" + hex;
        createCSSColor();
        
        var color_color = cssStuff['color'];
        
        $.cookie("cookie_color_color", color_color);
    },
});

var switcher_skins = $('.predefined a');
var switcher_link = $('#skins-switcher');
var switcher_pattern = $('#pattern');
 
switcher_pattern.bind('change', function() {
    var pattern = $(this).val();
    var patternUrl = 'url(lpd-switcher/pattern/' + pattern + '.png)';
    $('html').css({
      backgroundImage: patternUrl,
    });
    $.cookie("cookie_pattern", patternUrl)
});

/* predefined skins */ 
switcher_skins.click(function(e) {
    var title = $(this).attr('title');
    
    if (title == "android") {
        switcher_link.attr('href',"lpd-switcher/none.css");
        cssStuff['ellipce'] = "#73c9ff";
        cssStuff['background'] = "#315e7b";
        createCSSColor1();
        cssStuff['color']  = "#9ebf26";
        createCSSColor();
        var atrrHref = switcher_link.attr('href');
        var patternUrl = 'url(lpd-switcher/pattern/13.png)';
        $('html').css({
          backgroundImage: patternUrl,
        });
    }
    if (title == "autumn") {
        switcher_link.attr('href',"lpd-switcher/none.css");
        cssStuff['ellipce'] = "#ffcfcf";
        cssStuff['background'] = "#302727";
        createCSSColor1();
        cssStuff['color'] = "#f2861b";
        createCSSColor();
        var atrrHref = switcher_link.attr('href');
        var patternUrl = 'url(lpd-switcher/pattern/wood1.png)';
        $('html').css({
          backgroundImage: patternUrl,
        });
    }
    if (title == "violet") {
        switcher_link.attr('href',"lpd-switcher/none.css");
        cssStuff['ellipce'] = "#c8c8ff";
        cssStuff['background'] = "#424254";
        createCSSColor1();
        cssStuff['color'] = "#cc2a41";
        createCSSColor();
        var atrrHref = switcher_link.attr('href');
        var patternUrl = 'url(lpd-switcher/pattern/parquet1.png)';
        $('html').css({
          backgroundImage: patternUrl,
        });
    }
    
    $.cookie("cookie_skins", atrrHref);
    
    var color_background = cssStuff['background'];
    var color_ellipce = cssStuff['ellipce'];
    var color_color = cssStuff['color'];
    
    $.cookie("cookie_color_background", color_background);
    $.cookie("cookie_color_ellipce", color_ellipce);
    $.cookie("cookie_color_color", color_color);
    
    //$('html').css({
        //background: 'none',
    //});
    $.cookie("cookie_pattern", patternUrl);
    
    return false;
});

var cookie_skins = $.cookie("cookie_skins");
var pattern = $.cookie("cookie_pattern");
if (cookie_skins) {
    $("#skins-switcher").attr("href", cookie_skins);
    createCSSColor1();
    createCSSColor();
}
if (pattern) {
  $('html').css({
      background: pattern,
  });
}

});